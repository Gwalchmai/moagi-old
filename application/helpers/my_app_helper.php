<?php


function chat() {
	//require_once(APPPATH.'libraries/iFlyChat.php');

	$APP_ID  = '729b8675-d62e-41bb-aed3-3da169a74f5f';
	$API_KEY = 'VOc2xVy8vrOdLzsyy01pEQx1evf6_9340goRi75_rZcW54166';

	//$iflychat = new iFlyChat($APP_ID, $API_KEY);
	$user = array(
		'user_name'        => 'Guest', // string(required)
		'user_id'          => '2',
		'is_admin'         => false, // boolean (optional)
		'user_avatar_url'  => 'user-avatar-link', // string (optional)
		'user_profile_url' => 'user-profile-link', // string (optional)
	);

	//$iflychat->setUser($user);
	//$iflychat_code = $iflychat->getHtmlCode();
	//echo $iflychat_code;
}

function loadmodels() {
	$ci =& get_instance();
	$ci->load->model( 'lookup_model', '', true );
	$ci->load->model( 'user_model', '', true );
	$ci->load->model( 'sd_model', '', true );
	$ci->load->model( 'transaction_model', '', true );
	$ci->load->model( 'admin_model', '', true );
	$ci->load->model( 'tender_model', '', true );
}

function is_verified( $rs, $fieldname ) {
	$verification_status = val( $rs, $fieldname );
	switch ( $verification_status ) {
		case 1:
			$return_icon = '<img src=' . base_url(). 'clientassets/img/verified.png width="20" align="right" alt="Supplier information verified correctly" title="Supplier information verified correctly">';
			break;
		case 2:
			$return_icon = '<img src="' . base_url() . 'clientassets/img/verification_unavailable.png" width="20" align="right" alt="Unable to verify information" title="Unable to verify information">';
			break;
		case 3:
			$return_icon = '<img src="' . base_url() . 'clientassets/img/verification_failed.png" width="20" align="right" alt="Data supplied by company is incorrect" title="Data supplied by company is incorrect">';
			break;
		default:
			$return_icon = '';
			break;
	}

	return $return_icon;
}

function checkuserlogin() {
	$ci =& get_instance();
	if ( $ci->session->userdata( 'user' ) == null ) {
		header( 'Location: /web' );
		exit;
	}
}

function checklogin() {
	$ci =& get_instance();
	if ( $ci->session->userdata( 'user' ) == null ) {
		header( 'Location: /core/login' );
		exit;
	}
}

function checkadminlogin() {
	$ci =& get_instance();
	if ( $ci->session->userdata( 'admin' ) == null ) {
		header( 'Location: /core/login' );
		exit;
	}
}

function user() {
	$ci =& get_instance();
	if ( $ci->session->userdata( 'userid' ) != null ) {
		return $ci->session->userdata( 'userid' );
	} else {
		return null;
	}
}

function company() {
	$ci =& get_instance();
	if ( $ci->session->userdata( 'company' ) != null ) {
		return $ci->session->userdata( 'company' );
	} else {
		return null;
	}
}

function checkcompany() {
	$ci =& get_instance();
	if ( $ci->session->userdata( 'company' ) == null ) {
		error_msg( 'Please select a client to work with.' );
		header( 'Location: /brs/clients' );
		exit;
	}
}

function get_user_name( $userid ) {
	$ci =& get_instance();
	$rs = $ci->user_model->get( $userid );

	return $rs->firstname . ' ' . $rs->lastname;
}

function get_parent_category_name( $categoryid ) {
	$ci =& get_instance();
	$rs = $ci->category_model->find_all( array( 'categoryid' => $categoryid ) );
	if ( count( $rs ) == 0 ) {
		return 'ROOT';
	} else {
		return $rs[0]->title;
	}
}

function get_user_company_name( $userid ) {
	$ci =& get_instance();
	$rs = $ci->user_model->get( $userid );
	if ( count( $rs ) == 0 ) {
		return '';
	} else if ( $rs->companyid == '' ) {
		return '';
	} else {
		return get_company_name( $rs->companyid );
	}
}

function get_company_name( $companyid ) {
	$ci =& get_instance();
	$rs = $ci->company_model->get( $companyid );

	return $rs->companyname;
}

function get_user_email( $userid ) {
	$ci =& get_instance();
	$rs = $ci->user_model->get( $userid );

	return $rs->email;
}

function get_unread_message_count( $messagefolderid = 6 ) {
	$ci =& get_instance();
	$rs = $ci->message_model->find_all( array(
		'touserid'        => user()->userid,
		'readon'          => null,
		'messagefolderid' => $messagefolderid
	), 'messageid' );

	return count( $rs );
}

function get_unread_alert_count() {
	$ci =& get_instance();
	$rs = $ci->alert_model->find_all( array( 'userid' => user()->userid, 'readon' => null ), 'alertid' );

	return count( $rs );
}

function get_company_options( $selected ) {
	$ci =& get_instance();
	$rs = $ci->company_model->find_all( array( 'deletedon' => null ) );
	foreach ( $rs as $row ) {
		if ( $selected == $row->companyid ) {
			echo '<option value="' . $row->companyid . '" selected="selected">' . $row->companyname . '</option>';
		} else {
			echo '<option value="' . $row->companyid . '">' . $row->companyname . '</option>';
		}
	}
}

function calculate_saving_perc( $realamount, $amount ) {
	if ( ( $realamount == '' ) || ( $realamount == '0' ) || ( $amount == '' ) || ( $amount == '0' ) ) {
		return '0%';
	} else {
		return number_format( $amount / $realamount * 100, 0 ) . '%';
	}
}

function audit( $auditactionid, $module, $data ) {
	$ci                     =& get_instance();
	$audit                  = array();
	$audit['createdon']     = mysqltime();
	$audit['userid']        = user()->userid;
	$audit['data']          = $data;
	$audit['module']        = $module;
	$audit['auditactionid'] = $auditactionid;
	$ci->audit_model->insert( $audit );
}

function get_photo1( $rs ) {
	if ( $rs->photo1 == '' ) {
		return '/assets/placeholder.png';
	} else {
		return UPLOADURL . $rs->photo1;
	}
}

function get_user_type_count( $userroleid ) {
	$ci =& get_instance();
	$rs = $ci->db->query( 'select count(*) as total from user where userroleid = ?', array( $userroleid ) );

	return $rs->row( 0 )->total;
}

function verify_dropdown( $displayfield, $value = null ) {
	$sVerifyDropdown = '<select name="' . $displayfield . '" id="' . $displayfield . '">';
	if ( $value == null ) {
		$sVerifyDropdown .= '<option value="0">Please select an option</option>';
	}

	if ( $value == 1 ) {
		$sVerifyDropdown .= '<option value="1" selected="selected">Information verified correctly</option>';
	} else {
		$sVerifyDropdown .= '<option value="1">Information verified correctly</option>';
	}

	if ( $value == 2 ) {
		$sVerifyDropdown .= '<option value="2" selected="selected">Unable to verify information</option>';
	} else {
		$sVerifyDropdown .= '<option value="2">Unable to verify information</option>';
	}

	if ( $value == 3 ) {
		$sVerifyDropdown .= '<option value="3" selected="selected">Incorrect information supplied</option>';
	} else {
		$sVerifyDropdown .= '<option value="3">Incorrect information supplied</option>';
	}

	$sVerifyDropdown .= '</select>';

	return $sVerifyDropdown;
}

?>
