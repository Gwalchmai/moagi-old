<?php

function fp()
{
   if(sget('fp') != '')
      return true;
   else
      return false;
}

function yesno($value)
{
   if($value == '')
      return 'No';
   else if($value == '1')
      return 'Yes';
   else
      return 'No';
}

function nullie($value)
{
   if($value == null)
      return null;
   else if($value == '')
      return null;
   else
      return $value;
}

function zeroie($value)
{
  if($value == null)
     return '0';
  else if($value == '')
     return '0';
  else
     return $value;
}

function sget($field, $specialchars = false,$default = '')
{
   $ci =& get_instance();
   if($ci->input->post($field))
      return $ci->input->post($field);
   else if($default != '')
      return $default;
   else
      return '';
}

function get_lookup_options($lookuptype,$selected)
{
   $ci =& get_instance();
   if(($lookuptype == 'skillid') || ($lookuptype == 'toolsoftradeid') || ($lookuptype == 'techexpertiseid') || ($lookuptype == 'oeambrandid') || ($lookuptype == 'domainexpertiseid'))
      $rs = $ci->lookup_model->find_all(array('lookuptype' => $lookuptype,'profiletypeid' => user()->profiletypeid));
   else
      $rs = $ci->lookup_model->find_all(array('lookuptype' => $lookuptype));
   foreach($rs as $row)
   {
      if(($selected != null) && (contains($selected,"[")) && (contains($selected,"\"")))
      {
         if(contains($selected,'"' . $row->lookupid . '"'))
            echo '<option value="' . $row->lookupid . '" selected="selected">' . $row->lookupvalue . '</option>';
         else
            echo '<option value="' . $row->lookupid . '">' . $row->lookupvalue . '</option>';
      }
      else if($selected == $row->lookupid)
         echo '<option value="' . $row->lookupid . '" selected="selected">' . $row->lookupvalue . '</option>';
      else
         echo '<option value="' . $row->lookupid . '">' . $row->lookupvalue . '</option>';
    }
}

function get_lookup_value($lookupid)
{
   $ci =& get_instance();
   $rs = $ci->lookup_model->get_by('lookupid', $lookupid);
   if(count($rs) > 0)
      return $rs->lookupvalue;
   else
      return null;
}

function get_lookup_extra($lookupid)
{
   $ci =& get_instance();
   $rs = $ci->lookup_model->get_by('lookupid', $lookupid);
   if(count($rs) > 0)
      return $rs->lookupextra;
   else
      return null;
}

function get_lookup_extra2($lookupid)
{
   $ci =& get_instance();
   $rs = $ci->lookup_model->get_by('lookupid', $lookupid);
   if(count($rs) > 0)
      return $rs->lookupextra2;
   else
      return null;
}

function get_lookup_extra3($lookupid)
{
   $ci =& get_instance();
   $rs = $ci->lookup_model->get_by('lookupid', $lookupid);
   if(count($rs) > 0)
      return $rs->lookupextra3;
   else
      return null;
}

function get_lookup_values($lookuptype)
{
   $ci =& get_instance();
   $rs = $ci->lookup_model->get_many_by('lookuptype',$lookuptype);
   return $rs;
}

function get_lookup_value_names($json)
{
   if($json != '')
   {
     $obj = json_decode($json);
     $list = '';
     foreach($obj as $value)
     {
       if($list != '')
         $list .= ',';
       $list .= get_lookup_value($value);
     }
     return $list;
   }
   else
      return '';
}

function get_multiple_lookup_options($lookuptype,$selected)
{
   $ci =& get_instance();
   $rs = $ci->lookup_model->find_all(array('lookuptype' => $lookuptype));
   foreach($rs as $row)
   {
      if(($selected != null) && (contains($selected,"[")) && (contains($selected,"\"")))
      {
         if(contains($selected,'"' . $row->lookupid . '"'))
            echo '<option value="' . $row->lookupid . '" selected="selected">' . $row->lookupvalue . '</option>';
         else
            echo '<option value="' . $row->lookupid . '">' . $row->lookupvalue . '</option>';
      }
      else if($selected == $row->lookupid)
         echo '<option value="' . $row->lookupid . '" selected="selected">' . $row->lookupvalue . '</option>';
      else
         echo '<option value="' . $row->lookupid . '">' . $row->lookupvalue . '</option>';
    }
}


function encuri($data)
{
    $ci =& get_instance();
    //return base64_encode(urlencode($ci->encrypt->encode($data)));
    return base64_encode(urlencode($data));
}

function decuri($data)
{
    $ci =& get_instance();
    //return $ci->encrypt->decode(urldecode(base64_decode($data)));
    return base64_decode(urldecode($data));
}

function mysqltime()
{
   return date('Y-m-d H:i:s');
}

function val($data, $field, $default = '')
{
    $ci =& get_instance();
    if($ci->input->post(strtolower($field)) != '')
        return trim($ci->input->post(strtolower($field)));
    else if($data == null) return $default;
    else if(is_array($data))
    {
      if(array_key_exists($field,$data))
         return $data[$field];
      else
         return $default;
    }
    else if(trim($data->$field) == '')  // 29 Aug
        return trim($default);   // 29 Aug
    else if(count($data) >= 1)
    {
        return trim($data->$field);
    }
    else
        return trim($default);
}

function showdate($date)
{
    if($date == null)
        return '';
    else if($date == '')
        return '';
    else
        return date('Y-m-d',strtotime($date));
}

function showtime($date)
{
    if($date == null)
        return '';
    else if($date == '')
        return '';
    else
        return date('H:i',strtotime($date));
}

function showdatetime($date)
{
    if($date == null)
        return '';
    else if($date == '')
        return '';
    else
        return date('j F',strtotime($date)) . ', at ' . date('H:i a',strtotime($date));
}

function friendlydate($date)
{
   return date('M j, Y',strtotime($date));
}

function limit_text($text,$length = 25)
{
   if(strlen($text) > $length)
      return substr($text,0,$length) . '...';
   else
      return $text;
}

function insert_id($table)
{
   $ci =& get_instance();
   $rs = $ci->db->query('select @@identity as id from ' . $table);
   return $rs->row(0)->id;
}

function timeelapsed($ptime)
{
    $etime = time() - strtotime($ptime);

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function contains($string, $value)
{
    if($value == null)
        return false;
    else if(strtoupper(trim($string)) == strtoupper(trim($value)))
        return true;
    else if(strpos(strtoupper(trim($string)),strtoupper(trim($value))) === false)
        return false;
    else
        return true;
}

function moneyns($num)
{
    if(($num == null) || ($num == ''))
        return '0.00';
    else
    {
        $str = number_format($num,2);
        $str = str_replace(",","",$str);
        return $str;
    }
}

function money($num)
{
    if(($num == null) || ($num == ''))
        return '0.00';
    else
    {
        $str = number_format($num,2);
        $str = str_replace(",","",$str);
        return $str;
    }
}

function gravatar($email = '')
{
   if($email == '')
      $email = user()->email;

   $hash = md5(trim(strtolower($email)));
   return 'https://www.gravatar.com/avatar/' . $hash;
}


function get_file_extension($file_name) {
   $info = new SplFileInfo($file_name);
   return $info->getExtension();
}

function get_multidimensional_form_post_array($formpost,$lastfield)
{
   $dataarr = array();
   if(isset($formpost))
   {
      $data = array();
      foreach($formpost as $arr)
      {
         foreach($arr as $key => $value)
         {
            $data[$key] = $value;
            if($key == $lastfield)
            {
               $dataarr[] = $data;
               $data = array();
            }
         }
      }
   }
   return $dataarr;
}

function upload_file($fieldname)
{
   if(isset($_FILES[$fieldname]['tmp_name']))
	{
		$ext = strtolower(get_file_extension($_FILES[$fieldname]['name']));
		$filename = md5($_FILES[$fieldname]['tmp_name']) . '.' . $ext;
		if(move_uploaded_file($_FILES[$fieldname]['tmp_name'],UPLOADPATH . $filename))
      {
         return $filename;
      }
	}
   return '';
}

function post_to_json_str_array($fieldname)
{
   if((isset($_POST[$fieldname])) && ($_POST[$fieldname] != null))
   {
      $arr = array();
      foreach($_POST[$fieldname] as $item)
      {
         $arr[] = $item;
      }
      return json_encode($arr);
   }
   else
      return '';
}

function generate_password($length = 8) {
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);

    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }

    return $result;
}

?>
