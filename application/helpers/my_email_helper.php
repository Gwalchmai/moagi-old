<?php

function sendemail( $to, $subject, $msg, $attachments = null ) {
	//send email to new user
	$ci =& get_instance();

	$msg = $ci->load->view( 'templates/email_generic', array( 'emailtitle' => $subject, 'emailbody' => $msg ), true );

	$config['protocol'] = 'mail';
	$config['charset']  = 'iso-8859-1';
	$config['mailtype'] = 'html';

	$ci->email->initialize( $config );

	$ci->email->from( 'admin@saibppbd.co.za', 'SAIBPP Business Directory ' );

	$ci->email->to( $to );
	$ci->email->subject( $subject );
	$ci->email->message( $msg );
	$ci->email->send();

	//send notification of new user to admin
	$ci->email->to( 'admin@saibppbd.co.za' );
	$ci->email->subject( 'saibppbd.co.za: New user registration notification' );
	$ci->email->message( 'Please note that new user has registered on www.saibppbd.co.za.  Please visit http://www.saibppbd.co.za/admin to verify the new user' );

	return $ci->email->send();
}

function welcome_email( $data ) {
	$option = 'As a basic subscriber, you unfortunately do not have access to the full suite of functionality that the SAIBPP Business Directory offers. We don\'t want you missing out on a single functionality, and would encourage you to become a premium member at a cost of only R2 190 per annum.';
	if ( ( $data->supplierpaymentoptionid != '' ) && ( $data->supplierpaymentoptionid != '0' ) ) {
		$option = 'As a premium subscriber, you have access to the full suite of functionality that the SAIBPP Business Directory offers.';
	} else if ( ( $data->procurerpaymentoptionid != '' ) && ( $data->procurerpaymentoptionid != '0' ) ) {
		$missing = '<ul>';
	}

	$fields = array();
	if ( $data->companyname == '' ) {
		$fields[] = 'Company Name';
	}
	if ( $data->tradingname == '' ) {
		$fields[] = 'Company Name';
	}
	if ( $data->saibppmemberid == '' ) {
		$fields[] = 'SAIBPP Member';
	}
	if ( $data->companytypeid == '' ) {
		$fields[] = 'Company Type';
	}
	if ( $data->industrysectorid == '' ) {
		$fields[] = 'Industry Sector';
	}
	if ( $data->propertysubsectorid == '' ) {
		$fields[] = 'Property Industry Sub-Sector';
	}
	if ( $data->companytel == '' ) {
		$fields[] = 'Company Tel';
	}
	if ( $data->companyemail == '' ) {
		$fields[] = 'Company Email';
	}
	if ( $data->locationid == '' ) {
		$fields[] = 'Location';
	}
	if ( $data->titleid == '' ) {
		$fields[] = 'Contact Person Title';
	}
	if ( $data->name == '' ) {
		$fields[] = 'Contact Person Name';
	}
	if ( $data->surname == '' ) {
		$fields[] = 'Contact Person Surname';
	}
	if ( $data->designation == '' ) {
		$fields[] = 'Contact Person Designation';
	}
	if ( $data->email == '' ) {
		$fields[] = 'Contact Person Email Address';
	}
	if ( $data->tel == '' ) {
		$fields[] = 'Contact Person Mobile Number';
	}
	if ( $data->bankconfirmationletter == '' ) {
		$fields[] = 'Bank Confirmation Letter';
	}
	if ( $data->companylogo == '' ) {
		$fields[] = 'Company Logo';
	}
	if ( $data->yearsoperating == '' ) {
		$fields[] = 'Years Operating';
	}
	if ( $data->imageofpremises == '' ) {
		$fields[] = 'Proof of business address (RICA documents)';
	}
	if ( $data->tr_companyname1 == '' ) {
		$fields[] = 'Trade References';
	}
	if ( $data->blackownershippercent == '' ) {
		$fields[] = 'Black Ownership %';
	}
	if ( $data->blackwomenownershippercent == '' ) {
		$fields[] = 'Black Women Ownership %';
	}
	if ( $data->beestatusid == '' ) {
		$fields[] = 'BBBEE Status';
	}
	if ( $data->enterprisesizeid == '' ) {
		$fields[] = 'Enterprise Size';
	}
	if ( $data->bbbeecerttypeid == '' ) {
		$fields[] = 'BBBEE Certificate Type';
	}
	if ( $data->verificationauditorid == '' ) {
		$fields[] = 'Verification Auditor';
	}
	if ( $data->verificationauditorid == '' ) {
		$fields[] = 'Are you classified as an empowering supplier?';
	}
	if ( $data->blackcompanystatusid == '' ) {
		$fields[] = 'Black Company Status';
	}
	if ( $data->vatregisteredid == '' ) {
		$fields[] = 'Is your business VAT registered';
	}
	if ( $data->vatnumber == '' ) {
		$fields[] = 'VAT Number';
	}
	if ( $data->businessclassificationid == '' ) {
		$fields[] = 'Business Classification';
	}
	if ( $data->bbbeecertificate == '' ) {
		$fields[] = 'BBBEE Certificate';
	}
	if ( $data->taxclearancecert == '' ) {
		$fields[] = 'Tax Clearance Certificate';
	}
	if ( $data->workmanscompensation == '' ) {
		$fields[] = 'Workman\'s Compensation';
	}
	if ( $data->publicliability == '' ) {
		$fields[] = 'Public Liability';
	}
	if ( $data->tradingreferences == '' ) {
		$fields[] = 'Trading References';
	}
	if ( $data->otherdocuments == '' ) {
		$fields[] = 'Other Documents';
	}

	try {
		foreach ( $fields as $field ) {
			$missing .= '<li>' . $field . '</li>';
		}
	} catch ( Exception $e ) {

	}

	$missing .= '</ul>';

	$body = <<<HTML
<p>Good Day $data->name,</p>
<p>SAIBPPBD is a multipurposed business directory connecting suppliers and procurers in the property sector.</p>
 <p>We confirm your username and password as follows:<br />
Username: $data->email<br />
Password: $data->password<br /></p>
<p>We recommend that you change your password regularly to prevent fraudulent activity on your account.</p>
<p>We've noticed that you have omitted some required company details.  Kindly go to our site on http://www.saibppbd.co.za to complete the application process.</p>
<p>SAIBPPBD Launch Promo: First 100 registered profiles enjoy full PREMIUM ACCESS until March 2018. Premium Access includes UNLIMITED access to all SAIBPPBD features including; B-BBEE verification, featured listings and industry news!</p>
<p>Drop us a line on admin@saibppbd.co.za to tell us what you think or so that we can provide you with further assistance!</p>
<p><a href="http://www.saibppbd.co.za">Click here</a> to log in and continue the registration process or explore SAIBPPBD.</p>
HTML;

	sendemail( $data->email, 'Welcome to the SAIBPP Business Directory!', $body, null );
}

function free_user_email( $data ) {
	$body = '<p>Good Day ' . $data->name . ',</p>
            <p>We trust that the SAIBPP Business Directory functionality and service meets your expectations.  If not, feel free to email us at admin@saibppbd.co.za to provide feedback on the difficulties you\'re experiencing.  We will endeavour to address your concerns as soon as possible!</p>
            <p>We would like expose you to the benefits that a premium subscriber enjoys and will therefore provide you with the full operation of the opportunities functionality for a limited period of 2 weeks.</p>
            <p>Full access to the consolidated tender/RFI/RFQ opportunities in the property sector is at your disposal for a period of 2 weeks.  You will be able to access the following:</p>
            <ul>
            <li>Daily tender/RFQ/RFI alerts matching your company rules – manage your preferred industry sectors, regions and keywords</li>
            <li>View +-150 active procurement opportunities on any given day</li>
            </ul>
            <p>If you need any further assistance, please email us on admin@saibppbd.co.za.</p>
            ';

	sendemail( $data->email, 'SAIBPP Membership', $body, null );
}

function membership_email( $data ) {
	if ( $data->userroleid == 4 ) {
		$rates = '<p>Supplier:<ul>
                <li>R2 189.00 for a 12-month subscription</li>
                <li>R1 194.00 for a 6-month subscription</li>
                </ul></p>';
	} else {
		$rates = '<p>Procurer:<ul>
              <li>R3 289.00 for a 12-month subscription</li>
              <li>R1 794.00 for a 6-month subscription</li>
              </ul></p>';
	}
	$body = '<p>Good Day ' . $data->name . ',</p>
            <p><strong>Welcome to the SAIBPP Business Directory Community </strong></p>
            <p>Thank you for upgrading your membership to premium.</p>' . $rates . '
            <p>We trust you will find good value from all the features available to our premium members.</p>
            <p>Kindly email proof of payment to admin@saibppbd.co.za</p>
            <p>Your premium status will be activated on receipt of proof of payment.</p>
            ';

	sendemail( $data->email, 'SAIBPP Premium Membership', $body, null );
}

function payment_processed_email( $data, $pdfinvoice ) {
	$body = '<p>Good Day ' . $data->name . ',</p>
            <p>Your membership payment was processed successfully.</p>
            <p>If you need any further assistance, please email us on admin@saibppbd.co.za.</p>
            ';

	$attachments   = array();
	$attachments[] = $pdfinvoice;
	sendemail( $data->email, 'SAIBPP Membership Payment', $body, $attachments );
}
