<?php


function CreatePayment() {
   	$ci =& get_instance();

	$user = $ci->user_model->get( $ci->session->userdata( 'user' )->userid );

	echo "User Data: <br>";
	var_dump( $user );

	$amount      = 0;
	$description = '';
	$name        = '';
	$subid       = 0;

	if ( $user->supplierpaymentoptionid == 446 ) {
		$name        = 'Supplier 12 Months Subscription - One Off Payment of R1999.00';
		$amount      = 1999.00;
		$description = 'Supplier 12 Months Subscription - One Off Payment of R1999.00';
		$subid       = 446;
	} else if ( $user->supplierpaymentoptionid == 447 ) {
		$name        = 'Supplier 12 Month Subscription - 12 monthly payments of R199.00';
		$amount      = 199.00;
		$description = 'Supplier 12 Month Subscription - 12 monthly payments of R199.00';
		$subid       = 447;
	} else if ( $user->procurerpaymentoptionid == 448 ) {
		$name        = 'Procurer 12 Months Subscription - One Off Payment of R1999.00';
		$amount      = 1999.00;
		$description = 'Procurer 12 Months Subscription - One Off Payment of R1999.00';
		$subid       = 448;
	} else if ( $user->procurerpaymentoptionid == 449 ) {
		$name        = 'Procurer 12 Month Subscription - 12 monthly payments of R199.00';
		$amount      = 199.00;
		$description = 'Procurer 12 Month Subscription - 12 monthly payments of R199.00';
		$subid       = 449;
	}

	$pfOutput = '';
	$data     = array(
		// Merchant details
		'merchant_id'      => 12110763,
		'merchant_key'     => 'g69fexcgrhiwa',
		'return_url'       => base_url( '/payment/returnurl' ),
		'cancel_url'       => base_url( '/payment/cancelurl' ),
		'notify_url'       => base_url( '/payment/notifyurl' ),
		// Buyer details
		'name_first'       => $user->name,
		'name_last'        => $user->surname,
		'email_address'    => $user->email,
		// Transaction details
		'm_payment_id'     => $user->userid, //Unique payment ID to pass through to notify_url
		'amount'           => number_format( $amount, 2, '.', '' ), //Amount in ZAR
		'item_name'        => $name,
		'item_description' => $description,
		'custom_int1'      => $user->userid, //custom integer to be passed through
		'custom_str1'      => $subid,
		'payment_method'   => $user->payment_method
	);

	echo "Payment Data:<br>";
	echo "<pre>";
	print_r( $data );
	echo "</pre>";

	//handle month-to-month subscriptions
	if ( $user->supplierpaymentoptionid == 447 || $user->procurerpaymentoptionid == 449 ) {
		?>
        <input type="hidden" name="subscription_type" value="1">
        <input type="hidden" name="frequency" value="3">
        <input type="hidden" name="cycles" value="12">
		<?php
	}


	// Create GET string
	foreach ( $data as $key => $val ) {
		if ( ! empty( $val ) ) {
			$pfOutput .= $key . '=' . urlencode( trim( $val ) ) . '&';
		}
	}
	// Remove last ampersand
	$getString = substr( $pfOutput, 0, - 1 );
	if ( isset( $passPhrase ) ) {
		$getString .= '&passphrase=' . urlencode( trim( $passPhrase ) );
	}
	echo "Get String:<br>";

	$data['signature'] = md5( $getString );

	$testingMode = true;
	$pfHost      = $testingMode ? 'sandbox.payfast.co.za' : 'www.payfast.co.za';
	$htmlForm    = '
    <form action="https://' . $pfHost . '/eng/process" method="post" name="form" id="form">';
	foreach ( $data as $name => $value ) {
		$htmlForm .= '<input name="' . $name . '" type="hidden" value="' . $value . '" />';
	}
	$htmlForm .= '</form><script>document.form.submit();</script>';
	echo $htmlForm;
}

?>
