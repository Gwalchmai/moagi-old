<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Dashboard</h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<?php display_messages(); ?>
					<form id="form" method="post">
					<input type="hidden" name="fp" value="1">
					<!-- start: page -->
					<div class="row">
						<div class="col-md-6 col-lg-12 col-xl-6">
							<section class="panel">
								<div class="panel-body">
									<div class="row">
										<div class="col-lg-12">
											<div class="chart-data-selector" id="salesSelectorWrapper">
												<div class="row">
													<div class="col-lg-2">
														Province:
														<strong>
															<select id="provinceid" name="provinceid" class="form-control mb-md">
																<option value=""></option>
																<?php get_lookup_options('provinceid',val($rs,'provinceid',sget('provinceid'))); ?>
															</select>
														</strong>
													</div>
													<div class="col-lg-2">
														District:
														<strong>
															<select id="districtid" name="districtid" class="form-control mb-md">
																<option value=""></option>
																<?php get_lookup_options('districtid',val($rs,'districtid',sget('districtid'))); ?>
															</select>
														</strong>
													</div>
													<div class="col-lg-2">
														School:
														<strong>
															<select id="companyid" name="companyid" class="form-control mb-md">
																<option value=""></option>
																<?php get_company_options(val($rs,'companyid',sget('companyid'))); ?>
															</select>
														</strong>
													</div>
													<div class="col-lg-2">
														Grade:
														<strong>
															<select id="gradeid" name="gradeid" class="form-control">
																<option value=""></option>
																<?php echo get_lookup_options('gradeid',sget('gradeid')); ?>
															</select>
														</strong>
													</div>
													<div class="col-lg-2">

														Subject:
														<strong>
															<select id="subjectid" name="subjectid" class="form-control" >
																<option value=""></option>
																<?php echo get_lookup_options('subjectid',sget('subjectid')); ?>
															</select>
														</strong>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
						<div class="col-md-6 col-lg-12 col-xl-6">
							<div class="row">
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-primary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-primary">
														<i class="fa fa-bank"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Schools</h4>
														<div class="info">
															<strong class="amount">
																<?php
																	$where = array();

																	if(sget('provinceid') != '')
																		$where['provinceid'] = sget('provinceid');

																	if(sget('districtid') != '')
																		$where['districtid'] = sget('districtid');

																	$rs = $this->company_model->find_all($where);
																	echo count($rs);
																?>
															</strong>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-secondary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-secondary">
														<i class="fa fa-group"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Students</h4>
														<div class="info">
															<strong class="amount">
																<?php
																	$where = array();

																	$where['userroleid'] = 77;

																	if(sget('provinceid') != '')
																		$where['provinceid'] = sget('provinceid');

																	if(sget('districtid') != '')
																		$where['districtid'] = sget('districtid');

																	if(sget('companyid') != '')
																		$where['companyid'] = sget('companyid');

																	$rs = $this->user_model->find_all($where);
																	echo count($rs);
																?>
															</strong>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-tertiary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-tertiary">
														<i class="fa fa-bar-chart"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Average Marks</h4>
														<div class="info">
															<strong class="amount">
																<?php
																	$where = array();

																	if(sget('provinceid') != '')
																		$where['provinceid'] = sget('provinceid');

																	if(sget('districtid') != '')
																		$where['districtid'] = sget('districtid');

																	if(sget('companyid') != '')
																		$where['companyid'] = sget('companyid');

																	if(sget('gradeid') != '')
																		$where['gradeid'] = sget('gradeid');

																	if(sget('subjectid') != '')
																		$where['subjectid'] = sget('subjectid');

																	$rs = $this->stats_model->find_all($where);
																	$count = 0;
																	$total = 0;
																	$worst = 100;
																	foreach($rs as $row)
																	{
																		$count++;
																		$total = $total + $row->mark;
																		if($row->mark < $worst)
																			$worst = $row->mark;

																	}
																	if($count != 0)
																		echo number_format($total / $count,0);
																	else
																		echo 'N/A';
																?>
															</strong>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
								<div class="col-md-12 col-lg-6 col-xl-6">
									<section class="panel panel-featured-left panel-featured-quartenary">
										<div class="panel-body">
											<div class="widget-summary">
												<div class="widget-summary-col widget-summary-col-icon">
													<div class="summary-icon bg-quartenary">
														<i class="fa fa-bar-chart"></i>
													</div>
												</div>
												<div class="widget-summary-col">
													<div class="summary">
														<h4 class="title">Worst Marks</h4>
														<div class="info">
															<strong class="amount"><?php echo $worst; ?></strong>
														</div>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Performance</h2>
								</header>
								<div class="panel-body">

									<div class="panel-body">
										<div class="row">
											<div class="col-md-6">
												<div id="ChartistLineChartWithTooltips" class="ct-chart ct-perfect-fourth ct-golden-section"></div>
											</div>
											<div class="col-md-6">
												<div id="ChartistStackedChart" class="ct-chart ct-perfect-fourth ct-golden-section"></div>
											</div>
										</div>
									</div>
								</div>
							</section>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-6 col-md-12">
							<section class="panel">
								<header class="panel-heading panel-heading-transparent">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Top Performing Schools</h2>
								</header>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped mb-none">
											<thead>
												<tr>
													<th>#</th>
													<th>School</th>
													<th>Avg</th>
												</tr>
											</thead>
											<tbody>
												<?php

													function countavgmargs($rs,$companyid)
													{
														$count = 0;
														$total = 0;
														foreach($rs as $row)
														{
															if($row->companyid == $companyid)
															{
																$count++;
																$total = $total + $row->mark;
															}
														}
														if($count != 0)
															return $total / $count;
														else
															return 0;
													}

													$processed = array();
												   $index = 1;
													foreach($rs as $row)
													{
														if(!in_array($row->companyid,$processed))
														{
															$processed[] = $row->companyid;
															?>
															<tr>
																<td><?php echo $index; ?></td>
																<td><?php echo get_company_name($row->companyid); ?>
																<td><?php echo number_format(countavgmargs($rs,$row->companyid),2) . '%'; ?>
															</tr>
															<?php
															$index++;
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</section>
						</div>
						<div class="col-lg-6 col-md-12">
							<section class="panel">
								<header class="panel-heading panel-heading-transparent">
									<div class="panel-actions">
										<a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
										<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
									</div>

									<h2 class="panel-title">Top Worst Performing Schools</h2>
								</header>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped mb-none">
											<thead>
												<tr>
													<th>#</th>
													<th>School</th>
													<th>Avg</th>
												</tr>
											</thead>
											<tbody>
												<?php

													$processed = array();
												   $index = 1;
													foreach($rs as $row)
													{
														if(!in_array($row->companyid,$processed))
														{
															$processed[] = $row->companyid;
															?>
															<tr>
																<td><?php echo $index; ?></td>
																<td><?php echo get_company_name($row->companyid); ?>
																<td><?php echo number_format(countavgmargs($rs,$row->companyid),2) . '%'; ?>
															</tr>
															<?php
															$index++;
														}
													}
												?>
											</tbody>
										</table>
									</div>
								</div>
							</section>
						</div>
					</div>
				</form>

					<!-- end: page -->
				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => true)); ?>

		<script src="/assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="/assets/vendor/chartist/chartist.js"></script>

		<script>

		   jQuery(document).ready(function(){

				$('#provinceid').change(function(){
					$('#form').submit();
				});

				$('#districtid').change(function(){
					$('#form').submit();
				});

				$('#companyid').change(function(){
					$('#form').submit();
				});

				$('#subjectid').change(function(){
					$('#form').submit();
				});

				$('#gradeid').change(function(){
					$('#form').submit();
				});

				new Chartist.Line('#ChartistLineChartWithTooltips', {
					labels: ['2016/1', '2016/2', '2016/3', '2016/4'],
					series: [{
						name: 'Demo School',
						data: [1, 2, 3, 5]
					}, {
						name: 'Demo School',
						data: [1, 1.618, 2.618, 4.236]
					}]
				});

				var $chart = $('#ChartistLineChartWithTooltips');

				var $toolTip = $chart
					.append('<div class="tooltip"></div>')
					.find('.tooltip')
					.hide();

				$chart.on('mouseenter', '.ct-point', function() {
					var $point = $(this),
						value = $point.attr('ct:value'),
						seriesName = $point.parent().attr('ct:series-name');
					$toolTip.html(seriesName + '<br>' + value).show();
				});

				$chart.on('mouseleave', '.ct-point', function() {
					$toolTip.hide();
				});

				$chart.on('mousemove', function(event) {
					$toolTip.css({
						left: (event.offsetX || event.originalEvent.layerX) - $toolTip.width() / 2 - 10,
						top: (event.offsetY || event.originalEvent.layerY) - $toolTip.height() - 40
					});
				});

				new Chartist.Bar('#ChartistStackedChart', {
					labels: ['Q1', 'Q2', 'Q3', 'Q4'],
					series: [
						[800000, 1200000, 1400000, 1300000],
						[200000, 400000, 500000, 300000],
						[100000, 200000, 400000, 600000]
					]
				}, {
					stackBars: true,
					axisY: {
						labelInterpolationFnc: function(value) {
							return (value / 1000) + 'k';
						}
					}
				}).on('draw', function(data) {
					if (data.type === 'bar') {
						data.element.attr({
							style: 'stroke-width: 30px'
						});
					}
				});

		   });

		</script>
	</body>
</html>
