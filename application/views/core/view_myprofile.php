<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-6">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>
									<form id="form" method="post">
			                     <input type="hidden" name="fp" value="1">

										<div class="form-group mb-lg">
											<label>First Name</label>
											<input name="firstname" id="firstname" type="text" class="form-control " required value="<?php echo val($rs,'firstname',sget('firstname')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Last Name</label>
											<input name="lastname" id="lastname" type="text" class="form-control " required value="<?php echo val($rs,'lastname',sget('lastname')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>E-mail Address</label>
											<input name="email" type="email" class="form-control " required value="<?php echo val($rs,'email',sget('email')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Mobile</label>
											<input name="mobile" type="mobile" class="form-control " required value="<?php echo val($rs,'mobile',sget('mobile')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Landline</label>
											<input name="landline" type="landline" class="form-control " value="<?php echo val($rs,'landline',sget('landline')); ?>" />
										</div>

										<div class="form-group mb-none">
											<p><small>Leave your password field empty if you do not intend to change it.</small></p>
											<div class="row">
												<div class="col-sm-6 mb-lg">
													<label>Password</label>
													<input name="pwd" type="password" class="form-control " />
												</div>
												<div class="col-sm-6 mb-lg">
													<label>Password Confirmation</label>
													<input name="pwd_confirm" type="password" class="form-control " />
												</div>
											</div>
										</div>

										<div class="row">
											<div class="col-md-6">
												<button type="submit" class="btn btn-primary">Save Changes</button>
												<!--<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Cancel</button>-->
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
