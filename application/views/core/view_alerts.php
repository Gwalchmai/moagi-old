<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-12">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>

									<?php
										$rsalerts = $this->alert_model->find_all(array('userid' => user()->userid,'deletedon' => null));
										if(count($rsalerts) == 0)
											echo '<p>There are currently no alerts.</p>';
										else {
											?>
											<table class="table table-bordered table-striped mb-none" id="datatable-default">
												<thead>
													<tr>
														<th>Alert</th>
														<th>Created</th>
														<th>Due</th>
														<th width="170"></th>
													</tr>
												</thead>
												<tbody>
											<?php
											foreach($rsalerts as $row)
											{
												$unread = '';
												if($row->readon == '')
													$unread = 'style="font-weight:bold;"';
												?>
												<tr>
													<td <?php echo $unread; ?>><?php echo $row->title; ?></td>
													<td <?php echo $unread; ?>><?php echo showdate($row->createdon); ?></td>
													<td <?php echo $unread; ?>><?php echo showdate($row->duedate); ?></td>
													<td>
														<a href="/core/alerts/action/<?php echo encuri($row->alertid); ?>" class="btn btn-primary btn-sm">Action</a>
														<a href="/core/alerts/delete/<?php echo encuri($row->alertid); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-primary btn-sm">Delete</a>
													</td>
												</tr>
											<?php } ?>
												</tbody>
											</table>
										<?php
										}
										?>

								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
