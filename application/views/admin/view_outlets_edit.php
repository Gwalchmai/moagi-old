<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-8">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>
									<form id="form" method="post">
			                     <input type="hidden" name="fp" value="1">

										<div class="form-group mb-lg">
											<label>Vendor</label>
											<select id="companyid" name="companyid" class="form-control mb-md">
												<option value=""></option>
												<?php get_company_options(val($rs,'companyid',sget('companyid'))); ?>
											</select>
										</div>

										<div class="form-group mb-lg">
											<label>Outlet Name</label>
											<input name="title" id="title" type="text" class="form-control " required value="<?php echo val($rs,'title',sget('title')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>E-mail Address</label>
											<input name="email" type="email" class="form-control " value="<?php echo val($rs,'email',sget('email')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Tel</label>
											<input name="tel" type="tel" class="form-control " value="<?php echo val($rs,'tel',sget('tel')); ?>" />
										</div>

										<div class="form-group mb-none">
											<div class="row">
												<div class="col-sm-6 mb-lg">
													<label>Physical Address</label>
													<input name="physical1" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical1',sget('physical1')); ?>"  />
													<input name="physical2" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical2',sget('physical2')); ?>"  />
													<input name="physical3" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical3',sget('physical3')); ?>"  />
													<select id="provinceid" name="provinceid" class="form-control mb-md" required>
														<option value=""></option>
														<?php get_lookup_options('provinceid',val($rs,'provinceid',sget('provinceid'))); ?>
													</select>
												</div>
												<div class="col-sm-6 mb-lg">
													<label>GPS Coordinates (Lat &amp; Lng)</label>
													<input name="gpslat" type="number" placeholder="GPS Lat, eg: 27.0993" class="form-control address_field_space_below" value="<?php echo val($rs,'gpslat',sget('gpslat')); ?>"  />
													<input name="gpslng" type="number" placeholder="GPS Lng, eg: -28.0993" class="form-control address_field_space_below" value="<?php echo val($rs,'gpslng',sget('gpslng')); ?>"  />
												</div>
											</div>
										</div>


										<div class="checkbox">
											<label>
												<input type="checkbox" id="enabled" name="enabled" <?php if(val($rs,'enabled',sget('enabled')) == '1') echo 'checked="checked"'; ?> value="1">
												Outlet Enabled
											</label>
										</div>

										<div class="row">
											<div class="col-md-6">
												<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Cancel</button>
												<button type="submit" class="btn btn-primary">Save Changes</button>
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
