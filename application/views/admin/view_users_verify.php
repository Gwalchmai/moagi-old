<?php $this->load->view( 'common/inc_html_header' ); ?>
<body>
<section class="body">

	<?php $this->load->view( 'common/inc_header' ); ?>

    <div class="inner-wrapper">

		<?php $this->load->view( 'common/inc_sidebar_left' ); ?>

        <section role="main" class="content-body">
            <header class="page-header">
                <h2><?php echo $pagetitle; ?></h2>

                <div class="right-wrapper pull-right">
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/">
                                <i class="fa fa-home"></i>
                            </a>
                        </li>
                        <li><span><?php echo $pagetitle; ?></span></li>
                    </ol>

                    <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
                </div>
            </header>

            <!-- start: page -->
            <div class="row">
                <div class="col-md-8">
                    <section class="panel">
                        <div class="panel-body">
							<?php display_messages(); ?>
                            <form id="form" method="post">
                                <input type="hidden" name="fp" value="1">

                                <h4>Please select the verification option by choosing from the drop-down.</h4>

                                <div class="form-group mb-lg">
									<?php
									$displayfield = 'companyname';
									$checkfield   = 'ver_companyreg';
									?>
                                    <div class="col-md-6">
                                        <strong>Company Registration Name:
                                            <br></strong><?php echo val( $rs, $displayfield, sget( $displayfield ) ); ?>
                                    </div>

                                    <div class="col-md-6">
										<?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$displayfield = 'tradingname';
										$checkfield   = 'ver_tradename';
										?>
                                        <strong>Trade Name:<br> </strong>
										<?php echo val( $rs, $displayfield, sget( $displayfield ) ); ?>
                                    </div>
                                    <div class="col-md-6">
										<?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$displayfield = 'companyregno';
										$checkfield   = 'ver_companyregno';
										?>
                                        <strong>Company Registration Number:<br> </strong>
										<?php echo val( $rs, $displayfield, sget( $displayfield ) ); ?>
                                    </div>
                                    <div class="col-md-6">
										<?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$displayfield = 'vatnumber';
										$checkfield   = 'ver_vatregno';
										?>
                                        <strong>VAT Registration Number:<br> </strong>
										<?php echo val( $rs, $displayfield, sget( $displayfield ) ); ?>
                                    </div>
                                    <div class="col-md-6">
										<?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$displayfield = 'bankconfirmationletter';
										$checkfield   = 'ver_bankingdetails';
										?>
                                        <strong>Banking Details: <br></strong>
										<?php if ( val( $rs, $displayfield, sget( $displayfield ) ) == '' ) {
											echo ' - Not available.';
										} else { ?>
                                            <a href="<?php echo UPLOADURL . val( $rs, $displayfield, sget( $displayfield ) ); ?>"
                                               target="_blank">View</a>
										<?php } ?>
                                    </div>
                                    <div class="col-md-6">
										<?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$checkfield = 'ver_directors';
										$sduser     = $this->sd_model->find_all( array( 'userid' => val( $rs, 'userid' ) ) );
										$names      = '';
										foreach ( $sduser as $sd ) {
											if ( $names != '' ) {
												$names .= ',';
											}
											$names .= $sd->sd_firstname . ' ' . $sd->sd_lastname;
										}
										?>
                                        <strong>Directors: <br></strong>
										<?php echo $names; ?>
                                    </div>
                                    <div class="col-md-6">
										<?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$numbers    = val( $rs, 'tel', sget( 'tel' ) );
										$checkfield = 'ver_telnumbers';
										?>
                                        <strong>Telephone Number: <br></strong>
                                        <?php echo $numbers; ?>
                                    </div>
                                    <div class="col-md-6">
		                                <?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$address    = val( $rs, 'companyaddress', sget( 'companyaddress' ) ) . ', ' . get_lookup_value( val( $rs, 'locationid' ) );
										$checkfield = 'ver_address';
										?>
                                        <strong>Address: <br></strong>
                                        <?php echo $address; ?>
                                    </div>
                                    <div class="col-md-6">
		                                <?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$displayfield = 'bbbeecertificate';
										$checkfield   = 'ver_bee';
										?>
                                        <strong>BEE Certification: <br></strong>
										<?php if ( val( $rs, $displayfield, sget( $displayfield ) ) == '' ) {
											echo ' - Not available.';
										} else { ?>
                                            <a href="<?php echo UPLOADURL . val( $rs, $displayfield, sget( $displayfield ) ); ?>"
                                               target="_blank">View</a>
										<?php } ?>
                                    </div>
                                    <div class="col-md-6">
		                                <?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="form-group mb-lg">
                                    <div class="col-md-6">
										<?php
										$displayfield = '';
										$checkfield   = 'ver_tradereferences';
										$names        = '';
										$names        .= $sd->tr_companyname1;
										$names        .= ', ';
										$names        .= $sd->tr_companyname2;
										$names        .= ', ';
										$names        .= $sd->tr_companyname3;
										?>
                                        <strong>Trade References: <br></strong>
                                       <?php echo $names; ?>
                                    </div>
                                    <div class="col-md-6">
		                                <?php echo verify_dropdown( $checkfield, val( $rs, $checkfield, sget( $checkfield ) ) ); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">
                                            Cancel
                                        </button>
                                        <button type="submit" class="btn btn-primary">Save Verification Settings</button>
                                    </div>
                                </div>
                            </form>
                    </section>
                </div>

            </div>
    </div>
    <!-- end: page -->

</section>
</div>

<?php $this->load->view( 'common/inc_sidebar_right.php' ); ?>
</section>

<?php $this->load->view( 'common/inc_html_footer.php', array( 'collapse_left_side' => false, 'hascharts' => false ) ); ?>
</body>
</html>
