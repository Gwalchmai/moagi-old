<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-8">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>
									<form id="form" method="post">
			                     <input type="hidden" name="fp" value="1">

										<div class="form-group mb-lg">
											<label>Parent Category</label>
											<select id="parentcategoryid" name="parentcategoryid" class="form-control mb-md" required>
												<option value="0">Root</option>
												<?php
													function listchildren($parentcategoryid,$selected,$level)
													{
														$ci =& get_instance();
														$rs = $ci->category_model->find_all(array('parentcategoryid' => $parentcategoryid));
														foreach($rs as $row)
														{
															$spacing = '';
															for($i=1;$i<=$level;$i++)
																$spacing .= '&nbsp;&nbsp;&nbsp;';

															if($selected == $row->categoryid)
																echo '<option value="' . $row->categoryid . '" selected="selected">' . $spacing . $row->title . '</option>';
															else
																echo '<option value="' . $row->categoryid . '">' . $spacing . $row->title . '</option>';

															listchildren($row->categoryid,$selected,$level + 1);
														}
													}

													listchildren(0,val($rs,'parentcategoryid',sget('parentcategoryid')),1);
												?>
											</select>
										</div>

										<div class="form-group mb-lg">
											<label>Title</label>
											<input name="title" id="title" type="text" class="form-control " required value="<?php echo val($rs,'title',sget('title')); ?>" />
										</div>

										<div class="checkbox">
											<label>
												<input type="checkbox" id="enabled" name="enabled" <?php if(val($rs,'enabled',sget('enabled')) == '1') echo 'checked="checked"'; ?> value="1">
												Category Enabled
											</label>
										</div>

										<div class="row">
											<div class="col-md-6">
												<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Cancel</button>
												<button type="submit" class="btn btn-primary">Save Changes</button>
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
