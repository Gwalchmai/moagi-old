<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<div class="row">
						<div class="col-md-12">
							<p><a href="<?php echo $listurl; ?>/add" class="btn btn-primary pull-right"><?php echo $addtitle; ?></a><br /></p>
						</div>
					</div>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-12">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>

									<?php
										$rs = $this->outlet_model->find_all(array('deletedon' => null));

										if(count($rs) == 0)
											echo '<p>There are currently no ' . strtolower($pagetitle) . ' to show here.</p>';
										else {
											?>
											<table class="table table-bordered table-striped mb-none" id="datatable-default">
												<thead>
													<tr>
														<th>Company</th>
														<th>Outlet</th>
														<th>Enabled</th>
														<th width="170"></th>
													</tr>
												</thead>
												<tbody>
											<?php
											foreach($rs as $row)
											{
											?>
												<tr>
													<td><?php echo get_company_name($row->companyid); ?></td>
													<td><?php echo $row->title; ?></td>
													<td><?php echo yesno($row->enabled); ?></td>
													<td>
														<a href="<?php echo $listurl; ?>/edit/<?php echo encuri($row->outletid); ?>" class="btn btn-primary btn-sm">Edit</a>
														<a href="<?php echo $listurl; ?>/delete/<?php echo encuri($row->outletid); ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-primary btn-sm">Delete</a>
													</td>
												</tr>
											<?php
											}
											?>
												</tbody>
											</table>
										<?php
										}
										?>

								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
