<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-8">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>
									<form id="form" method="post" enctype="multipart/form-data">
			               <input type="hidden" name="fp" value="1">

										 <div class="form-group">
 											 <label>Category <em class="req">*</em></label>
 											 <?php $fieldname = "tendercategoryid"; ?>
 											 <select name="<?php echo $fieldname; ?>" class="form-control mb-md" required="required">
 													 <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
 											 </select>
 										</div>

 										<div class="form-group">
 												<label>Tender Number <em class="req">*</em> </label>
 												<?php $fieldname = "tendernumber"; ?>
 												<input type="text" required="required" class="form-control" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
 										</div>

 										<div class="form-group">
 												<label>Description <em class="req">*</em></label>
 												<?php $fieldname = "description"; ?>
 												<textarea class="form-control" required="required" name="<?php echo $fieldname; ?>"><?php echo val($rs,$fieldname,sget($fieldname)); ?></textarea>
 										</div>

 										<div class="form-group">
 												<label>Date Published <em class="req">*</em> </label>
 												<?php $fieldname = "tenderdate"; ?>
 												<input type="date" required="required" class="form-control datepicker" name="<?php echo $fieldname; ?>" value="<?php if(val($rs,$fieldname,sget($fieldname)) == '') echo date('Y-m-d'); else echo showdate(val($rs,$fieldname,sget($fieldname))); ?>">
 										</div>

 										<div class="form-group">
 												<label>Time Published</label>
 												<?php $fieldname = "tenderdatetime"; ?>
 												<input type="time" class="form-control timepicker" name="<?php echo $fieldname; ?>" value="<?php if(val($rs,$fieldname,sget($fieldname)) == '') echo date('H:m'); else echo val($rs,$fieldname,sget($fieldname)); ?>">
 										</div>

 										<div class="form-group">
 												<label>Closing Date </label>
 												<?php $fieldname = "tenderclosedate"; ?>
 												<input type="date" class="form-control datepicker" name="<?php echo $fieldname; ?>" value="<?php echo showdate(val($rs,$fieldname,sget($fieldname))); ?>">
 										</div>

 										<div class="form-group">
 												<label>Closing Time </label>
 												<?php $fieldname = "tenderclosedatetime"; ?>
 												<input type="time" class="form-control timepicker" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
 										</div>

 										<div class="form-group">
 												<label>Briefing Session Date </label>
 												<?php $fieldname = "briefingsessiondate"; ?>
 												<input type="date" class="form-control datepicker" name="<?php echo $fieldname; ?>" value="<?php echo showdate(val($rs,$fieldname,sget($fieldname))); ?>">
 										</div>

 										<div class="form-group">
 												<label>Briefing Session Time </label>
 												<?php $fieldname = "briefingsessiontime"; ?>
 												<input type="time" class="form-control timepicker" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
 										</div>

 										<div class="form-group">
 												<label>Upload Tender Document</label>
 												<?php $fieldname = "tenderdocument"; ?>

 												<br /><label for="<?php echo $fieldname; ?>" class="custom-file-upload">Browse</label>
 												<input id="<?php echo $fieldname; ?>" type="file" class="form-control" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">

 												<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>
 										</div>

										<div class="form-group">
 												<label>or Paste Tender Document URL </label>
 												<?php $fieldname = "docurl"; ?>
 												<input type="text" class="form-control" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
 										</div>

										<div class="row">
											<div class="col-md-6">
												<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Cancel</button>
												<button type="submit" class="btn btn-primary">Save Changes</button>
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
