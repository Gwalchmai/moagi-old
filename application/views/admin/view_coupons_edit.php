<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-6">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>
									<h4>Coupon Edit</h4>
									<form id="form" method="post" enctype="multipart/form-data">
			                     <input type="hidden" name="fp" value="1">

										<div class="form-group mb-lg">
											<label>Vendor</label>
											<select id="companyid" name="companyid" class="form-control mb-md">
												<option value=""></option>
												<?php get_company_options(val($rs,'companyid',sget('companyid'))); ?>
											</select>
										</div>

										<div class="form-group mb-lg">
											<label>Parent Category</label>
											<select id="categoryid" name="categoryid" class="form-control mb-md" required>
												<option value="0">Root</option>
												<?php
													function listchildren($parentcategoryid,$selected,$level)
													{
														$ci =& get_instance();
														$rs = $ci->category_model->find_all(array('parentcategoryid' => $parentcategoryid));
														foreach($rs as $row)
														{
															$spacing = '';
															for($i=1;$i<=$level;$i++)
																$spacing .= '&nbsp;&nbsp;&nbsp;';

															if($selected == $row->categoryid)
																echo '<option value="' . $row->categoryid . '" selected="selected">' . $spacing . $row->title . '</option>';
															else
																echo '<option value="' . $row->categoryid . '">' . $spacing . $row->title . '</option>';

															listchildren($row->categoryid,$selected,$level + 1);
														}
													}

													listchildren(0,val($rs,'categoryid',sget('categoryid')),1);
												?>
											</select>
										</div>

										<div class="form-group mb-lg">
											<label>Coupon Title</label>
											<input name="title" id="title" type="text" class="form-control " required value="<?php echo val($rs,'title',sget('title')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Description</label>
											<textarea name="description" id="description" class="form-control " style="height:100px;"><?php echo val($rs,'description',sget('description')); ?></textarea>
										</div>

										<div class="form-group mb-lg">
											<label>Coupon Image</label>
											<input name="photo1" id="photo1" type="file" class="form-control " value="<?php echo val($rs,'photo1',sget('photo1')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Discount Type</label>
											<select id="discounttypeid" name="discounttypeid" class="form-control mb-md" required>
												<?php get_lookup_options('discounttypeid',val($rs,'discounttypeid',sget('discounttypeid'))); ?>
											</select>
										</div>

										<div class="form-group mb-lg">
											<label>Real Amount</label>
											<input name="realamount" type="number" class="form-control " value="<?php echo moneyns(val($rs,'realamount',sget('realamount'))); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Discount Amount</label>
											<input name="amount" type="number" class="form-control " value="<?php echo moneyns(val($rs,'amount',sget('amount'))); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Usage Limit</label>
											<input name="usagelimit" type="number" class="form-control " value="<?php echo val($rs,'usagelimit',sget('usagelimit')); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Start Date</label>
											<input name="startdate" id="startdate" type="date" class="form-control datepicker" required value="<?php echo showdate(val($rs,'startdate',sget('startdate'))); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>End Date</label>
											<input name="enddate" id="enddate" type="date" class="form-control datepicker" required value="<?php echo showdate(val($rs,'enddate',sget('enddate'))); ?>" />
										</div>

										<div class="checkbox">
											<label>
												<input type="checkbox" id="excludesales" name="excludesales" <?php if(val($rs,'excludesales',sget('excludesales')) == '1') echo 'checked="checked"'; ?> value="1">
												Exclude Items on Sale
											</label>
										</div>

										<div class="form-group mb-lg">
											<label>Min Amount Spend</label>
											<input name="minamountspent" type="number" class="form-control " value="<?php echo moneyns(val($rs,'minamountspent',sget('minamountspent'))); ?>" />
										</div>

										<div class="form-group mb-lg">
											<label>Exclude Products</label>
											<input name="excludeproducts" id="excludeproducts" type="text" class="form-control " value="<?php echo val($rs,'excludeproducts',sget('excludeproducts')); ?>" />
										</div>

										<div class="checkbox">
											<label>
												<input type="checkbox" id="enabled" name="enabled" <?php if(val($rs,'enabled',sget('enabled')) == '1') echo 'checked="checked"'; ?> value="1">
												Coupon Enabled
											</label>
										</div>

										<div class="row">
											<div class="col-md-6">
												<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Cancel</button>
												<button type="submit" class="btn btn-primary">Save Changes</button>
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
						<div class="col-md-6">

							<section class="panel">
								<div class="panel-body">
									<h4>Preview</h4>

									<div class="row">
										<div class="col-md-4">
											<?php if(val($rs,'photo1',sget('photo1')) != '') echo '<img src="' . UPLOADURL . val($rs,'photo1',sget('photo1')) . '" style="width:100%;">'; ?>
										</div>
										<div class="col-md-8">
											<h4><?php echo val($rs,'title',sget('title')); ?></h4>
											<p><?php echo val($rs,'description',sget('description')); ?></p>

											<div class="row">
												<div class="col-md-3">
													<strong>VALUE</strong><Br />
													<?php echo money(val($rs,'realamount',sget('realamount'))); ?>
												</div>
												<div class="col-md-3">
													<strong>DISCOUNT</strong><Br />
													<?php echo money(val($rs,'amount',sget('amount'))); ?>
												</div>
												<div class="col-md-3">
													<strong>YOU SAVE</strong><Br />
													<?php echo calculate_saving_perc(val($rs,'realamount',sget('realamount')),val($rs,'amount',sget('amount'))); ?>
												</div>
												<div class="col-md-3">
													<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Show Code</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</section>
							<em>Save changes to refresh the preview.</em>


						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
