<?php $this->load->view('common/inc_html_header'); ?>
	<body>
		<section class="body">

			<?php $this->load->view('common/inc_header'); ?>

			<div class="inner-wrapper">

				<?php $this->load->view('common/inc_sidebar_left'); ?>

				<section role="main" class="content-body">
					<header class="page-header">
						<h2><?php echo $pagetitle; ?></h2>

						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="/">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span><?php echo $pagetitle; ?></span></li>
							</ol>

							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<div class="row">
						<div class="col-md-8">
							<section class="panel">
								<div class="panel-body">
									<?php display_messages(); ?>
									<form id="form" method="post">
			                     <input type="hidden" name="fp" value="1">

										<div class="form-group mb-lg">
											<label>Vendor Name</label>
											<input name="companyname" id="companyname" type="text" class="form-control " required value="<?php echo val($rs,'companyname',sget('companyname')); ?>" />
										</div>

										<div class="form-group mb-none">
											<div class="row">
												<div class="col-sm-6 mb-lg">
													<label>Physical Address</label>
													<input name="physical1" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical1',sget('physical1')); ?>"  />
													<input name="physical2" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical2',sget('physical2')); ?>"  />
													<input name="physical3" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical3',sget('physical3')); ?>"  />
													<input name="physical4" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'physical4',sget('physical4')); ?>"  />
													<input name="physicalcode" type="text" class="form-control " value="<?php echo val($rs,'physicalcode',sget('physicalcode')); ?>" placeholder="Code"  />
												</div>
												<div class="col-sm-6 mb-lg">
													<label>Postal Address</label>
													<input name="postal1" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'postal1',sget('postal1')); ?>"  />
													<input name="postal2" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'postal2',sget('postal2')); ?>"  />
													<input name="postal3" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'postal3',sget('postal3')); ?>"  />
													<input name="postal4" type="text" class="form-control address_field_space_below" value="<?php echo val($rs,'postal4',sget('postal4')); ?>"  />
													<input name="postalcode" type="text" class="form-control " value="<?php echo val($rs,'postalcode',sget('postalcode')); ?>" placeholder="Code"  />
												</div>
											</div>
										</div>

										<div class="form-group mb-none">
											<div class="row">
												<div class="col-sm-6 mb-lg">
													<label>Primary Tel.</label>
													<input name="tel1" type="text" class="form-control " value="<?php echo val($rs,'tel1',sget('tel1')); ?>"  />
												</div>
												<div class="col-sm-6 mb-lg">
													<label>Secondary Tel</label>
													<input name="tel2" type="text" class="form-control " value="<?php echo val($rs,'tel2',sget('tel2')); ?>"  />
												</div>
											</div>
										</div>

										<div class="form-group mb-none">
											<div class="row">
												<div class="col-sm-6 mb-lg">
													<label>Fax</label>
													<input name="fax" type="text" class="form-control " value="<?php echo val($rs,'fax',sget('fax')); ?>"  />
												</div>
												<div class="col-sm-6 mb-lg">
													<label>Email</label>
													<input name="email" type="text" class="form-control " value="<?php echo val($rs,'email',sget('email')); ?>"  />
												</div>
											</div>
										</div>

										<div class="form-group mb-lg">
											<label>Website</label>
											<input name="website" id="website" type="text" class="form-control " value="<?php echo val($rs,'website',sget('website')); ?>" />
										</div>

										<div class="checkbox">
											<label>
												<input type="checkbox" id="enabled" name="enabled" <?php if(val($rs,'enabled',sget('enabled')) == '1') echo 'checked="checked"'; ?> value="1">
												Vendor Enabled
											</label>
										</div>

										<div class="row">
											<div class="col-md-6">
												<button type="button" onclick="window.history.go(-1); return false;" class="btn btn-primary">Cancel</button>
												<button type="submit" class="btn btn-primary">Save Changes</button>
											</div>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>
					<!-- end: page -->

				</section>
			</div>

			<?php $this->load->view('common/inc_sidebar_right.php'); ?>
		</section>

		<?php $this->load->view('common/inc_html_footer.php',array('collapse_left_side' => false, 'hascharts' => false)); ?>
	</body>
</html>
