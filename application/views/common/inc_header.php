<!-- start: header -->
<header class="header">
   <div class="logo-container">
      <a href="/core" class="logo">
         <img src="/assets/images/logo.png" height="35" alt="" />
      </a>
      <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
         <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
      </div>
   </div>

   <!-- start: search & user box -->
   <div class="header-right">

      <form action="#" class="search nav-form">
         <div class="input-group input-search">
            <!--<input type="text" class="form-control" name="q" id="q" placeholder="Search...">
            <span class="input-group-btn">
               <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
            </span>-->
         </div>
      </form>

      <span class="separator"></span>


      <span class="separator"></span>

      <div id="userbox" class="userbox">
         <a href="#" data-toggle="dropdown">
            <figure class="profile-picture">
               <img src="<?php echo gravatar(); ?>" alt="Joseph Doe" class="img-circle" data-lock-picture="<?php echo gravatar(); ?>" />
            </figure>
            <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
               <span class="name"><?php echo user()->firstname . ' ' . user()->lastname; ?></span>
               <span class="role"><?php echo get_lookup_value(user()->userroleid);?></span>
            </div>

            <i class="fa custom-caret"></i>
         </a>

         <div class="dropdown-menu">
            <ul class="list-unstyled">
               <li>
                  <a role="menuitem" tabindex="-1" href="/core/logout"><i class="fa fa-power-off"></i> Logout</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <!-- end: search & user box -->
</header>
<!-- end: header -->
