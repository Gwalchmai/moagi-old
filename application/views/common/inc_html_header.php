<!doctype html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title><?php echo APPTITLE; ?></title>

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<!--<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">-->

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="/assets/vendor/bootstrap/css/bootstrap.css" />

		<link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="/assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="/assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="/assets/vendor/jquery-ui/jquery-ui.css" />
		<link rel="stylesheet" href="/assets/vendor/jquery-ui/jquery-ui.theme.css" />
		<link rel="stylesheet" href="/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="/assets/vendor/morris.js/morris.css" />
		<link rel="stylesheet" href="/assets/vendor/chartist/chartist.min.css" />
		<link rel="stylesheet" href="/assets/vendor/select2/css/select2.css" />
		<link rel="stylesheet" href="/assets/vendor/select2-bootstrap-theme/select2-bootstrap.css" />
		<link rel="stylesheet" href="/assets/vendor/jquery-datatables-bs3/assets/css/datatables.css" />
		<link rel="stylesheet" href="/assets/vendor/jstree/themes/default/style.css" />

		<link rel="stylesheet" href="/assets/vendor/dropzone/basic.css" />
		<link rel="stylesheet" href="/assets/vendor/dropzone/dropzone.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="/assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="/assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="/assets/stylesheets/theme-custom.css">
		<link rel="stylesheet" href="/assets/stylesheets/custom.css?id=<?php echo md5(date('Yis')); ?>">

		<script src="/assets/vendor/jquery/jquery.js"></script>

		<!-- Head Libs -->
		<script src="/assets/vendor/modernizr/modernizr.js"></script>
		<link rel="stylesheet" href="/assets/javascripts/redactor/redactor.css" />
	</head>
