<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">

   <div class="sidebar-header">
      <div class="sidebar-title">
         Navigation
      </div>
      <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
         <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
      </div>
   </div>

   <div class="nano">
      <div class="nano-content">
         <nav id="menu" class="nav-main" role="navigation">
            <ul class="nav nav-main">
               <li class="<?php if($mainmenu == "users") echo 'nav-active'; ?>"><a href="/admin/users"><i class="fa fa-user" aria-hidden="true"></i><span>Companies</span></a></li>
               <li class="<?php if($mainmenu == "tenders") echo 'nav-active'; ?>"><a href="/admin/tenders"><i class="fa fa-shopping-cart" aria-hidden="true"></i><span>Opportunities</span></a></li>
            </ul>
         </nav>
         <hr class="separator" />

      </div>
   </div>
</aside>
<!-- end: sidebar -->
