<script>

    $(document).ready(
        function()
        {
            $('.wysiwyg').redactor({
                imageUpload: '/adminutils/imageupload',
                imageGetJson: '/adminutils/getimages',
                pastePlainText: false,
                minHeight: <?php echo $minheight; ?>
            });
        }
    );
</script>
