<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xml:lang="en" xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
<style type="text/css">

@page {
    margin: 2cm;
}

body {
  font-family: sans-serif;
  margin: 0.5cm 0;
  text-align: left;
  font-size:10pt;
}

#header,
#footer {
  position: fixed;
  left: 0;
    right: 0;
    color: #aaa;
    font-size: 0.9em;
}

#header {
  top: 0;
    border-bottom: 0.1pt solid #aaa;
}

#footer {
  bottom: 0;
  border-top: 0.1pt solid #aaa;
}

#header table,
#footer table {
    width: 100%;
    border-collapse: collapse;
    border: none;
}

#header td,
#footer td {
  padding: 0;
    width: 50%;
}

.page-number {
  text-align: right;
}

.page-number:before {
  content: "Page " counter(page);
}

hr {
  page-break-after: always;
  border: 0;
}

.head {
color:#365f91;
font-weight:bold;
}

table.collapse {
  border-collapse: collapse;
  border: 0.7pt solid black;
}

table.collapse td {
  border: 0.7pt solid black;
}



h1 {
text-decoration:underline;
font-size:15pt;
}

h2 {
font-size:13pt;
}

#page {
width:700px;
}

.grey {
background-color: #e5e5e5;
}

#table tr td {
padding:2px;
font-size:8pt;
}

.actions { display: none; }

.table {
width:100%;
}

</style>
</head>
<body>

<div id="header">
  <table>
    <tr>
      <td></td>
      <td style="text-align: right;"><div class="page-number"></div></td>
    </tr>
  </table>
</div>

<div id="footer">

    <small>
    <?php echo APPTITLE; ?> - Document Printed <?php echo date('Y-m-d'); ?>
    </small>
</div>

<table width="500">
<tr>
    <td><img src="<?php echo base_url('/clientassets/img/logo.png'); ?>" height="80"></td>
    <td align="right"> Vusa House<br />5th Floor, Suite 2<Br /> Gandhi Square<Br />Johannesburg<br />
Tel no: 011 838 6700/6722<br />
email: admin@saibppbd.co.za
    </td>
</tr>
<tr>
    <td colspan="2"><h2>Tax Invoice</h2></td>
</tr>
<tr>
    <td colspan="2" style="background-color:#000000;height:3px;"></td>
</tr>
<tr>
    <td colspan="2" style="background-color:#ececec;"><strong>Customer Details</strong></td>
</tr>
<tr>
    <td>
        <table style="font-size:10px;">
        <tr>
            <td><strong>Attention</strong></td>
            <td><?php echo $cust[0]->name . ' ' . $cust[0]->surname; ?></td>
        </tr>
        <tr>
            <td><strong>Company Name</strong></td>
            <td><?php echo $cust[0]->companyname; ?></td>
        </tr>
        <tr>
            <td><strong>Address</strong></td>
            <td><?php echo $cust[0]->address; ?></td>
        </tr>
        <tr>
            <td><strong>Tel</strong></td>
            <td><?php echo $cust[0]->tel; ?></td>
        </tr>
        </table>
    </td>
    <td align="right" valign="top">
        <table style="font-size:10px;">
        <tr>
            <td><strong>Invoice No.</strong></td>
            <td><?php echo $trans[0]->payref; ?></td>
        </tr>
        <tr>
            <td><strong>Date</strong></td>
            <td><?php echo showdate($trans[0]->createdon); ?></td>
        </tr>
        <tr>
            <td><strong>VAT No.</strong></td>
            <td><?php echo $cust[0]->vatnumber; ?></td>
        </tr>
        </table>
    </td>
</tr>
<?php

  $description = '';
  if($trans[0]->amount > 1194)
    $description = '6-Month';
  else
    $description = '12-Month';
  if($cust[0]->userroleid == 4)
    $description .= ' Supplier Contract';
  else
    $description .= ' Procurer Contract';
?>
<tr>
    <td colspan="2">
        <br />
        <table width="100%" cellspacing="0" cellpadding="0" style="font-size:10px;">
        <tr style="background-color:#ececec;">
            <td><strong>Period</strong></td>
            <td><strong>Description</strong></td>
            <td><strong>Rate</strong></td>
            <td align="right"><strong>Total</strong></td>
        </tr>
        <tr>
            <td><?php echo $description; ?></td>
            <td>www.saibppd.co.za subscription</td>
            <td></td>
            <td align="right"><?php echo money($trans[0]->amount); ?></td>
        </tr>
        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="background-color:#ececec;">Total VAT Excl.</td>
            <td style="background-color:#ececec;" align="right"><?php echo money($trans[0]->amount); ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="background-color:#ececec;">VAT @ 14%</td>
            <td style="background-color:#ececec;" align="right"><?php echo money(0); ?></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td style="background-color:#ececec;"><strong>Incl.</strong></td>
            <td style="background-color:#ececec;" align="right"><strong><?php echo money($trans[0]->amount); ?></strong></td>
        </tr>
        </table>
        <p style="font-size:8px;">
          <center><strong>Payment COD on date of invoice.</strong></center>
        </p>

    </td>
</tr>
<tr>
    <td colspan="2" style="background-color:#000000;height:3px;"></td>
</tr>
<tr>
    <td colspan="2">
      <p style="font-size:8px;">
        <strong>SAIBPP VAT No. is not applicable</strong>
      </p>
    </td>
</tr>
<tr>
    <td colspan="2" style="background-color:#000000;height:3px;"></td>
</tr>
</table>



</body>
</html>
