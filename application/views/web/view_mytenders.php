<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<div class="content-area my-profile">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="my-address">
                <div class="row">
                <!-- Agent box list start -->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h1 class="title">
                            My Tenders
                            <a href="/createtender/0" class="btn button-sm pull-right">Create Tender</a>
                        </h1>
                        <hr>

                        <?php display_messages(); ?>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <table width="100%">
                          <thead>
                            <tr>
                              <td><strong>Tender #</strong></td>
                              <td><strong>Date Published</strong></td>
                              <td><strong>Time Published</strong></td>
                              <td><strong>Description</strong></td>
                              <td></td>
                            </tr>
                            <?php

                                foreach($rs as $tender)
                                {
                                  ?>
                                  <tr style="height:25px;">
                                    <td><?php echo $tender->tendernumber; ?></td>
                                    <td><?php echo showdate($tender->tenderdate); ?></td>
                                    <td><?php echo $tender->tenderdatetime; ?></td>
                                    <td><?php echo $tender->description; ?></td>
                                    <td align="right">
                                        <a href="/createtender/<?php echo encuri($tender->tenderid); ?>" class="link">Edit</a>
                                        &nbsp;&nbsp;|&nbsp;&nbsp;
                                        <a href="/mytenders/<?php echo encuri($tender->tenderid); ?>" class="link" onclick="return confirm('Are you sure you want to delete this tender?');">Delete</a>
                                    </td>
                                  </tr>
                                  <?php
                                }

                            ?>
                          </thead>
                          </table>
                        </div>


                    </div>
                </div>
              </div>
                <!-- Agent box list end -->
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
