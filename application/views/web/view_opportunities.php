<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<div class="content-area my-profile">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="my-address">
                <div class="row">
                <!-- Agent box list start -->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <h1 class="title">
                            Opportunities
                        </h1>
                        <hr>

                        <?php display_messages(); ?>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                          <table width="100%" class="table table-bordered" style="padding: 10px; !important;">
                          <thead>
                            <tr>
                              <td style="padding: 10px; !important;"><strong>Category</strong></td>
                              <td style="padding: 10px; !important;"><strong>Tender Description</strong></td>
                              <td style="padding: 10px; !important;"><strong>Tender No.</strong></td>
                              <td style="padding: 10px; !important;"><strong>Date Published</strong></td>
                              <td style="padding: 10px; !important;"><strong>Closing Date</strong></td>
                              <td style="padding: 10px; !important;"><strong>Compulsory Briefing<br />Session</strong></td>
                              <td style="padding: 10px; !important;"></td>
                            </tr>
                          </thead>
                            <?php
                                $rs = $this->tender_model->find_all();
                                foreach($rs as $tender)
                                {
                                  ?>
                                  <tr style="height:25px;">
                                    <td style="padding: 10px; !important;"><?php echo get_lookup_value($tender->tendercategoryid); ?></td>
                                    <td style="padding: 10px; !important;"><?php echo $tender->description; ?></td>
                                    <td style="padding: 10px; !important;"><?php echo $tender->tendernumber; ?></td>
                                    <td style="padding: 10px; !important;"><?php echo showdate($tender->tenderdate); ?></td>
                                    <td style="padding: 10px; !important;"><?php echo showdate($tender->tenderclosedate); ?></td>
                                    <td style="padding: 10px; !important;"><?php echo showdate($tender->briefingsessiondate); ?></td>
                                    <td align="right" style="padding: 10px; !important;">
                                        <a href="/viewtender/<?php echo encuri($tender->tenderid); ?>" class="link">View</a>
                                    </td>
                                  </tr>
                                  <?php
                                }

                            ?>
                          </thead>
                          </table>
                        </div>


                    </div>
                </div>
              </div>
                <!-- Agent box list end -->
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
