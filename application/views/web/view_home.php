<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<?php $this->load->view('web/common/inc-banner'); ?>

&nbsp;<br/>

<!-- Featured properties start -->
<div class="content-area featured-properties">
    <div class="container">
        <div class="row">
            <div class="filtr-container">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1, 2, 3">
                    <div class="property">
                        <!-- Property img -->
                        <a href="http://www.pareto.co.za" class="property-img" target="_blank">
                            <div class="property-tag button alt featured">Sponsored By</div>
                            <img src="/clientassets/img/ads/pareto.png" alt="properties-1" class="img-responsive">
                        </a>
                        <!-- Property content -->
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1, 2, 3">
                    <div class="property">
                        <!-- Property img -->
                        <a href="http://www.saibpp.co.za" class="property-img" target="_blank">
                            <div class="property-tag button alt featured">Courtesy Of</div>
                            <img src="/clientassets/img/saibpp_logo_advert.jpg" alt="properties-1"
                                 class="img-responsive">
                        </a>
                        <!-- Property content -->
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12  filtr-item" data-category="1, 2, 3">
                    <div class="property">
                        <!-- Property img -->
                        <a href="http://www.pareto.co.za" class="property-img" target="_blank">
                            <div class="property-tag button alt featured">Sponsored By</div>
                            <img src="/clientassets/img/ads/pareto.png" alt="properties-1" class="img-responsive">
                        </a>
                        <!-- Property content -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Featured properties end -->

<!-- News start -->
<div class="categories">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1><span>Featured</span> Articles</h1>
        </div>
        <div class="clearfix"></div>
        <div class="row wow">
            <div class="col-lg-7 col-md-7 col-sm-12">
                <div class="row">
                    <div class="col-sm-6 col-pad wow fadeInLeft delay-04s">
                        <div class="category">
                            <a href="http://www.moagimag.co.za/issue1/index.html?page=52" target="_blank">
                                <div class="category_bg_box cat-1-bg"
                                     style="background-image: url('/clientassets/img/news/property_markets.jpg');">
                                    <div class="category">
                                    <span class="category-content">
                                        <!-- <a href="http://www.moagimag.co.za/issue1/index.html?page=52" class="btn button-sm button-theme" target="_blank">View Article</a> -->
                                    </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-6 col-pad wow fadeInLeft delay-04s">
                        <div class="category">
                            <a href="http://www.moagimag.co.za/issue1/index.html?page=32" target="_blank">
                                <div class="category_bg_box cat-2-bg"
                                     style="background-image: url('/clientassets/img/news/parks_tau.jpg');">
                                    <div class="category">
                                    <span class="category-content">
                                         <!-- <a href="http://www.moagimag.co.za/issue1/index.html?page=32"
                                            class="btn button-sm button-theme" target="_blank">View Article</a> -->
                                    </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="col-sm-12 col-pad wow fadeInUp delay-04s">
                        <div class="category">
                            <a href="http://www.moagimag.co.za/issue1/index.html?page=56" target="_blank">
                                <div class="category_bg_box cat-3-bg"
                                     style="background-image: url('/clientassets/img/news/spatial_design.jpg');">
                                    <div class="category">
                                    <span class="category-content">
                                        <!-- <a href="http://www.moagimag.co.za/issue1/index.html?page=56"
                                           class="btn button-sm button-theme " target="_blank">View Article</a> -->
                                    </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-md-5 col-sm-12 col-pad wow fadeInRight delay-04s">
                <div class="category">
                    <a href="http://www.moagimag.co.za/issue1/index.html?page=20" target="_blank">
                        <div class="category_bg_box category_long_bg cat-4-bg"
                             style="background-image: url('/clientassets/img/news/andile_mazwai.jpg');">
                            <div class="category">
                            <span class="category-content">
                                 <!-- <a href="http://www.moagimag.co.za/issue1/index.html?page=20"
                                    class="btn button-sm button-theme " target="_blank">View Article</a> -->
                            </span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- News end-->

<div class="clearfix"></div>

<!-- Counters strat -->
<div class="counters">
    <div class="container">

        <div class="row">
            <div class="col-md-4 col-sm-6 bordered-right">
                <div class="counter-box">
                    <i class="flaticon-tag"></i>
                    <h1 class="counter"><?php echo get_user_type_count(5); ?></h1>
                    <p>Listed Procurers</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 bordered-right">
                <div class="counter-box">
                    <i class="flaticon-symbol-1"></i>
                    <h1 class="counter"><?php echo get_user_type_count(4); ?></h1>
                    <p>Listed Suppliers</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 bordered-right">
                <div class="counter-box">
                    <i class="flaticon-people"></i>
                    <h1 class="counter">0</h1>
                    <p>Verified suppliers</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Counters end -->

<?php
$this->load->view('web/common/inc-partners');
$this->load->view('web/common/inc-footer');
$this->load->view('web/common/inc-html-footer');
?>
