<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<?php //$this->load->view('web/common/inc-search'); ?>

<!-- About city estate start -->
<div class="about-city-estate">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="about-text">
                    <div class="main-title-2">
                        <h1>Verification Turnaround Times </h1>
                        <p><strong>Basic verification: 3 days </strong></p>
                        <ul>
                            <li>Company details, including financial information, on receipt of appropriate consent
                                form</li>
                            <li>B-BBEE status, on receipt of appropriate consent form</li>
                        </ul>

                        <p><strong>Full verification: 7 days </strong></p>
                        <ul>
                            <li>Professional Associations and Accreditations</li>
                            <li>Trade References</li>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- About city estate end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
