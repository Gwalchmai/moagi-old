<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<td class="agent-section content-area">
<div class="container">
<th class="row">
<th class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <!-- Agent box list start -->
<th class="agent-box-big clearfix">
<th class="col-lg-12 col-md-12 col-sm-12 agent-content">
    <h1 class="title">
        <?php echo val($rs, 'companyname'); ?>
        <a href="/profile" class="btn button-sm pull-right">Edit</a>
        <span class="pull-right">&nbsp;</span>
        <a href="/changepassword" class="btn button-sm pull-right">Change Password</a>
        <span class="pull-right">&nbsp;</span>
        <a href="/mytenders/0" class="btn button-sm pull-right">My Tenders</a>
        <span class="pull-right">&nbsp;</span>
        <a href="/mynotifications" class="btn button-sm pull-right">My Notifications</a>
    </h1>
    <hr>

    <?php display_messages(); ?>

    <div class="row">
        <div class="col-md-5"><strong>Trading Name:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'tradingname', 'N/A'); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>SAIBPP Member:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'saibppmemberid')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Company Type:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'companytypeid')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Location:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'companyaddress'); ?><?php echo get_lookup_value(val($rs, 'locationid')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Industry Sub-Sector:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'industrysectorid')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Industry:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'propertysubsectorid')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Property Associates:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value_names(val($rs, 'propertyassociations')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Bank Confirmation Letter:</strong></div>
        <?php $fieldname = 'bankconfirmationletter'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Company Logo:</strong></div>
        <?php $fieldname = 'companylogo'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Proof of business address (RICA documents):</strong></div>
        <?php $fieldname = 'imageofpremises'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Years Operating:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'yearsoperating'); ?></div>
    </div>

    <p>
    <h4 class="title">Contact Details</h4>
    <hr/>
    <div class="row">
        <div class="col-md-5"><strong>Contact Title:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'titleid')); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Contact Name:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'name'); ?> <?php echo val($rs, 'surname'); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Contact Designation:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'designation'); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Contact Tel:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'tel'); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Contact Email:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'email'); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Company Email:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'companyemail'); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Company Tel:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'companytel'); ?></div>
    </div>


    <h4 class="title">Trade References</h4>
    <hr/>
    <?php for ($i = 1; $i <= 3; $i++) {
        ?>
        <div class="row">
            <div class="col-md-5"><strong>Company Name (<?php echo $i; ?>):</strong></div>
            <div class="col-md-7"><?php echo val($rs, 'tr_companyname' . $i); ?></div>
        </div>
        <div class="row">
            <div class="col-md-5"><strong>Description Of project, service, product provision
                    (<?php echo $i; ?>):</strong></div>
            <div class="col-md-7"><?php echo val($rs, 'tr_description' . $i); ?></div>
        </div>
        <div class="row">
            <div class="col-md-5"><strong>Client Project Manager (<?php echo $i; ?>):</strong></div>
            <div class="col-md-7"><?php echo val($rs, 'tr_contact' . $i); ?></div>
        </div>
        <div class="row">
            <div class="col-md-5"><strong>Contact Number (<?php echo $i; ?>):</strong></div>
            <div class="col-md-7"><?php echo val($rs, 'tr_contactnumber' . $i); ?></div>
        </div>
        <div class="row">
            <div class="col-md-5"><strong>Contact Email (<?php echo $i; ?>):</strong></div>
            <div class="col-md-7"><?php echo val($rs, 'tr_contactemail' . $i); ?></div>
        </div>
        <div class="row">
            <div class="col-md-5"><strong>Project Cost (<?php echo $i; ?>):</strong></div>
            <div class="col-md-7">R <?php echo val($rs, 'tr_projectcost' . $i); ?></div>
        </div>
        <br/>
        <?php
    }
    ?>

    <p>
    <h4 class="title">Enterprise Credentials</h4>
    <hr/>
    <div class="row">
        <div class="col-md-5"><strong>Black Ownership:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'blackownershippercent'); ?>%</div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Black Women Ownership:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'blackwomenownershippercent'); ?>%</div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>B-BBEE Status:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'beestatusid')); ?></div>
    </div>

    <div class="row">
        <div class="col-md-5"><strong>Enterprise Size:</strong></div>
        <div class="col-md-7"><?php echo htmlentities(get_lookup_value(val($rs, 'enterprisesizeid'))); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>BBBEE Certificate Type:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'bbbeecerttypeid')); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Verification Auditor:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'verificationauditorid')); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Empowering Supplier:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'empoweringsupplierid')); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Black Company Status:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'blackcompanystatusid')); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>VAT Registered:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'vatregisteredid')); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>VAT Number:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'vatnumber'); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Company Registration Number:</strong></div>
        <div class="col-md-7"><?php echo val($rs, 'companyregno'); ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Business Classification:</strong></div>
        <div class="col-md-7"><?php echo get_lookup_value(val($rs, 'businessclassificationid')); ?></div>
    </div>

    <h4 class="title">Enterprise Documents<span class="pull-right">Available</span></h4>
    <hr/>
    <div class="row">
        <div class="col-md-5"><strong>CK Documents:</strong></div>
        <?php $fieldname = 'ckdocuments'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>BBBEE Certificate:</strong></div>
        <?php $fieldname = 'bbbeecertificate'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Tax Clearance Certificate:</strong></div>
        <?php $fieldname = 'taxclearancecert'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Workman's Compensation:</strong></div>
        <?php $fieldname = 'workmanscompensation'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Public Liability:</strong></div>
        <?php $fieldname = 'publicliability'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Trade References:</strong></div>
        <?php $fieldname = 'tradingreferences'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>
    <div class="row">
        <div class="col-md-5"><strong>Other Documents:</strong></div>
        <?php $fieldname = 'otherdocuments'; ?>
        <div class="col-md-7"><?php if (val($rs, $fieldname, sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs, $fieldname, sget($fieldname)) . '" class="link" target="_blank">View</a>'; ?></div>
    </div>

    <p>
    <h4 class="title">Directors</h4>
    <hr>
    <div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th><strong>Title</strong></th>
            <th><strong>Name</strong></th>
            <th><strong>Surname</strong></th>
            <th><strong>Qualifications</strong></th>
            <th><strong>Email Address</strong></th>
            <th><strong>Telephone Number</strong></th>
        </tr>
        </thead>
        <tbody>

        <?php
        $rssd = $this->sd_model->find_all(array('userid' => val($rs, 'userid')));
        foreach ($rssd

        as $row) {
        ?>

        <tr>
            <td><?php echo get_lookup_value($row->sd_titleid); ?></td>
            <td><?php echo $row->sd_firstname; ?></td>
            <td><?php echo $row->sd_lastname; ?></td>
            <td><?php if ($row->sd_directorsqualification != '') echo '<a href="' . UPLOADURL . $row->sd_directorsqualification . '" class="link" target="_blank">View</a>'; ?></td>
            <td><?php echo $row->sd_email; ?></td>
            <td><?php echo $row->sd_telno; ?></td>
        </tr>
        <?php
        }

        ?>
        </tbody>
    </table>
    </div>
</th>

    <!-- Agent box list end -->

    <!-- Agent section end -->

    <?php $this->load->view('web/common/inc-partners'); ?>

    <?php $this->load->view('web/common/inc-footer'); ?>

    <?php $this->load->view('web/common/inc-html-footer'); ?>
