<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Login</span></h1>
                        </div>
                        <?php display_messages(); ?>
                        <!-- Form start -->
                        <form method="post">
                           <input type="hidden" name="fp" value="1">
                            <div class="form-group">
                                <input type="email" name="email" class="input-text" placeholder="Email Address" value="<?php echo sget('email'); ?>">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="input-text" placeholder="Password">
                            </div>
                            <div class="checkbox">
                                <div class="ez-checkbox pull-left">
                                    <label>
                                        <input type="checkbox" class="ez-hide">
                                        Remember me
                                    </label>
                                </div>
                                <a href="/forgotpassword" class="link-not-important pull-right">Forgot Password</a>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button-md button-theme btn-block">login</button>
                            </div>
                        </form>
                        <!-- Form end -->
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>
                            New to <?php echo APPTITLE; ?>? <a href="/registerquick">Sign up now</a>
                        </span>
                        <center>or</center>
                        <span>
                           <a href="/">return to website</a>
                        </span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<?php $this->load->view('web/common/inc-html-footer'); ?>
