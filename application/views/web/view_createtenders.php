<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<div class="content-area my-profile">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="my-address">
                    <h1 class="title">
                        <?php echo $pagetitle; ?>
                    </h1>
                    <hr>

                    <?php display_messages(); ?>
                    <div class="row">
                      <div class="col-md-12">
                         <div class="alert alert-danger" id="validationErrors" style="display:none;">
                             <button type="button" class="close" data-dismiss="alert">&times;</button>
                             <strong>Validation Errors</strong> - Please ensure that you complete all the required fields correctly.
                         </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                         <form id="form" method="post" enctype="multipart/form-data">
                             <input type="hidden" name="fp" value="1">

                             <div class="form-group">
                                <label>Category <em class="req">*</em></label>
                                <?php $fieldname = "tendercategoryid"; ?>
                                <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
                                    <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
                                </select>
                             </div>

                             <div class="form-group">
                                 <label>Tender Number <em class="req">*</em> </label>
                                 <?php $fieldname = "tendernumber"; ?>
                                 <input type="text" required="required" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Description </label>
                                 <?php $fieldname = "description"; ?>
                                 <textarea class="input-text" name="<?php echo $fieldname; ?>"><?php echo val($rs,$fieldname,sget($fieldname)); ?></textarea>
                             </div>

                             <div class="form-group">
                                 <label>Date Published <em class="req">*</em> </label>
                                 <?php $fieldname = "tenderdate"; ?>
                                 <input type="date" required="required" class="input-text datepicker" name="<?php echo $fieldname; ?>" value="<?php if(val($rs,$fieldname,sget($fieldname)) == '') echo date('Y-m-d'); else echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Time Published</label>
                                 <?php $fieldname = "tenderdatetime"; ?>
                                 <input type="time" class="input-text timepicker" name="<?php echo $fieldname; ?>" value="<?php if(val($rs,$fieldname,sget($fieldname)) == '') echo date('H:m'); else echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Closing Date </label>
                                 <?php $fieldname = "tenderclosedate"; ?>
                                 <input type="date" class="input-text datepicker" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Closing Time </label>
                                 <?php $fieldname = "tenderclosedatetime"; ?>
                                 <input type="time" class="input-text timepicker" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Briefing Session Date </label>
                                 <?php $fieldname = "briefingsessiondate"; ?>
                                 <input type="date" class="input-text datepicker" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Briefing Session Time </label>
                                 <?php $fieldname = "briefingsessiontime"; ?>
                                 <input type="time" class="input-text timepicker" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
                             </div>

                             <div class="form-group">
                                 <label>Tender Document</label>
                                 <?php $fieldname = "tenderdocument"; ?>

                                 <br /><label for="<?php echo $fieldname; ?>" class="custom-file-upload">Browse</label>
                                 <input id="<?php echo $fieldname; ?>" type="file" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">

                                 <?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>
                             </div>

                             <a href="/mytenders" class="btn button-md button-theme">Cancel </a>
                             <a href="#" onclick="SaveTender();" class="btn button-md button-theme">Save </a>
                         </form>
                       </div>
                     </div>
                  </div>
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>

<script>

  function SaveTender()
  {
    var validated = true;
    $(':input[required]:visible').each(function() {
        if ($(this).val() == '') {
           $(this).css("border", "2px solid red");
           validated = false;
       }
    });

    if(validated)
    {
       $('#validationErrors').hide();
       $('#form').submit();
       return true;
    }
    else {
       $('#validationErrors').show();
    }
  }

</script>
