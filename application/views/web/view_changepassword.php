<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Change Password</span></h1>
                        </div>
                        <?php display_messages(); ?>
                        <!-- Form start -->
                        <form method="post">
                           <input type="hidden" name="fp" value="1">
                            <div class="form-group">
                                <input type="password" name="newpassword" class="input-text" placeholder="New Password">
                            </div>
                            <div class="form-group">
                                <input type="password" name="confirmnewpassword" class="input-text" placeholder="Confirm New Password">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="button-md button-theme btn-block">Change Password</button>
                            </div>
                        </form>
                        <!-- Form end -->
                    </div>                    
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<?php $this->load->view('web/common/inc-html-footer'); ?>
