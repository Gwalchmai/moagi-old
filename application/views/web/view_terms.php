<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<?php //$this->load->view('web/common/inc-search'); ?>

<!-- About city estate start -->
<div class="about-city-estate">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="about-text">
                    <div class="main-title-2">
                        <h1>The SAIBPP Business Directory Terms and Conditions of use</h1>

                        <p><strong>Terms and Conditions for the Use of this Business Directory</strong></p>
                        <p>Please read the following terms and conditions of use before using this Business Directory. In using this Business Directory – or any other branded Business Directory that is powered by the SAIBPP Business Directory, you agree to these terms and conditions of use. The SAIBPP Business Directory reserves the right to update these terms and conditions at any time without notice to you.</p>

                        <p><strong>Definitions</p></strong>
                        <p>The terms “South African Institute for Black Property Practitioners”, “SAIBPP”, “the SAIBPP Business Directory", “we”, “us”, “our” and “ours” when used in these Terms and Conditions mean the SAIBPP Business Directory. The terms “you”, “your” and “yours” when used in these Terms and Conditions mean any user of this Business Directory.</p>

                        <p><strong>Use of this Business Directory</p></strong>
                        <p>This Business Directory is owned and operated by SAIBPP, trading as South African Institute for Black Property Practitioners. SAIBPP (Registration No: 1997/006816/08) is incorporated in the Republic of South Africa with its registered address at Vusa House, 5th Floor, Suite 2, Gandhi Square, Johannesburg - Postal Address (PO Box 62329, Marshalltown, 2017).</p>

                        <p>If you have any queries about this Business Directory, please contact the SAIBPP Business Directory by sending us an email at admin@ saibppbd.co.za.</p>

                        <p><strong>Intellectual Property Rights</p></strong>
                        <p>This Business Directory contains material which is owned by or licensed to the SAIBPP Business Directory. This material includes, but is not limited to, the design, layout, look, appearance, graphics and data. It is protected by intellectual property laws including, but not limited to, copyright.</p>
                        <p>You may download, view, copy and print information and documentation held on this Business Directory provided that:</p>
                        <p>Materials are used solely for your own personal use or for product/supplier research in connection with your business or your employer’s business. Commercial use is not permitted.</p>
                        <p>Materials may not be altered or modified in any way without prior written permission from the SAIBPP Business Directory.</p>
                        <p>You may not otherwise, without express permission from us, copy, distribute, sell or publish any of the content of the Business Directory. Unauthorised use of this Business Directory may give rise to a claim for damages and/or be a criminal offence.</p>

                        <p><strong>Changes to this agreement</p></strong>
                        <p>From time to time we may change these terms without prior notice to you. The current terms can always be found atwww.saibppbd.co.za By using this Business Directory after we have posted any revisions to these terms, you accept the terms as revised.</p>

                        <p><strong>Privacy</p></strong>
                        <p>We respect your privacy. Please review our Privacy Policy for a description of the information we collect and how we use it.</p>

                        <p><strong>Professional advice</p></strong>
                        <p>Information provided on pages held on this Business Directory and in any documents downloaded from this Business Directory is provided for general information only. It is subject to change without notice. The SAIBPP Business Directory is not responsible for any inaccuracies and makes no representation and gives no warranty as to its accuracy. Since the information was not prepared for you personally, it is not intended to form recommendations or advice. It is your sole responsibility to satisfy yourself that the information is suitable for your purposes.</p>
                        <p>Nothing on this Business Directory is intended to be nor should be construed as an offer to enter into a contractual relationship unless a user opts to utilise Value Add Services, provide Advertising or enter into a Premium Membership Agreement.</p>

                        <p><strong>Trademarks</p></strong>
                        <p>Your use of this site and any materials downloaded, viewed, copied or printed does not authorise you to use any names or trademarks of the SAIBPP Business Directory, its trading partners or associates.</p>

                        <p><strong>Errors and downtime</p></strong>
                        <p>Errors may appear from time to time on the Business Directory and some information on the Business Directory may be out of date. Before relying on information you have found on the Business Directory, please confirm any facts that are important to your decision. From time to time the Business Directory may be inaccessible or inoperable for any reason, including, without limitation: (i) equipment malfunctions; (ii) periodic maintenance procedures or repairs which we may undertake from time to time; or (iii) causes beyond our control or which we cannot reasonably foresee.</p>

                        <p><strong>Links to partners and sponsors</p></strong>
                        <p>These Terms and Conditions apply to www.saibppbd.co.za and all the branded websites that are operated by the SAIBPP Business Directory in collaboration with its key partners. The Business Directory may contain hyperlinks to partners and sponsors that are not operated by the SAIBPP Business Directory. These hyperlinks are provided for your reference and convenience only and you should note that we do not have any control over these other websites. We cannot, therefore, be held liable for any materials or representations in respect to such websites. All links are used at your own risk. In evoking links to other websites, you agree to accept these conditions of use.</p>
                        <p><strong>Links from other partners and sponsors</p></strong>
                        <p>Persons providing access to this Business Directory via links from another Business Directory or website, are solely responsible for the content, accuracy, opinions expressed, privacy policies, products or services of, or availability through, the source websites and for any representations made or impressions created concerning the SAIBPP Business Directory.</p>
                        <p><strong>Special Offers/Coupons</p></strong>
                        <p>Advertisers on the SAIBPP Business Directory may include special offers within their paid for listings. the SAIBPP Business Directory does not take responsibility for or offer assistance in relation to these special offers. Other terms and conditions of the advertiser may apply. You should check any terms and conditions with the advertiser before use.</p>

                        <p><strong>Usernames and passwords</p></strong>
                        <p>To obtain access to certain services through this Business Directory, you may be given an opportunity to register with us. As part of any such registration process, you will select/be given a username and password. You agree that the information you supply during that registration process will be accurate and complete and that you will not register under the name of, nor attempt to enter our site under the name of, another person. We reserve the right to reject or terminate any username that we deem offensive. You will be responsible for preserving the confidentiality of your password and will notify us by email of any known or suspected unauthorised use of your account.</p>
                        <p>Any misrepresentation of information provided through the registration process whether intentionally or neglectfully, the owners of the SAIBPP Business Directory reserve the right to remove your registration from this Business Directory with no consequential costs or liabilities accruing to the owners of the SAIBPP Business Directory for whatever reason.</p>
                        <p><strong>Security</p></strong>
                        <p>Users may want access Value Add Services which are optional and available to all members. We make use of the Payfast Payment Gateway for any credit card transactions. If you prefer not to send your credit card number through the Internet, you can place your order by email.</p>

                        <p><strong>Connecting to the Business Directory</p></strong>
                        <p>The SAIBPP Business Directory has been optimised to operate with all major Web Browsers. You shall be solely responsible for providing, maintaining and ensuring compatibility with the Business Directory, all hardware, software, electrical and other physical requirements for your use of the Business Directory, including, without limitation, telecommunications and Internet access connections and links, web browsers or other equipment, programs and services required to access and use the site.</p>
                        <p><strong>No warranty</p></strong>
                        <p>The site is provided "as is" without warranty of any kind, express or implied. Use of the site is at your sole risk. We do not warrant that your use of the site will be uninterrupted or error free, nor do we make any warranty as to any results that may be obtained by use of the site. We make no warranties, express or implied, including, without limitation, any implied warranties of merchantability, merchantable quality, fitness for a particular purpose, non-infringement, effectiveness, completeness, accuracy, and title. Limitation of liability: If you are dissatisfied with the site, your sole and exclusive remedy shall be to discontinue use of the site. In no event shall our total liability for direct damages exceed the total fees paid by you to us. Moreover, under no circumstances shall we be liable to you or any other person for any indirect, incidental, consequential, special or punitive damages for any matter arising from or relating to this agreement, the site or the Internet generally, including, without limitation, your use or inability to use the site, any changes to or inaccessibility of the site, delay, failure, unauthorised access to or alteration of any transmission or data, any material or data sent or received or not sent or received, any transaction or agreement entered into through the site, or any data or material from a third person accessed on or through the site, whether such liability is asserted on the basis of contract, tort or otherwise.</p>
                        <p><strong>Indemnity</p></strong>
                        <p>You agree to indemnify, defend and hold us, including our directors, officers, employees and agents harmless, from and against any action, cause, claim, damage, debt, demand or liability, including reasonable costs and attorney’s fees, asserted by any person or company, arising out of or relating to your breach of these terms.</p>
                        <p><strong>Cold Calling and Product Offerings</p></strong>
                        <p>The Business Directory has been setup for the express use of procurement networking and the Terms of Use expressly prohibit any member from extracting membership information of other members for the purposes of building marketing lists. Any such use will be governed by a fair use policy and any member contravening this fair use policy risks being removed from the network.</p>
                        <p><strong>Refund Policy – Premium Membership Subscriptions</p></strong>
                        <p>This Refund Policy is only applicable to Premium Membership which is an annual membership. In line with the Consumer Protection Act the SAIBPP Business Directory will refund membership fees only in event of the Business Directory technology failing or solutions that are made available to Premium Members not being operational for a period of more than 7days. This refund will also only be applicable to membership fees that are paid for future access and not applicable to paid fees that relate to historical membership. In event of cancellation within the first month membership, the equivalent of a single month’s upgrade cost will apply as an administration cost for cancelling the membership upgrade.</p>
                        <p><strong>Protection of Private Information (POPI) Act 4 of 2013</p></strong>
                        <p>By registering as a supplier on the Business Directory you acknowledge that your company profile information is in the public domain and accessible to other users of the Business Directory. The Business Directory is a marketing platform for your company to reach buyers of other companies and by registering as a member you acknowledge that your company profile is in the public domain.</p>
                        <p>In terms of the POPI Act the SAIBPP Business Directory warrants that all confidential information such as personal identification numbers of directors and company banking details will be masked and only made available to an authorised administrator. Furthermore the SAIBPP Business Directory warrants that your company information will not be provided to any third party for the purposes of bulk unsolicited marketing communication. The SAIBPP Business Directory may make your company information available to its partners for the purposes of market research.</p>
                        <p>By being a member of the SAIBPP Business Directory you also accept that the SAIBPP Business Directory communicates with its members from time to time through email, letters, facsimiles and direct telephone calls. The purpose of this communication serves many purposes from keeping you up to date on latest developments on the Business Directory solutions, as well as discounted offers and solutions available to you through our Partner network. At anytime you can choose to opt out of this member communication by contacting the SAIBPP Business Directory via email.</p>
                        <p><strong>Governing law</p></strong>
                        <p>These Terms and Conditions are governed by and construed in accordance with the laws of the Republic of South Africa. You agree to submit any dispute arising out of your use of this Business Directory to the exclusive jurisdiction of the courts of the Republic of South Africa.</p>
                        <p><strong>Important Notices</p></strong>
                        <p>These Terms and Conditions apply to users who are also recognized as consumers for purposes of the Consumer Protection Act, 68 of 2008 (the “CPA”).</p>
                        <p>These Terms and Conditions contain provisions that -</p>
                        <ul>
                           <li>may limit the risk or liability of the SAIBPP Business Directory or a third party; and/or</li>
                           <li>may create risk or liability for the user; and/or</li>
                           <li>may compel the user to indemnify the SAIBPP Business Directory or a third party; and/or</li>
                           <li>serves as an acknowledgement, by the user, of such facts.</li>
                        </ul>
                        <p>Your attention is drawn to the importance of these Terms and Conditions and should be carefully noted.</p>
                        <p>If there is any reference in these Terms and Conditions that you do not understand, it is your responsibility to ask the SAIBPP Business Directory to explain any aspect of uncertainty to you before you accept the Terms and Conditions or continue using the Business Directory.</p>
                        <p>Nothing in these Terms and Conditions is intended or must be understood to unlawfully restrict, limit or avoid any right or obligation, as the case may be, created for either you or the SAIBPP Business Directory.</p>
                        <p>the SAIBPP Business Directory permits the use of this Business Directory subject to the Terms and Conditions. By using this Business Directory in any way, it is deemed that you have accepted all the Terms and Conditions unconditionally. You must not use this Business Directory if you do not agree to the Terms and Conditions.</p>
                        <p><strong>Registration and use of the Business Directory</p></strong>
                        <p>To register as a user, you must provide a unique username and password and provide certain company information and personal details to the SAIBPP Business Directory. You will need to use your unique username and password to access the Business Directory in order to purchase Goods.</p>
                        <p>You agree and warrant that your username and password shall:</p>
                        <ul>
                           <li>be used for personal use only; and</li>
                           <li>not be disclosed by you to any third party.</li>
                        </ul>
                        <p>For security purposes, you agree to enter the correct username and password whenever ordering Goods, failing which you will be denied access.</p>
                        <p>You agree that, once the correct username and password relating to your account have been entered, irrespective of whether the use of the username and password is unauthorised or fraudulent, you will be liable for payment of such order, save where the order is cancelled by you in accordance with these Terms and Conditions.</p>
                        <p>You agree to notify the SAIBPP Business Directory immediately upon becoming aware of or reasonably suspecting any unauthorised access to or use of your username and password and to take steps to mitigate any resultant loss or harm.</p>
                        <p>By using the Business Directory, you warrant that you are of full legal capacity. If you are not legally permitted to enter into a binding agreement, then you may use the Business Directory only with the involvement and supervision of such person who agrees to be bound to these Terms and Conditions and to be liable and responsible for you and all your obligations under these Terms and Conditions.</p>
                        <p>You agree that you will not in any way use any device, software or other instrument to interfere or attempt to interfere with the proper working of the Business Directory. In addition, you agree that you will not in any way use any robot, spider, other automatic device, or manual process to monitor, copy, distribute or modify the Business Directory or the information contained herein, without the prior written consent from an authorised the SAIBPP Business Directory representative (such consent is deemed given for standard search engine technology employed by Internet search engines to direct Internet users to this Business Directory).</p>
                        <p>You may not use the Business Directory to distribute material which is defamatory, offensive, contains or amounts to hate speech or is otherwise unlawful.</p>
                        <p>You may not in any way display, publish, copy, print, post or otherwise use the Business Directory and/or the information contained therein without the express prior written consent of an authorised the SAIBPP Business Directory representative.</p>
                        <p><strong>Payment</p></strong>
                        <p>We are committed to providing secure online payment facilities through the secure Payfast payment gateway. All transactions are encrypted using appropriate encryption technology.</p>
                        <p>Payment can be made for Goods via -</p>
                        <ul>
                        <li>debit card;</li>
                        <li>credit card: where payment is made by credit card, we may require additional information in order to authorise and/or verify the validity of payment. In such cases we are entitled to withhold service until such time as the additional information is received by us and authorisation is obtained by us for the amounts. If we do not receive authorisation your order for the service will be cancelled. You warrant that you are fully authorised to use the credit card supplied
                        for purposes of paying the Goods. You also warrant that your credit card has sufficient available funds to cover all the costs incurred as a result of the services used on the Business Directory;</li>
                     </ul>

                        <p>You may contact us via email at admin@saibppbd.co.za to obtain a full record of your payment. We will also send you email communications about your order and payment.</p>
                        <p>Once you have selected your payment method and you accept these Terms and Conditions, you will be directed to a link to a secure site for payment.</p>
                        <p><strong>Electronic communications</p></strong>
                        <p>When you visit the Business Directory or send emails to us, you consent to receiving communications from us or any of our divisions or partners electronically in accordance with our privacy policy.</p>
                        <p><strong>Limitation of liability</p></strong>
                        <p>the SAIBPP Business Directory cannot be held liable for any inaccurate information published on the Business Directory, save where such liability arises from the gross negligence or wilful misconduct of the SAIBPP Business Directory. You are encouraged to contact us to report any possible malfunctions or errors by way of email to admin@saibppbd.co.za .</p>
                        <p>The SAIBPP Business Directory shall not be liable for any direct, indirect, incidental, special or consequential loss or damages which might arise from your use of, or reliance upon, the Business Directory or the content contained in the Business Directory; or your inability to use the Business Directory, and/or unlawful activity on the Business Directory and/or any linked third party sponsors and/or partners.</p>
                        <p><strong>Availability and termination</p></strong>
                        <p>We will use reasonable endeavours to maintain the availability of the Business Directory, except during scheduled maintenance periods, and are entitled to discontinue providing the Business Directory or any part thereof with or without notice to you.</p>
                        <p>The SAIBPP Business Directory may in its sole discretion terminate, suspend and modify this Business Directory, with or without notice to you. You agree that the SAIBPP Business Directory will not be liable to you in the event that it chooses to suspend, modify or terminate this Business Directory.</p>
                        <p>If you fail to comply with your obligations under these Terms and Conditions, this may (in our sole discretion with or without notice to you) lead to a suspension and/or termination of your access to the Business Directory without any prejudice to any claims for damages or otherwise that we may have against you.</p>
                        <p>The SAIBPP Business Directory is entitled, for purposes of preventing suspected fraud and/or where it suspects that you are abusing the Business Directory, to blacklist you on its database (including suspending or terminating your access to the Business Directory), refuse to accept or process payment on any order, in whole or in part, on notice to you. The SAIBPP Business Directory accepts no other liability which may arise as a result of such blacklisting.</p>
                        <p>At any time, you can choose to stop using the Business Directory, with or without notice to the SAIBPP Business Directory.</p>
                        <p><strong>Information</p></strong>
                        <p>For the purposes of the ECT Act, the SAIBPP Business Directory’s information is as follows, which should be read in conjunction with other terms and conditions contained on the Business Directory:</p>
                        <ol>
                           <li>Full name: South African Institute for Black Property Practitioners, a non-profit organisation registered in South Africa with registration number (1997/006816/08)</li>
                           <li>Physical address for receipt of legal service (also street address): Vusa House, 5th Floor, Suite 2, Gandhi Square, Johannesburg.  Postal address (PO Box 62329, Marshalltown, 2017) (marked for attention: CEO)</li>
                           <li>Phone number: 011 838 6700/6722</li>
                           <li>Official email address: admin@saibpp.co.za</li>
                        </ol>
                        <p><strong>General</p></strong>
                        <p>The SAIBPP Business Directory may, in its sole discretion, at any time and for any reason and without prior written notice, suspend or terminate the operation of the Business Directory or the user’s right to use the Business Directory or any of its contents subject to us processing any outstanding obligations to you.</p>
                        <p>You may not cede, assign or otherwise transfer your rights and obligations in terms of these Terms and Conditions to any third party.</p>
                        <p>Any failure on the part of you or the SAIBPP Business Directory to enforce any right in terms hereof shall not constitute a waiver of that right.</p>
                        <p>If any term or condition contained herein is declared invalid, the remaining terms and conditions will remain in full force and effect.</p>
                        <p>No variation, addition, deletion, or agreed cancellation of the Terms and Conditions will be of any force or effect unless in writing and accepted by or on behalf of the parties hereto.</p>
                        <p>These Terms and Conditions contain the whole agreement between you and the SAIBPP Business Directory and no other warranty or undertaking is valid, unless contained in this document between the parties.</p>







                         

                        <p><strong>Disclaimer</p></strong>
                         


                        <p>The site is provided "as is" without warranty of any kind, express or implied. Use of the site is at your sole risk. We do not warrant that your use of the site will be uninterrupted or error free, nor do we make any warranty as to any results that may be obtained by use of the site.</p>
                        <p>We make no warranties, express or implied, including, without limitation, any implied warranties of merchantability, merchantable quality, fitness for a particular purpose, non-infringement, effectiveness, completeness, accuracy, and title.</p>
                        <p>Limitation of liability: If you are dissatisfied with the site, your sole and exclusive remedy shall be to discontinue use of the site. In no event shall our total liability for direct damages exceed the total fees paid by you to us.</p>
                        <p>Moreover, under no circumstances shall we be liable to you or any other person for any indirect, incidental, consequential, special or punitive damages for any matter arising from or relating to this agreement, the site or the Internet generally, including, without limitation, your use or inability to use the site, any changes to or inaccessibility of the site, delay, failure, unauthorised access to or alteration of any transmission or data, any material or data sent or received or not sent or received, any transaction or agreement entered into through the site, or any data or material from a third person accessed on or through the site, whether such liability is asserted on the basis of contract, tort or otherwise.</p>





                        
                        <p><strong>Privacy Policy</p></strong>
                         
                        <p>By using this Business Directory or any other branded Business Directory that is powered by the SAIBPP Business Directory, you give your consent that all personal data that you submit may be processed by South African Institute for Black Property Practitioners in the manner and for the purposes described in the following Privacy Policy.</p>
                        <p><strong>Definitions</p></strong>
                        <p>The terms "South African Institute for Black Property Practitioners”, ", “the SAIBPP Business Directory”, "we", "us", "our" and "ours" when used in these Terms and Conditions mean the SAIBPP Business Directory. The terms "you", "your" and "yours" when used in these Terms and Conditions mean any user of this website.</p>
                        <p>The term “Personal Data” refers to personally identifiable information about you, such as your name, job title, email or mailing address, as well as the information provided in relation to your business.</p>
                        <p><strong>About your privacy</p></strong>
                        <p>We are committed to safeguarding the privacy of registered Business Directory users whilst providing the highest possible quality of service. We will only use the information that we collect about you lawfully in accordance with the South African Data Protection Bill and the Protection of Personal Information Act.</p>
                        <p>If you have any questions concerning your personal information or regarding our practices, please contact: The CEO, the SAIBPP Business Directory, Vusa House, 5th Floor, Suite 2, Gandhi Square, Johannesburg, Postal Address (PO Box 62329, Marshalltown, 2017), Telephone 011 838 6700/6722 or send us an email to admin@[BRANDNAME]saibppbd.co.za .</p>
                        <p><strong>Collection of personal data</p></strong>
                        <p>We collect information through your registration process and from any email messages you may send to us. When you use this Business Directory to register to receive additional information or support from us, we ask you for contact information like your name, job title, company details, telephone number and email address.</p>
                        <p>We may also collect email addresses via marketing events such as trade shows and exhibitions. In such cases, we assume that people providing their email addresses are opting in to receive email messages from us. Therefore, we do not include an email opt-out mechanism when collecting event leads.</p>
                        <p>If, at any time, you wish to opt out of receiving information from the SAIBPP Business Directory via email, send us an email to admin@[BRANDNAME]saibppbd.co.za or contact us on 011 838 6700/6722.</p>
                        <p><strong>Anonymous data collected through this Business Directory</p></strong>
                        <p>We also use software tools to collect information automatically about your visit to our Business Directory. The information obtained in this way, which includes demographic data and browsing patterns, is only used in aggregate form and, as such, cannot be used to identify you personally. Such aggregate information helps us to audit usage of our Business Directory and improve the service provided.</p>
                        <p><strong>Cookies</p></strong>
                        <p>In order to collect the anonymous data described above, we may use temporary "cookies" that collect the domain name of the user and the date and time you visited this Business Directory. Cookies by themselves cannot be used to discover the identity of the user. A cookie is a small piece of information which is sent to your browser and stored on your computer’s hard drive. Cookies do not damage your computer. You can set your browser to notify you when you receive a cookie and this enables you to decide if you want to accept it or not.</p>
                        <p><strong>Use of personal data</p></strong>
                        <p>We process your personal data only for specific purposes defined in various areas of this Business Directory. We ask only for data that is adequate, relevant and not excessive for those purposes. When we ask you for personal data, we tell you the purposes for which we will process that data. Some of these purposes include:</p>
                        <ul>
                           <li>Providing you with information about our services, special offers and events or articles and case studies or reports we think will be of interest to you</li>
                           <li>Sending you regular newsletters by email or by post</li>
                           <li>Providing you with service information and promotional material that you have specifically requested</li>
                           <li>Processing any other requests that you make</li>
                           <li>To summarise usage behaviour for advertisers and partners, and to describe our service and performance. This is not based on individual behaviour, but is generic</li>
                           <li>Conducting marketing research.</li>
                        </ul>
                        <p><strong>Protection of Private Information (POPI) Act 4 of 2013</p></strong>
                        <p>By using the Business Directory you acknowledge that your company profile information is in the public domain and accessible to other users of the Business Directory. The Business Directory is a marketing platform for your company to reach buyers of other companies and by registering as a member you acknowledge that your company profile is in the public domain.</p>
                        <p>In terms of the POPI Act the SAIBPP Business Directory warrants that all confidential information such as personal identification numbers of directors and company banking details will be masked and only made available to an authorised administrator. Furthermore the SAIBPP Business Directory warrants that your company information will not be provided to any third party for the purposes of bulk unsolicited marketing communication. the SAIBPP Business Directory may make your company information available to its partners for the purposes of market research.</p>
                        <p>By being a member of the SAIBPP Business Directory you also accept that the SAIBPP Business Directory communicates with its members from time to time through email, letters, facsimles and direct telephone calls. The purpose of this communication serves many purposes from keeping you up to date on latest developments on the Business Directory solutions, as well as discounted offers and solutions available to you through our Partner network. At anytime you can choose to opt out of this member communication by emailing us at admin@saibpp.co.za.</p>
                        <p><strong>Disclosure of your personal data</p></strong>
                        <p>We do not share, sell or distribute your personal data with unrelated third parties, except under the following limited circumstances:</p>
                        <p>Personal data may occasionally be transferred to third parties who act for or on behalf of the SAIBPP Business Directory, or in connection with the business of the SAIBPP Business Directory for further processing in accordance with the purposes to which you have subsequently consented.</p>
                        <p>We may share or transfer the information in our databases to comply with a legal requirement, for the administration of justice, to protect your vital interest, to protect the security or integrity of our databases or this Business Directory, to take precautions against legal liability, or in the event of a corporate sale, merger, reorganisation, dissolution or similar event.</p>
                        <p><strong>Data integrity and security</</p></strong>
                        <p>We keep your personal data only for as long as reasonably necessary for the purposes for which it was collected or to comply with any applicable legal or ethical reporting or document retention requirements.</p>
                        <p>In order to prevent unauthorised access or disclosure, we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online. Our servers and our databases are protected by industry standard security technology, such as industry standard firewalls and password protection.</p>
                        <p>The employees who have access to personal data have been trained to handle such data properly and in accordance with our security protocols and strict standards of confidentiality. Although we cannot guarantee against any loss, misuse, unauthorised disclosure, alteration or destruction of data, we try to prevent such unfortunate occurrences.</p>
                        <p><strong>Data access and corrections</p></strong>
                        <p>At any time you can print a report on all the personal data forming part of the registration process. Should you need to request confirmation of any additional data held on you or your organisation, we will disclose to you the personal data we hold about you upon receipt of your written request addressed to The Administration Manager at the address given above. We will also correct, amend or delete any personal data that is inaccurate and notify any third party recipients of the necessary changes.</p>
                        <p><strong>Links to other websites</p></strong>
                        <p>This Privacy Policy applies only to www.the SAIBPP Business Directory.co.za. The Business Directory contains hyperlinks to websites that are not operated by the SAIBPP Business Directory. These hyperlinks are provided for your reference and convenience only and you should note that we do not have any control over these other websites. We cannot, therefore, be responsible for the protection and privacy of any data which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and review the privacy statement posted on any site you visit before using the site or providing any personal data about yourself.</p>
                        <p><strong>Updates and changes to privacy policy</p></strong>
                        <p>We reserve the right, at any time and without notice, to add to, change, update or modify this Privacy Policy, simply by posting such change, update or modification on this website. Any such change, update or modification will be effective immediately upon posting on the site.</p>
                        <p><strong>Governing law</p></strong>
                        <p>These Terms and Conditions are governed by and construed in accordance with the laws of of the Republic of South Africa. You agree to submit any dispute arising out of your use of this website to the exclusive jurisdiction of the courts of the Republic of South Africa.</p>


                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- About city estate end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
