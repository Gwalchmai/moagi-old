<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- About city estate start -->
<div class="about-city-estate">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="about-text">
                    <div class="main-title-2">
                        <h1><span>Membership</span> Confirmation</h1>
                    </div>
                    <p>Payment confirmation:</p>
                    <ul>
                       <li>Confirmation of premium subscription application via electronic fund transfer
                       <li>You have elected to upgrade to premium membership at the applicable <?php echo decuri($_GET['option']); ?> rate.
                       <li>An email has been sent to you confirming your application for premium subscription for an <?php echo decuri($_GET['option']); ?> period.
                       <li>Proof of payment must be emailed to admin@saibppbd.co.za
                       <li>On receipt of the proof of payment your subscription will be upgraded to premium status which will allow you full access to the all features of the business directory.
                    </ul>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- About city estate end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
