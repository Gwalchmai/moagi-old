<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<?php $this->load->view('web/common/inc-search'); ?>

<!-- About city estate start -->
<div class="about-city-estate">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="about-text">
                    <div class="main-title-2">
                        <h1>Welcome to the SAIBPP Business Directory!</h1>
                    </div>
                    <p>SAIBPPBD is the first online B-BBEE business directory & enterprise development platform specific to the property sector & built environment.</p>
                    <p>Created by the <a href="http://www.saibpp.co.za" style="color: #0a6aa1" target="_blank">South African Institute of Black Property Practitioners (SAIBPP),</a> the SAIBPP Business Directory forms an integral part of SAIBPP's skills & enterprise development programme enabling big business to connect with, reach and support more emerging entrepreneurs, property practitioners and empowered companies.</p>
                    <p>The SAIBPP Business Directory would not have been possible without the support of <a href="http://www.pareto.co.za" style="color: #0a6aa1" target="_blank">Pareto Ltd</a> and is a living example of what can be achieved through powerful collaborations with like-minded industry partners.</p>
                    <h5>SAIBPPBD KEY FEATURES:</h5>
                    <ul>
                        <li><strong>Data validation:</strong> Through our partnership with property sector professional associations and Managed Integrity Evaluation (MIE), a reputable South African screening firm, all supplier information listed on SAIBPPBD has been verified and confirmed.<br><br></li>
                        <li><strong>Chatroom:</strong> an easy to use live networking and business exchange platform where industry practitioners can discuss their issues, concerns, solutions and successes in the property sector – COMING SOON<br><br></li>
                        <li><strong>Moagi Magazine:</strong> the official publication of the South African Institute of Black Property Practitioners is an incredible selection of the latest South African property trends, news and entrepreneur profiles.<br><br></li>
                        <li>And many more...<br><br></li>
                    </ul>
                    <p>The stage is now set for improved business networking and quicker decision-making, with accurate, comprehensive, validated data. Convert business opportunities into business contracts & be part of a growing & inclusive sector.</p>
                </div>
            </div>
            <!-- Begin Property Image section -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="car-detail-slider simple-slider">
                    <div id="carousel-custom" class="carousel slide" data-ride="carousel">
                        <div class="carousel-outer">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item">
                                    <img src="/clientassets/img/about1.jpg" class="img-preview img-responsive" alt="properties-1">
                                </div>
                                <div class="item">
                                    <img src="/clientassets/img/about2.jpg" class="img-preview img-responsive" alt="properties-2">
                                </div>
                                <div class="item">
                                    <img src="/clientassets/img/about3.jpg" class="img-preview img-responsive" alt="properties-3">
                                </div>
                                <div class="item active left">
                                    <img src="/clientassets/img/about1.jpg" class="img-preview img-responsive" alt="properties-8">
                                </div>
                                <div class="item next left">
                                    <img src="/clientassets/img/about2.jpg" class="img-preview img-responsive" alt="properties-5">
                                </div>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-custom" role="button" data-slide="prev">
                                    <span class="slider-mover-left no-bg" aria-hidden="true">
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                    <span class="slider-mover-right no-bg" aria-hidden="true">
                                        <i class="fa fa-angle-right"></i>
                                    </span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End  Property Image section -->
            <p>&nbsp</p>
            <!-- Begin Advertising Section -->
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="car-detail-slider simple-slider">
                    <div id="carousel-advertise" class="carousel slide" data-ride="carousel">
                        <div class="carousel-outer">
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item">
                                    <img src="/clientassets/img/about1.jpg" class="img-preview img-responsive" alt="properties-1">
                                </div>
                                <div class="item">
                                    <img src="/clientassets/img/about2.jpg" class="img-preview img-responsive" alt="properties-2">
                                </div>
                                <div class="item">
                                    <img src="/clientassets/img/about3.jpg" class="img-preview img-responsive" alt="properties-3">
                                </div>
                                <div class="item active left">
                                    <img src="/clientassets/img/about1.jpg" class="img-preview img-responsive" alt="properties-8">
                                </div>
                                <div class="item next left">
                                    <img src="/clientassets/img/about2.jpg" class="img-preview img-responsive" alt="properties-5">
                                </div>
                            </div>
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-advertise" role="button" data-slide="prev">
                                    <span class="slider-mover-left no-bg" aria-hidden="true">
                                        <i class="fa fa-angle-left"></i>
                                    </span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-custom" role="button" data-slide="next">
                                    <span class="slider-mover-right no-bg" aria-hidden="true">
                                        <i class="fa fa-angle-right"></i>
                                    </span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Advertising Section -->
        </div>
    </div>
</div>
<!-- About city estate end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
