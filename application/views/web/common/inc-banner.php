<!-- Banner start -->
<div class="banner">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="/clientassets/img/sliders/slider1.jpg?v=1" alt="banner-slider-2">
                <div class="carousel-caption banner-slider-inner banner-top-align text-left">
                    <form method="post" action="/search">
                    <input type="hidden" name="fp" value="1">
                    <div class="banner-search-box pull-right" style="margin-top: 100px">
                        <!-- Search area start -->
                        <div class="search-area">
                            <div class="search-area-inner">
                                <div class="search-contents ">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <select class="selectpicker search-fields" name="service" data-live-search="true" data-live-search-placeholder="Search value">
                                                <option>Service</option>
                                                <?php echo get_lookup_options('propertysubsectorid',sget('propertysubsectorid')); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <select class="selectpicker search-fields" name="beestatus" data-live-search="true" data-live-search-placeholder="Search value">
                                                <option>BBBEE Status</option>
                                                <?php echo get_lookup_options('beestatusid',sget('beestatusid')); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <select class="selectpicker search-fields" name="location" data-live-search="true" data-live-search-placeholder="Search value">
                                                <option value="">Location</option>
                                                <?php echo get_lookup_options('locationid',sget('locationid')); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <input type="text" class="searchtext" name="company" placeholder="Company Name">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                        <div class="form-group">
                                            <button type="submit" class="search-button">Search</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Search area start -->
                    </div>
                    </form>
                </div>
            </div>
            <div class="item">
                <img src="/clientassets/img/sliders/slider2.jpg?v=1" alt="banner-slider-3">
                <div class="carousel-caption banner-slider-inner banner-top-align text-left">
                    <form method="post" action="/search">
                    <input type="hidden" name="fp" value="1">
                    <div class="banner-search-box" style="margin-top: 80px">
                        <!-- Search area start -->
                        <div class="search-area">
                            <div class="search-area-inner">
                              <div class="search-contents ">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <select class="selectpicker search-fields" name="service" data-live-search="true" data-live-search-placeholder="Search value">
                                              <option>Service</option>
                                              <?php echo get_lookup_options('propertysubsectorid',sget('propertysubsectorid')); ?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <select class="selectpicker search-fields" name="beestatus" data-live-search="true" data-live-search-placeholder="Search value">
                                              <option>BBBEE Status</option>
                                              <?php echo get_lookup_options('beestatusid',sget('beestatusid')); ?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <select class="selectpicker search-fields" name="location" data-live-search="true" data-live-search-placeholder="Search value">
                                            <option value="">Location</option>
                                            <?php echo get_lookup_options('locationid',sget('locationid')); ?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <input type="text" class="searchtext" name="company" placeholder="Company Name">
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                      <div class="form-group">
                                          <button type="submit" class="search-button">Search</button>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <!-- Search area start -->
                    </div>
                    </form>
                </div>
            </div>
            <div class="item">
                <img src="/clientassets/img/sliders/slider3.jpg?v=1" alt="banner-slider-4">
                <div class="carousel-caption banner-slider-inner banner-top-align text-left">
                    <form method="post" action="/search">
                    <input type="hidden" name="fp" value="1">
                    <div class="banner-search-box pull-right" style="margin-top: 45px">
                        <!-- Search area start -->
                        <div class="search-area">
                            <div class="search-area-inner">
                              <div class="search-contents ">
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <select class="selectpicker search-fields" name="service" data-live-search="true" data-live-search-placeholder="Search value">
                                              <option>Service</option>
                                              <?php echo get_lookup_options('propertysubsectorid',sget('propertysubsectorid')); ?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <select class="selectpicker search-fields" name="beestatus" data-live-search="true" data-live-search-placeholder="Search value">
                                              <option>BBBEE Status</option>
                                              <?php echo get_lookup_options('beestatusid',sget('beestatusid')); ?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <select class="selectpicker search-fields" name="location" data-live-search="true" data-live-search-placeholder="Search value">
                                            <option value="">Location</option>
                                            <?php echo get_lookup_options('locationid',sget('locationid')); ?>
                                          </select>
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                      <div class="form-group">
                                          <input type="text" class="searchtext" name="company" placeholder="Company Name">
                                      </div>
                                  </div>
                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                                      <div class="form-group">
                                          <button type="submit" class="search-button">Search</button>
                                      </div>
                                  </div>
                              </div>
                            </div>
                        </div>
                        <!-- Search area start -->
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
            <span class="sr-only">Previous</span>
        </a>

        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
<!-- Banner end -->
