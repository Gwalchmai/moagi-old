<!-- Partners block start -->
<div class="partners-block">
    <div class="container">
        <h3>Partners & Affiliates</h3>
        <div class="row">
            <div class="col-md-12">
                <div class="carousel our-partners slide" id="ourPartners">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="http://www.wpn.co.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner8.png" alt="audiojungle">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="http://www.bbcbe.org/" target="_blank">
                                    <img src="/clientassets/img/partners/partner9.png" alt="audiojungle">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="http://www.safma.co.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner1.png" alt="audiojungle">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="https://www.gbcsa.org.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner2.png" alt="themeforest">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="https://www.mie.co.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner3.png" alt="tuts">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="https://www.ecsa.co.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner4.png" alt="graphicriver">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="http://www.cidb.org.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner5.png" alt="codecanyon">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="http://www.pareto.co.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner6.png" alt="codecanyon">
                                </a>
                            </div>
                        </div>
                        <div class="item">
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <a href="http://www.saiv.org.za/" target="_blank">
                                    <img src="/clientassets/img/partners/partner7.png" alt="codecanyon">
                                </a>
                            </div>
                        </div>
                    </div>
                    <a class="left carousel-control" href="#ourPartners" data-slide="prev"><i class="fa fa-chevron-left icon-prev"></i></a>
                    <a class="right carousel-control" href="#ourPartners" data-slide="next"><i class="fa fa-chevron-right icon-next"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Partners block end -->
