<!-- Footer start -->
<footer class="main-footer clearfix">
    <div class="container">
        <!-- Footer top -->
        <div class="footer-top">
            <div class="row">
                <!--<div class="col-lg-4  col-md-4 col-sm-5 col-xs-12">
                    <form action="#" method="post">
                        <input type="text" class="form-contact" name="email" placeholder="Enter your email">
                        <button type="submit" name="submitNewsletter" class="btn btn-default button-small">
                            <i class="fa fa-paper-plane"></i>
                        </button>
                    </form>
                </div>-->
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <ul class="social-list clearfix">
                        <li><a href="https://za.linkedin.com/in/saibpp-property-organisation-33492148" target="_blank"><i
                                        class="fa fa-linkedin"></i></a></li>
                        <li><a href="http://www.twitter.com/saibpp" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="http://www.facebook.com/saibppSA/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCiaQq8Mh93HY0mogYNsAV0Q" target="_blank"><i
                                        class="fa fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Footer info-->
        <div class="footer-info">
            <div class="row">
                <!-- Links -->
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>Useful Links</h1>
                        </div>
                        <ul class="links">
                            <li>
                                <a href="http://www.wpn.co.za" target="_blank">www.wpn.co.za</a>
                            </li>
                            <li>
                                <a href="http://www.sapoa.org.za" target="_blank">www.sapoa.org.za</a>
                            </li>
                            <li>
                                <a href="http://www.sacsc.co.za" target="_blank">www.sacsc.co.za</a>
                            </li>
                            <li>
                                <a href="http://www.eaab.org.za" target="_blank">www.eaab.org.za</a>
                            </li>
                            <li>
                                <a href="http://www.propertycharter.co.za" target="_blank">www.propertycharter.co.za</a>
                            </li>
                            <li>
                                <a href="http://www.safma.co.za" target="_blank">www.safma.co.za</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- About us -->
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="footer-item">
                        <div class="main-title-2">
                            <h1>Contact Us</h1>
                        </div>
                        <ul class="personal-info">
                            <li>
                                <i class="fa fa-envelope"></i>
                                Email:<a href="mailto:admin@saibppbd.co.za">admin@saibppbd.co.za</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Disclaimer Start -->

            <!-- Trigger the modal with a button -->
            <button type="button" class="btn btn-link" data-toggle="modal" data-target="#myModal" style="background-color: #000000; font-color: #a2a2a2;">Disclaimer</button>

            <!-- Modal -->
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">SAIBPP Business Directory Disclaimer</h4>
                        </div>
                        <div class="modal-body">
                            <p>The site is provided "as is" without warranty of any kind, express or implied. Use of the site is at your sole risk. We do not warrant that your use of the site will be uninterrupted or error free, nor do we make any warranty as to any results that may be obtained by use of the site.</p>
                            <p>We make no warranties, express or implied, including, without limitation, any implied warranties of merchantability, merchantable quality, fitness for a particular purpose, non-infringement, effectiveness, completeness, accuracy, and title.</p>
                            <p>Limitation of liability: If you are dissatisfied with the site, your sole and exclusive remedy shall be to discontinue use of the site. In no event shall our total liability for direct damages exceed the total fees paid by you to us.</p>
                            <p>Moreover, under no circumstances shall we be liable to you or any other person for any indirect, incidental, consequential, special or punitive damages for any matter arising from or relating to this agreement, the site or the Internet generally, including, without limitation, your use or inability to use the site, any changes to or inaccessibility of the site, delay, failure, unauthorised access to or alteration of any transmission or data, any material or data sent or received or not sent or received, any transaction or agreement entered into through the site, or any data or material from a third person accessed on or through the site, whether such liability is asserted on the basis of contract, tort or otherwise.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

            <!-- Disclaimer end -->
        </div>
    </div>
</footer>
<!-- Footer end -->



<!-- Copy right start -->
<div class="copy-right">
    <div class="container">
		<?php echo COPYRIGHT; ?> <a href="http://www.juuceconsult.co.za" target="_blank"> Powered by Juuce Consult (Pty) Ltd.</a>
    </div>
</div>
<!-- Copy end right-->
