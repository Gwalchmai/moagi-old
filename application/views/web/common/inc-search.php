<!-- Search area start -->

<div class="search-area">
    <div class="container">
        <div class="search-area-inner">
            <div class="search-contents ">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                <option>Service</option>
                                 <?php echo get_lookup_options('propertysubsectorid',sget('propertysubsectorid')); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                <option>BBBEE Status</option>
                                <?php echo get_lookup_options('beestatusid',sget('beestatusid')); ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="form-group">
                            <select class="selectpicker search-fields" data-live-search="true" data-live-search-placeholder="Search value">
                                <option>Location</option>
                                <option>Location</option>
                                <option>Eastern Cape</option>
                                <option>Free State</option>
                                <option>Gauteng</option>
                                <option>KwaZulu-Natal </option>
                                <option>Limpopo</option>
                                <option>Mpumalanga</option>
                                <option>Northern Cape</option>
                                <option>North West</option>
                                <option>Western Cape</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 ">
                        <div class="form-group">
                            <button class="search-button">Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Search area start -->
