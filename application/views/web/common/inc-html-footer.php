<?php chat(); ?>
<script type="text/javascript" src="/clientassets/js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="/clientassets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/clientassets/js/bootstrap-submenu.js"></script>
<script type="text/javascript" src="/clientassets/js/rangeslider.js"></script>
<script type="text/javascript" src="/clientassets/js/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="/clientassets/js/wow.min.js"></script>
<script type="text/javascript" src="/clientassets/js/bootstrap-select.min.js"></script>
<script type="text/javascript" src="/clientassets/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/clientassets/js/jquery.scrollUp.js"></script>
<script type="text/javascript" src="/clientassets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="/clientassets/js/leaflet.js"></script>
<script type="text/javascript" src="/clientassets/js/leaflet-providers.js"></script>
<script type="text/javascript" src="/clientassets/js/leaflet.markercluster.js"></script>
<script type="text/javascript" src="/clientassets/js/dropzone.js"></script>
<script type="text/javascript" src="/clientassets/js/jquery.filterizr.js"></script>
<script type="text/javascript" src="/clientassets/js/maps.js"></script>
<script type="text/javascript" src="/clientassets/js/app.js?v=<?php echo date(); ?>"></script>
<script type="text/javascript" src="/clientassets/js/ernic.ajax.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script type="text/javascript" src="/clientassets/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script type="text/javascript" src="/clientassets/js/ie10-viewport-bug-workaround.js"></script>

</body>
</html>
