<!-- Main header start -->
<header class="main-header">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navigation" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/" style="margin-top: 10px; !important;">
                    <img src="/clientassets/img/saibpp_small.png" alt="logo">
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" role="navigation" aria-expanded="true" id="app-navigation">
                <ul class="nav navbar-nav">
                    <li <?php if($this->uri->segment(1) == '') echo 'class="active"';?>><a href="/">HOME</a></li>

                    <li <?php if($this->uri->segment(1) == 'about') echo 'class="active"';?> class="dropdown">
                        <a href="/about" tabindex="0" data-toggle="dropdown" data-submenu="" aria-expanded="false">
                           ABOUT <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                           <li><a href="/about">Our Story</a></li>
                           <li><a href="/membership">Packages</a></li>
                           <li><a href="/verification">Verification Turnaround Times</a></li>
                           <li><a href="/entdev">Enterprise & Supplier Development</a></li>
                        </ul>
                    </li>

                    <?php /*<li><a href="/docs/SAIBPP Tender Opportunities for site.pdf" target="_blank">OPPORTUNITIES</a></li>*/?>
                    <li <?php if($this->uri->segment(1) == 'opportunities') echo 'class="active"';?>><a href="/opportunities">OPPORTUNITIES</a></li>
                    <li <?php if($this->uri->segment(1) == 'advertise') echo 'class="active"';?>><a href="/advertise">ADVERTISE</a></li>
                    <li <?php if($this->uri->segment(1) == 'news') echo 'class="active"';?>><a href="http://www.moagimag.co.za/issue1/" target="_blank">MAGAZINE</a></li>
                    <li <?php if($this->uri->segment(1) == 'contact') echo 'class="active"';?>><a href="/contact">CONTACT US</a></li>
                    <li <?php if($this->uri->segment(1) == 'chat') echo 'class="active"';?>><a href="/chat">CHAT ROOM</a></li>


                </ul>
                <ul class="nav navbar-nav navbar-right rightside-navbar">
                   <?php if(user() == null) { ?>
                     <li>
                       <a href="/registerquick" class="button">
                           Register
                       </a>
                     </li>
                     <li>&nbsp;</li>
                     <li>
                        <a href="/login" class="button">
                            Login
                        </a>
                     </li>
                    <?php } else { ?>
                       <li>
                         <a href="/myprofile" class="button">
                             Profile
                         </a>
                       </li>
                       <li>&nbsp;</li>
                       <li>
                          <a href="/logout" class="button">
                              Logout
                          </a>
                       </li>
                    <?php } ?>
                </ul>
            </div>

            <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>
    </div>
</header>
<!-- Main header end -->
