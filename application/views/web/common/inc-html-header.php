<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>SAIBPPBD.co.za</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="/clientassets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/clientassets/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="/clientassets/css/bootstrap-submenu.css">
    <link rel="stylesheet" type="text/css" href="/clientassets/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="/clientassets/css/leaflet.css" type="text/css">
    <link rel="stylesheet" href="/clientassets/css/map.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="/clientassets/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/clientassets/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="/clientassets/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="/clientassets/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="/clientassets/css/dropzone.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="/clientassets/css/style.css?v=1">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="/clientassets/css/skins/blue.css?v=1">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="/clientassets/img/favicon.ico" type="image/x-icon" >

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800%7CPlayfair+Display:400,700%7CRoboto:100,300,400,400i,500,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="/clientassets/css/ie10-viewport-bug-workaround.css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script type="text/javascript" src="/clientassets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script type="text/javascript" src="/clientassets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="/clientassets/js/html5shiv.min.js"></script>
    <script type="text/javascript" src="/clientassets/js/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/clientassets/css/progress-wizard.min.css?v=2">
    <link rel="stylesheet" type="text/css" href="/clientassets/css/saibpp.css?v=<?php echo md5(date('Yis')); ?>">
    <!-- Settings for magazine -->
    <?php
        if(isset($iIssueID)){
            ?>
    <meta name="Description" content="Here">
    <meta http-equiv="x-ua-compatible" content="IE=edge">
    <meta property="fb:app_id" content="449283325123417"/>
    <meta property="og:type" content="article"/>
    <meta property="og:title" content="Moagi Magazine"/>
    <meta property="og:image" content="http://www.saibppbd.co.za/mag/issue1/files/assets/cover/1.jpg"/>
    <meta property="og:description" content="No Description Available"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="apple-touch-icon" href="http://www.saibppbd.co.za/mag/issue1/html5/resources/ipad.png">
            <!-- WinJS references -->
    <!-- <link href="//Microsoft.WinJS.1.0/css/ui-dark.css" rel="stylesheet" /> <script src="//Microsoft.WinJS.1.0/js/base.js"></script> <script src="//Microsoft.WinJS.1.0/js/ui.js"></script>-->
    <link rel="stylesheet" href="https://code.3dissue.com/v8/css/3di_gl.min.css" media="screen, print" type="text/css">
    <style> body, #pgbk {
            background: #1a1a1a;
        }

        #plbar {
            background: #000000;
            border-color: #000000;
        }

        #plbk {
            border-color: #000000;
        } </style>
    <script src="https://code.3dissue.com/v8/js/libs/jquery-2.1.3.min.js"></script>
    <script src="https://code.3dissue.com/v8/js/libs/modernizr-latest.min.js"></script>
    <script src="https://code.3dissue.com/v8/js/f/3di_ds.min.js"></script>
    <?php
        }
        ?>
</head>
<body>
