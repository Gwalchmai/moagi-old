<div class="main-title-2">
   <h1>Enterprise Documents</h1>
</div>

<p><i>For multiple documents please ZIP the files together and upload the single ZIP File.</i></p>

<div class="form-group">
    <label>CK Documents<em class="req">*</em></label>
    <?php $fieldname = "ckdocuments"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>" <?php if(val($rs,$fieldname,sget($fieldname)) == '') echo 'required="required"' ?> >
</div>

<div class="form-group">
    <label>B-BBEE Certificate<em class="req">*</em></label>
    <?php $fieldname = "bbbeecertificate"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>" <?php if(val($rs,$fieldname,sget($fieldname)) == '') echo 'required="required"' ?> ">
</div>


<div class="form-group">
    <label>Tax Clearance Certificate</label>
    <?php $fieldname = "taxclearancecert"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Workman's Compensation</label>
    <?php $fieldname = "workmanscompensation"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Public Liability</label>
    <?php $fieldname = "publicliability"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Proof of business address (RICA documents)</label>
	<?php $fieldname = "imageofpremises"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Trading References</label>
    <?php $fieldname = "tradingreferences"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Bank Confirmation Letter</label>
	<?php $fieldname = "bankconfirmationletter"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Company Logo</label>
	<?php $fieldname = "companylogo"; ?>

	<?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<p><a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a> &nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deleteattachment=' . $fieldname . '&gostep=' . $step . '">Delete</a></p>'; ?>

    <input id="<?php echo $fieldname; ?>" type="file" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>