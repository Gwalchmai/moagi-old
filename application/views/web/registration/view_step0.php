<div class="main-title-2">
   <h1>Terms and Conditions</h1>
</div>

<p>Please confirm that you have read and agree to our <a href="/terms" class="link" target="_blank">Terms and Conditions?</a> </p>

<p>As part of the verification process, one of the attached two forms must be completed.  Independent contractors and sole proprietors should complete the document labelled MIE individual consent form.  Representatives of Companies should complete the document labelled "MIE Company consent form".  Once completed, upload the MIE consent form under "step 3: Enterprise document/other documents"
</p>
<ul>
  <li><a href="/clientassets/docs/MIE_Company_Consent_Form_2017.pdf" class="link" target="_blank">MIE Company Consent Form 2017</a></li>
  <li><a href="/clientassets/docs/MIE_Individual_Consent_Form_2017.pdf" class="link" target="_blank">MIE Individual Consent Form 2017</a></li>
</ul>


<div class="checkbox checkbox-theme checkbox-circle">
  <input id="cbAgree" type="checkbox">
  <label for="cbAgree">
      Yes, I agree
  </label>
</div>
