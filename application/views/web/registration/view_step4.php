<div class="main-title-2">
   <h1>Directors</h1>
</div>

<input type="hidden" name="sd_id" id="sd_id" value="">
<input type="hidden" name="delete_sd_id" id="delete_sd_id" value="">

<div class="modal fade" tabindex="-1" role="dialog" id="directorModal" style="z-index:999999;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="directorTitle">Modal title</h4>

      </div>
      <div class="modal-body" style="max-height:500px; overflow:scroll;">
         <input type="hidden" name="editDirectorAction" value="">
         <div class="form-group">
            <label>Type <em class="req">*</em></label>
            <?php $fieldname = "sd_typeid"; ?>
            <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
                <?php echo get_lookup_options($fieldname,val($rs,$fieldname,'')); ?>
            </select>
         </div>

          <div class="form-group">
              <label>First Name <em class="req">*</em></label>
              <?php $fieldname = "sd_firstname"; ?>
              <input type="text" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="">
          </div>

          <div class="form-group">
              <label>Last Name <em class="req">*</em></label>
              <?php $fieldname = "sd_lastname"; ?>
              <input type="text" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="">
          </div>

         <div class="form-group">
            <label>Title <em class="req">*</em></label>
            <?php $fieldname = "sd_titleid"; ?>
            <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
                <?php echo get_lookup_options($fieldname,val($rs,$fieldname,'')); ?>
            </select>
         </div>

          <div class="form-group">
              <label>Email Address <em class="req">*</em></label>
              <?php $fieldname = "sd_email"; ?>
              <input type="text" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="">
          </div>

          <div class="form-group">
              <label>Telephone Number <em class="req">*</em></label>
              <?php $fieldname = "sd_telno"; ?>
              <input type="text" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="">
          </div>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn button-md button-theme" data-dismiss="modal">Close</button>
        <button type="button" class="btn button-md button-theme" onclick="SaveDirector();">Save</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<button type="button" class="btn button-md button-theme" onclick="EditDirectorItem('N');">Add Director</button>
<table class="table table-striped" style="min-width: 1000px">
<thead>
     <tr>
         <th>Type</th>
         <th>Title</th>
         <th>Name &amp; Surname</th>
         <th>Email Address</th>
         <th>Telephone Number</th>
         <th></th>
     </tr>
   </thead>
   <tbody id="directoritemscontainer">
      <?php

         $rsdir = $this->sd_model->find_all(array('userid' => $this->session->userdata('userid')));
         foreach($rsdir as $row)
         {
            ?>
            <tr>
               <td><?php echo get_lookup_value($row->sd_typeid); ?></td>
               <td><?php echo get_lookup_value($row->sd_titleid); ?></td>
               <td><?php echo $row->sd_firstname . ' ' . $row->sd_lastname; ?></td>
                <td><?php echo $row->sd_email; ?></td>
                <td><?php echo $row->sd_telno; ?></td>
               <td>
                  <button type="button" class="btn button-sm button-theme" onclick="EditDirectorItem('<?php echo $row->sd_id; ?>');">Edit</button>
                  <button type="button" class="btn button-sm button-theme" onclick="DeleteDirectorItem('<?php echo $row->sd_id; ?>');">Delete</button>
               </td>
            </tr>
            <?php
         }

      ?>
   </tbody>
</table>

<script>
   var APPSERVER = '<?php echo base_url(''); ?>';

   function DeleteDirectorItem(itemid)
   {
      if(confirm('Are you sure you want to delete this director?'))
      {
         $('#delete_sd_id').val(itemid);
         $('#step').val(4);
         $('#form').submit();
      }
   }

   function EditDirectorItem(itemid)
   {
      $('#sd_id').val(itemid);
      if(itemid == 'N')
         $('#directorTitle').html('Add Director');
      else
      {
         $('#directorTitle').html('Edit Director');
          var param = {
              sd_id: itemid
           };
           var request = new eajax();
           request.loadingmsg = '';
           request.get(APPSERVER + 'ajax/getdirector',param,
              function(res)
              {
                 $('#sd_typeid').val(res.sd_typeid);
                 $('#sd_titleid').val(res.sd_titleid);
                 $('#sd_firstname').val(res.sd_firstname);
                 $('#sd_lastname').val(res.sd_lastname);
                 $('#sd_countryid').val(res.sd_countryid);
                  $('#sd_idnumber').val(res.sd_idnumber);
                 $('#sd_email').val(res.sd_email);
                 $('#sd_telno').val(res.sd_telno);
                 $('#sd_passport').val(res.sd_passport);
                 $('#sd_civilservantid').val(res.sd_civilservantid);
                 $('#sd_disabledid').val(res.sd_disabledid);

                 if((res.sd_directorsqualification != null) && (res.sd_directorsqualification != ''))
                 {
                     var html = '<p><a href="<?php echo UPLOADURL; ?>/' + res.sd_directorsqualification + '" target="_blank">View</a>';
                     html += '&nbsp;&nbsp; | &nbsp;&nbsp; <a href="?deletesdattachment=' + res.sd_directorsqualification + '&sd_id=' + res.sd_id + '&gostep=4">Delete</a></p>'
                     $('#fileaction').html(html);
                 }

                 if(res.message)
                   alert(res.message);
              },
              function(err)
              {
                 //console.log(err);
                 alert('Error ' + err.responseText);
              }
           );
      }
      $('#directorModal').modal('show');
   }

   function SaveDirector()
   {
      var validated = true;
      $(':input[required]:visible').each(function() {
          if ($(this).val() == '') {
             $(this).css("border", "2px solid red");
             validated = false;
         }
      });

      if(validated)
      {
         $('#step').val(4);
         $('#form').submit();
      }
   }

</script>
