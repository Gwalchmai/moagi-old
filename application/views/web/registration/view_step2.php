<div class="main-title-2">
   <h1>B-BBEE Status</h1>
</div>

<div class="form-group">
    <label>B-BBEE Status <em class="req">*</em></label>
    <?php $fieldname = "beestatusid"; ?>
    <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
        <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
    </select>
</div>

<div class="form-group">
    <label>Black Ownership % <em class="req">*</em></label>
    <?php $fieldname = "blackownershippercent"; ?>
    <input type="number" min="0" max="100" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Black Women Ownership % <em class="req">*</em></label>
    <?php $fieldname = "blackwomenownershippercent"; ?>
    <input type="number" min="0" max="100" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
   <label>B-BBEE Certificate Type <em class="req">*</em></label>
   <?php $fieldname = "bbbeecerttypeid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
   <label>Verification Auditor </label>
   <?php $fieldname = "verificationauditorid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
   <label>Are you classified as an empowering supplier? </label>
   <?php $fieldname = "empoweringsupplierid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
   <label>Black Company Status</label>
   <?php $fieldname = "blackcompanystatusid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

