<div class="main-title-2">
   <h1>Login and Company Details</h1>
</div>

<div class="form-group">
   <label>Register As <em class="req">*</em></label>
   <?php $fieldname = "userroleid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
    <label>Company Name <em class="req">*</em> </label>
    <?php $fieldname = "companyname"; ?>
    <input type="text" required="required" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Trading Name </label>
    <?php $fieldname = "tradingname"; ?>
    <input type="text" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
   <label>SAIBPP Member </label>
   <?php $fieldname = "saibppmemberid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
   <label>Company Type </label>
   <?php $fieldname = "companytypeid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker search-fields">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
   <label>Sub-Sector </label>
   <?php $fieldname = "industrysectorid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker search-fields">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
   <label>Industry </label>
   <?php $fieldname = "propertysubsectorid"; ?>
   <select id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" class="selectpicker search-fields">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
   <?php $fieldname = "propertysubsectorother"; ?>
   <input type="text" class="input-text" required <?php if(val($rs,$fieldname,sget($fieldname)) == ''); echo 'style="display:none;"'; ?> id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>" placeholder="Describe Other">
</div>

<div class="form-group">
   <label>Associations </label>
   <?php $fieldname = "propertyassociations";
         $lookupname = "propertyassociationid";
   ?>
   <select name="<?php echo $fieldname; ?>[]" id="<?php echo $fieldname; ?>" class="selectpicker" multiple >
       <?php echo get_multiple_lookup_options($lookupname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
   <?php $fieldname = "propertyassociationother"; ?>
   <input type="text" class="input-text" required <?php if(val($rs,$fieldname,sget($fieldname)) == ''); echo 'style="display:none;"'; ?> id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>" placeholder="Describe Other">
</div>

<div class="form-group">
    <label>Company Telephone Number </label>
    <?php $fieldname = "companytel"; ?>
    <input type="text" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Company Email Address </label>
    <?php $fieldname = "companyemail"; ?>
    <input type="email" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Structure Address Details </label>
    <?php $fieldname = "companyaddress"; ?>
    <textarea class="input-text" name="<?php echo $fieldname; ?>"><?php echo val($rs,$fieldname,sget($fieldname)); ?></textarea>
</div>

<div class="form-group">
    <label>Is your business VAT registered</label>
    <?php $fieldname = "vatregisteredid"; ?>
    <select name="<?php echo $fieldname; ?>" class="selectpicker">
        <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
    </select>
</div>

<div class="form-group">
    <label>VAT Number</label>
    <?php $fieldname = "vatnumber"; ?>
    <input type="text" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Company Registration Number</label>
    <?php $fieldname = "companyregno"; ?>
    <input type="text" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Enterprise Size <em class="req">*</em></label>
    <?php $fieldname = "enterprisesizeid"; ?>
    <select name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
        <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
    </select>
</div>

<div class="form-group">
   <label>Province </label>
   <?php $fieldname = "locationid"; ?>
   <select name="<?php echo $fieldname; ?>" class="selectpicker search-fields">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
    <label>Years Operating</label>
	<?php $fieldname = "yearsoperating"; ?>

    <input type="number" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="main-title-2">
    <h1>Contact Person</h1>
</div>

<div class="form-group">
    <label>Title<em class="req">*</em> </label>
    <?php $fieldname = "titleid"; ?>
    <select name="<?php echo $fieldname; ?>" class="selectpicker search-fields" required="required">
       <?php echo get_lookup_options($fieldname,val($rs,$fieldname,sget($fieldname))); ?>
   </select>
</div>

<div class="form-group">
    <label>Name<em class="req">*</em> </label>
    <?php $fieldname = "name"; ?>
    <input type="text" required="required" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Surname<em class="req">*</em> </label>
    <?php $fieldname = "surname"; ?>
    <input type="text" required="required" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Designation</label>
    <?php $fieldname = "designation"; ?>
    <input type="text" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Contact Person Email Address<em class="req">*</em></label>
    <?php $fieldname = "email"; ?>
    <input type="text" required="required" class="input-text" id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Contact Person Mobile Number <em class="req">*</em></label>
    <?php $fieldname = "tel"; ?>
    <input type="text" required="required" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="main-title-2">
    <h1>Trade References</h1>
</div>

<?php for($i=1;$i<=3;$i++) { ?>
<div class="form-group">
    <label>Company Name (<?php echo $i;?>)</label>
    <?php $fieldname = "tr_companyname" . $i; ?>
    <input type="text" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Description Of project, service, product provision(<?php echo $i;?>)</label>
    <?php $fieldname = "tr_description" . $i; ?>
    <textarea class="input-text" name="<?php echo $fieldname; ?>"><?php echo val($rs,$fieldname,sget($fieldname)); ?></textarea>
</div>

<div class="form-group">
    <label>Client Project Manager (<?php echo $i;?>)</label>
    <?php $fieldname = "tr_contact" . $i; ?>
    <input type="text" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Contact Number (<?php echo $i;?>)</label>
    <?php $fieldname = "tr_contactnumber" . $i; ?>
    <input type="text" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Contact Email (<?php echo $i;?>)</label>
    <?php $fieldname = "tr_contactemail" . $i; ?>
    <input type="email" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<div class="form-group">
    <label>Project Cost in Rands (<?php echo $i;?>)</label>
    <?php $fieldname = "tr_projectcost" . $i; ?><br>
    <input type="number" class="input-text" name="<?php echo $fieldname; ?>" value="<?php echo val($rs,$fieldname,sget($fieldname)); ?>">
</div>

<?php } ?>
