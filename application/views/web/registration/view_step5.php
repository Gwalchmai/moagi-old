<div class="main-title-2">
    <h1>Premium Membership</h1>
</div>

<p>Please note that <a href="/membership" class="link">basic membership</a> to the SAIBPP Business Directory is free.</p>
<p>However, should you wish to take advantage of the many <a href="/membership" class="link">benefits</a> this directory has to offer,
    including verification of company information you have provided, you may upgrade to premium membership now.</p>

		<?php $fieldname = "supplierpaymentoptionid"; ?>
		<?php if ( val( $rs, 'userroleid', sget( 'userroleid' ) ) == '4' ) { ?>
            <div class="form-group">
                <label for="<?= $fieldname ?>">Payment Options: <em class="req">*</em></label>
                <select id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
					<?php echo get_lookup_options( $fieldname, val( $rs, $fieldname, sget( $fieldname ) ) ); ?>
                </select>
            </div>

		<?php } else { ?>

            <div class="form-group">
	            <?php $fieldname = "procurerpaymentoptionid"; ?>
                <label for="<?= $fieldname ?>">Payment Options: <em class="req">*</em></label>
				    <select id="<?php echo $fieldname; ?>" name="<?php echo $fieldname; ?>" class="selectpicker" required="required">
					<?php echo get_lookup_options( $fieldname, val( $rs, $fieldname, sget( $fieldname ) ) ); ?>
                </select>
            </div>

		<?php } ?>

        <div class="form-group">
            <label for="payment_method">Payment Type: <em class="req">*</em></label><br>
            <select id="payment_method" name="payment_method" class="selectpicker" required="required">
                <option value="cc">Credit Card</option>
                <option value="dc">Debit Card</option>
            </select>
        </div>
<button type="submit" class="btn button-sm button-theme">Purchase Subscription</button>
</form>
<br>