<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Forgot</span> Password</h1>
                        </div>
                        <!-- Form start -->
                        <?php display_messages(); ?>
                        <form method="post">
                           <input type="hidden" name="fp" value="1">
                            <div class="form-group">
                                <input type="text" name="email" class="input-text" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="button-md button-theme btn-block">Send Me Email</button>
                            </div>
                        </form>
                        <!-- Form end -->
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>
                           I want to <a href="/login">return to login</a>
                        </span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<?php $this->load->view('web/common/inc-html-footer'); ?>
