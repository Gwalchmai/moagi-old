<?php $this->load->view('web/common/inc-html-header'); ?>
<?php $this->load->view('web/common/inc-header'); ?>

<div class="page_loader"></div>

<!-- Content area start -->
<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Main title -->
                        <div class="main-title">
                            <h1><span>Registration</span></h1>
                        </div>
                        <?php display_messages(); ?>
                        <!-- Form start -->
                        <form method="post">
                           <input type="hidden" name="fp" value="1">
                            <p>Thank you for registering. Your profile still needs to be verified. A confirmation email has been sent to the email address you supplied, once received please click the verification link contained in the email to complete your registration.
                            </p>
                            <div class="form-group">
                                <a href="/" class="button-md button-theme btn-block">Return to SAIBPPBD.co.za</a>
                            </div>
                        </form>
                        <!-- Form end -->
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>
                           &nbsp;
                        </span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Content area end -->

<?php $this->load->view('web/common/inc-html-footer'); ?>
