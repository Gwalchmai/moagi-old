<?php $this->load->view( 'web/common/inc-html-header' ); ?>

<div class="page_loader"></div>

<?php $this->load->view( 'web/common/inc-header' ); ?>

<?php // $this->load->view('web/common/inc-banner'); ?>

<div class="content-area my-profile">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="my-address">

                    <ul class="progress-indicator">
                        <li class="<?php if ( $step > 1 ) {
							echo 'completed';
						} ?> <?php if ( $step == 1 ) {
							echo 'info';
						} ?>" data-toggle="tooltip" data-placement="top" title="Login and Company Details"><span class="bubble"><span
                                        class="title">1</span></span></li>
                        <li class="<?php if ( $step > 2 ) {
							echo 'completed';
						} ?> <?php if ( $step == 2 ) {
							echo 'info';
						} ?>" data-toggle="tooltip" data-placement="top" title="Enterprise Credentials"><span class="bubble"><span
                                        class="title">2</span></span></li>
                        <li class="<?php if ( $step > 3 ) {
							echo 'completed';
						} ?> <?php if ( $step == 3 ) {
							echo 'info';
						} ?>" data-toggle="tooltip" data-placement="top" title="Enterprise Documents"><span class="bubble"><span
                                        class="title">3</span></span></li>
                        <li class="<?php if ( $step > 4 ) {
							echo 'completed';
						} ?> <?php if ( $step == 4 ) {
							echo 'info';
						} ?>" data-toggle="tooltip" data-placement="top" title="Directors"><span class="bubble"><span
                                        class="title">4</span></span></li>
                        <li class="<?php if ( $step > 5 ) {
							echo 'completed';
						} ?> <?php if ( $step == 5 ) {
							echo 'info';
						} ?>" data-toggle="tooltip" data-placement="top" title="Premium Membership"><span class="bubble"><span
                                        class="title"><span class="glyphicon glyphicon-flag"></span></span></span></li>
                    </ul>

                    <ul id="progresstitles">
                        <li><span <?php if ( $step == 1 ) {
								echo 'style="display:block"';
							} ?>>Login and Company Details</span></li>
                        <li><span <?php if ( $step == 2 ) {
								echo 'style="display:block"';
							} ?>>Enterprise Credentials</span></li>
                        <li><span <?php if ( $step == 3 ) {
								echo 'style="display:block"';
							} ?>>Enterprise Documents</span></li>
                        <li><span <?php if ( $step == 4 ) {
								echo 'style="display:block"';
							} ?>>Directors</span></li>
                        <li><span <?php if ( $step == 5 ) {
								echo 'style="display:block"';
							} ?>>Premium Membership</span></li>
                    </ul>


					<?php display_messages(); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert alert-danger" id="validationErrors" style="display:none;">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                <strong>Validation Errors</strong> - Please ensure that you complete all the required fields correctly.
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
							<?php if ( $step == 5 ) {
								echo '<form id="membership" method="post" action="' . base_url() . 'payment" enctype="multipart/form-data">';
							} else {
								echo '<form id="form" method="post" enctype="multipart/form-data">';
							} ?>
                            <input type="hidden" name="fp" value="1">
                            <input type="hidden" name="wizaction" id="wizaction" value="1">
                            <input type="hidden" name="step" id="step" value="<?php echo $step; ?>">

							<?php $this->load->view( 'web/registration/view_step' . $step ); ?>

							<?php if ( $step > 1 ) { ?>
                                <a href="#" class="btn button-sm button-theme" onclick="return PreviousStep();"><span
                                            class="glyphicon glyphicon-backward"></span> Previous Step </a>
							<?php } ?>
							<?php if ( $step == 5 ) { ?>
                                <a href="<?php echo base_url(); ?>myprofile" class="btn button-sm button-theme"> Return to Profile <span
                                            class="glyphicon glyphicon-flag"></span></a>
							<?php } ?>

							<?php if ( $step != '5' ) { ?>
                                <a href="#" class="btn button-sm button-theme" onclick="return NextStep();">Next Step/Save <span
                                            class="glyphicon glyphicon-forward"></span></a>
							<?php } ?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- My profile end -->

	<?php $this->load->view( 'web/common/inc-partners' ); ?>

	<?php $this->load->view( 'web/common/inc-footer' ); ?>

	<?php $this->load->view( 'web/common/inc-html-footer' ); ?>

    <script>

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

        function NextStep() {
			<?php if($step == 0) { ?>
            if (!$('#cbAgree').is(":checked")) {
                alert('You are required to accept the Terms and Conditions before you can register.');
                return;
            }
			<?php } ?>

            var validated = true;
            $(':input[required]:visible').each(function () {
                if ($(this).val() == '') {
                    $(this).css("border", "2px solid red");
                    validated = false;
                }
            });
            /*$(':select[required]:visible').each(function() {
				if ($(this).val() == '') {
				   $(this).css("border", "2px solid red");
				   validated = false;
			   }
			});*/

			<?php if($step == 1) { ?>
            if (!validateEmail($('#companyemail').val())) {
                $('#companyemail').css("border", "2px solid red");
                validated = false;
            }
            if (!validateEmail($('#email').val())) {
                $('#email').css("border", "2px solid red");
                validated = false;
            }
            if ($('#vatnumber').val() != '') {
                if ($('#vatnumber').val().length > 10) {
                    alert('VAT number limited to 10 characters.');
                    $('#vatnumber').css("border", "2px solid red");
                    validated = false;
                }
            }
			<?php } ?>

			<?php if($step == 2) { ?>
            if ($('#blackownershippercent').val() != '') {
                var val = $('#blackownershippercent').val() * 1;
                if (val > 100) {
                    alert('Ownership cannot exceed 100%');
                    $('#blackownershippercent').css("border", "2px solid red");
                    validated = false;
                }
            }
            if ($('#blackwomenownershippercent').val() != '') {
                var val = $('#blackwomenownershippercent').val() * 1;
                if (val > 100) {
                    alert('Ownership cannot exceed 100%');
                    $('#blackwomenownershippercent').css("border", "2px solid red");
                    validated = false;
                }
            }

			<?php } ?>



            if (validated) {
                $('#validationErrors').hide();
                $('#step').val(<?php echo $step + 1;?>);
                $('#form').submit();
                return true;
            }
            else {
                $('#validationErrors').show();
            }
        }

        function PreviousStep() {
            $('#step').val(<?php echo $step - 1;?>);
            $('#wizaction').val(2);
            $('#form').submit();
            return false;
        }

        jQuery(document).ready(function () {
            $('input').change(function () {
                $(this).css("border", "1px solid #e8e7e7");
            });
            $('select').change(function () {
                $(this).css("border", "1px solid #e8e7e7");
            });
            $('[data-toggle="tooltip"]').tooltip();

            $('#propertysubsectorid').on('hidden.bs.select', function (e) {
                if ($(this).val() == 450) {
                    $('#propertysubsectorother').show();
                }
            });

            $('#propertyassociations').on('hidden.bs.select', function (e) {
                if ($(this).val() == 518) {
                    $('#propertyassociationother').show();
                }
            });

            $('#propertyassociations').on('hidden.bs.select', function (e) {
                if ($(this).val() == 499) {
                    $('#propertyassociationother').show();
                }
            });

            $('#sd_proassociations').on('hidden.bs.select', function (e) {
                if (($(this).val() == 521) || ($(this).val() == 502)) {
                    $('#sd_proassociationother').show();
                }
            });
        });

    </script>
