<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<div class="agent-section content-area">
    <div class="container">

        <!-- option bar end -->
        <div class="clearfix"></div>

        <?php

            $where = '';
            $params = array();
            if((sget('service') != '') && (sget('service') != 'Service'))
            {
                if($where != '')
                  $where .= ' and ';
                $where .= ' propertysubsectorid = ?';
                $params[] = sget('service');
            }

            if((sget('beestatus') != '') && (sget('beestatus') != 'BBBEE Status'))
            {
                if($where != '')
                  $where .= ' and ';
                $where .= ' beestatusid = ?';
                $params[] = sget('beestatus');
            }

            if((sget('location') != '') && (sget('location') != 'Location'))
            {
                if($where != '')
                  $where .= ' and ';
                $where .= ' locationid = ?';
                $params[] = sget('location');
            }

            if(sget('company') != '')
            {
                if($where != '')
                  $where .= ' and ';
                $where .= ' companyname like ?';
                $params[] = '%' . sget('company') . '%';
            }

            $sql = 'select * from user ';
            if($where != '')
              $sql = $sql . ' where ' . $where;

            $rs = $this->db->query($sql,$params);
            //echo $this->db->last_query();
        ?>

        <div class="row">
            <div class="col-lg-9">
                <!-- Agent box big start -->
                <?php
                  if($rs->num_rows() == 0) echo '<p>No search results returned.</p>';
                  foreach($rs->result() as $row) { ?>
                <div class="agent-box-big clearfix">
                    <div class="col-md-2 agent-content detail">
                        <?php if($row->companylogo != '') echo '<img src="' . UPLOADURL . $row->companylogo . '" width="100">'; ?>
                    </div>
                    <div class="col-md-10 agent-content detail">
                        <!-- Title -->
                        <h1 class="title">
                            <a href="/companyprofile/<?php echo encuri($row->userid); ?>"><?php echo $row->companyname; ?></a>
                        </h1>

                        <!--<p>Morbi accumsan ipsum velit nam nec tellus a odio tincidunt auctor a ornare odio sed non mauris<span class="hidden-sm">  vitae erat consequat auctor eu in elit class aptent taciti sociosqu ad litora torquent per conubia nostra per inceptos him enaeos mauris in erat</span> </p>-->
                        <!-- footer -->
                        <div class="footer clearfix">
                            <div class="pull-left" style="color:#aaaaaa;">
                                <div class="row">
                                    <div class="col-md-4"><?php echo get_lookup_value($row->userroleid); ?></div>
                                    <div class="col-md-4"><?php echo get_lookup_value($row->locationid); ?></div>
                                    <div class="col-md-4"><?php echo get_lookup_value($row->beestatusid); ?></div>
                                </div>
                            </div>
                            <div class="pull-right">
                                <a href="/companyprofile/<?php echo encuri($row->userid); ?>" class="btn button-sm button-theme">Details</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>


                <!-- Agent box big end -->
            </div>
            <div class="col-lg-3">
                <img src="http://www.saibppbd.co.za/clientassets/img/ads/advert_search.png">
            </div>
        </div>
        <!-- Page navigation start -->
        <!--<nav aria-label="Page navigation">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <i class="fa fa-angle-left"></i>
                    </a>
                </li>
                <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </li>
            </ul>
        </nav>-->
        <!-- Page navigation end -->
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
