<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<div class="content-area my-profile">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="my-address">
                    <h1 class="title">
                        <?php echo $pagetitle; ?>
                    </h1>
                    <hr>

                    <?php display_messages(); ?>
                    <div class="row">
                      <div class="col-md-12">
                         <div class="alert alert-danger" id="validationErrors" style="display:none;">
                             <button type="button" class="close" data-dismiss="alert">&times;</button>
                             <strong>Validation Errors</strong> - Please ensure that you complete all the required fields correctly.
                         </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                         <form id="form" method="post" enctype="multipart/form-data">
                             <input type="hidden" name="fp" value="1">

                             <p>Select which categories you want to be notified about for new Tenders, RFI's, RFQ's, etc.</p>
                             <table width="60%">
                             <thead>
                               <tr>
                                 <td><strong>Category</strong></td>
                                 <td align="right"><strong>Receive Notifications?</strong></td>
                               </tr>
                               <?php

                                   $userid = $this->session->userdata('userid');
                                   $rsuser = $this->user_model->get($userid);
                                   foreach($rs as $row)
                                   {
                                     $selected = '';
                                     if(contains($rsuser->notifications,$row->lookupid))
                                       $selected = 'checked="checked"';

                                     ?>
                                     <tr style="height:25px;">
                                       <td><?php echo $row->lookupvalue; ?></td>
                                       <td align="right">
                                         <input type="checkbox" name="notifications[]" value="<?php echo $row->lookupid; ?>" <?php echo $selected; ?>>
                                       </td>
                                     </tr>
                                     <?php
                                   }

                               ?>
                             </thead>
                             </table>
                             <Br />

                             <a href="/myprofile" class="btn button-md button-theme">Cancel </a>
                             <a href="#" onclick="SaveSettings();" class="btn button-md button-theme">Save </a>
                         </form>
                       </div>
                     </div>
                  </div>
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>

<script>

  function SaveSettings()
  {
    $('#form').submit();
  }

</script>
