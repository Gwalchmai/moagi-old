<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<?php //$this->load->view('web/common/inc-search'); ?>

<!-- About city estate start -->
<div class="about-city-estate">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="about-text">
                    <div class="main-title-2">
                        <h1>Enterprise Development / Supplier Development in the Property Sector</h1>

                        <p>Following the release of the 2015 State of Transformation Report which aims to track the progress of transformation in the property sector, it is evident that the sector is still plagued by highly skewed ownership patterns. This has a detrimental impact on the entire value chain and the ability of the sector to develop and nurture new entrants.</p>

                        <p>the SAIBPP Business Directory is therefore an important SAIBPP initiative which will assist in providing for that needed attention around all B-BBEE elements by facilitating a marketplace in the property sector which promotes and aids economic activity for black owned companies.</p>

                        <p>Download both documents here:</p>

                        <ul>
                           <li><a href="/clientassets/docs/Gazette_No_40910-Amended_Property_Sector_Code.pdf" class="link" target="_blank">Property Sector Charter</a>
                           <li><a href="/clientassets/docs/PSCC_SOT_Report_Final_2017.pdf" class="link" target="_blank">2015/2016 State of Transformation Report for The South African Property Sector</a>
                        </ul>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- About city estate end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
