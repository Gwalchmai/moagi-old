<?php $this->load->view( 'web/common/inc-html-header' ); ?>

<div class="page_loader"></div>

<?php $this->load->view( 'web/common/inc-header' ); ?>

<!-- Agent section start -->
<div class="agent-section content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <!-- Agent box list start -->
                <div class="agent-box-big clearfix">
                    <div class="col-lg-9 col-md-9 col-sm-9 agent-content">
                        <div class="row">
							<?php if ( $rs->companylogo != '' ) {
								echo '<img src="' . UPLOADURL . $rs->companylogo . '" align="left" width="125" style="padding-left: 15px; padding-right: 10px;">';
							} ?>
                            <h1 class="title">
								<?php echo val( $rs, 'companyname' ); ?>

                            </h1>
                        </div>

                        <h4 class="title">Contact Details</h4>

                        <div class="row">
                            <div class="col-md-5"><strong>Trading Name:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'tradingname', 'N/A' ); ?>
								<?php
								$checkfield = 'ver_tradename';
								$value      = val( $rs, $checkfield );
								echo is_verified( $checkfield, $value );
								?>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Company Type:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'companytypeid' ) ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Company Province:</strong></div>
                            <div class="col-md-4">
								<?php
								echo val( $rs, 'companyaddress' );
								echo get_lookup_value( val( $rs, 'locationid' ) );
								$verified_logo = is_verified( $rs, 'ver_address' );
								echo $verified_logo;
								?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Industry Sub-Sector:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'industrysectorid' ) ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Property Industry:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'propertysubsectorid' ) ); ?></div>
                        </div>


                        <h4 class="title">Contact Details</h4>


                        <div class="row">
                            <div class="col-md-5"><strong>Contact Name:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'name' ); ?><?php echo val( $rs, 'surname' ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Contact Designation:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'designation' ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Contact Email:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'email' ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Contact Tel:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'tel' ); ?><?php
								$verified_logo = is_verified( $rs, 'ver_telnumbers' );
								echo $verified_logo;
								?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Company Email:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'companyemail' ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><strong>Years Operating:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'yearsoperating' ); ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-5"><h4>Trade References</h4></div>
                            <div class="col-md-4">
								<?php
								$verified_logo = is_verified( $rs, 'ver_tradereferences' );
								echo $verified_logo;
								?>
                            </div>
                        </div>

						<?php for ( $i = 1; $i <= 3; $i ++ ) {
							?>
                            <div class="row">
                                <div class="col-md-5"><strong>Company Name (<?php echo $i; ?>):</strong></div>
                                <div class="col-md-4"><?php echo val( $rs, 'tr_companyname' . $i ); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><strong>Description Of project, service, product provision (<?php echo $i; ?>
                                        ):</strong></div>
                                <div class="col-md-4"><?php echo val( $rs, 'tr_description' . $i ); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><strong>Client Project Manager (<?php echo $i; ?>):</strong></div>
                                <div class="col-md-4"><?php echo val( $rs, 'tr_contact' . $i ); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><strong>Contact Number (<?php echo $i; ?>):</strong></div>
                                <div class="col-md-4"><?php echo val( $rs, 'tr_contactnumber' . $i ); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><strong>Contact Email (<?php echo $i; ?>):</strong></div>
                                <div class="col-md-4"><?php echo val( $rs, 'tr_contactemail' . $i ); ?></div>
                            </div>
                            <div class="row">
                                <div class="col-md-5"><strong>Project Cost in Rands (<?php echo $i; ?>):</strong></div>
                                <div class="col-md-4"><?php echo val( $rs, 'tr_projectcost' . $i ); ?></div>
                            </div>
                            <br>

							<?php
						}
						?>


                        <h4 class="title">Enterprise Credentials</h4>

                        <div class="row">
                            <div class="col-md-5"><strong>Company Registration Number:</strong></div>
                            <div class="col-md-4"><?php $verified_logo = is_verified( $rs, 'ver_companyregno' );
								echo $verified_logo; ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Black Ownership:</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'blackownershippercent' ); ?>%</div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Black Women Ownership :</strong></div>
                            <div class="col-md-4"><?php echo val( $rs, 'blackwomenownershippercent' ); ?>%</div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>B-BBEE Status:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'beestatusid' ) ); ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>B-BBEE Certificate:</strong></div>
                            <div class="col-md-4"><?php if ( get_lookup_value( val( $rs, 'bbbeecertificate' ) ) == '' ) {
									echo 'N/A';
								} else { ?><a href="<?php echo UPLOADURL . get_lookup_value( val( $rs, 'beestatusid' ) ); ?>"
                                              class="link" target="_blank">View</a><?php } ?>
								<?php $verified_logo = is_verified( $rs, 'ver_bee' );
								echo $verified_logo; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Enterprise Size:</strong></div>
                            <div class="col-md-4"><?php echo htmlentities( get_lookup_value( val( $rs, 'enterprisesizeid' ) ) ); ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Empowering Supplier:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'empoweringsupplierid' ) ); ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Black Company Status:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'blackcompanystatusid' ) ); ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>VAT Registered:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'vatregisteredid' ) ); ?>
								<?php $verified_logo = is_verified( $rs, 'ver_vatregno' );
								echo $verified_logo; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Business Classification:</strong></div>
                            <div class="col-md-4"><?php echo get_lookup_value( val( $rs, 'businessclassificationid' ) ); ?></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Banking Information:</strong></div>
                            <div class="col-md-4"><?php $verified_logo = is_verified( $rs, 'ver_bankingdetails' );
								echo $verified_logo; ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-9"><h4 class="title">Enterprise Documents<span class="pull-right">Available</span></h4>
                            </div>

                            <div class="col-md-5"><strong>Tax Clearance Certificate:</strong></div>
                            <div class="col-md-4"><span class="pull-right"><?php if ( val( $rs, 'taxclearancecert' ) == '' ) {
										echo 'No';
									} else {
										echo 'Yes';
									} ?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Workman's Compensation:</strong></div>
                            <div class="col-md-4"><span class="pull-right"><?php if ( val( $rs, 'workmanscompensation' ) == '' ) {
										echo 'No';
									} else {
										echo 'Yes';
									} ?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5"><strong>Public Liability:</strong></div>
                            <div class="col-md-4"><span class="pull-right"><?php if ( val( $rs, 'publicliability' ) == '' ) {
										echo 'No';
									} else {
										echo 'Yes';
									} ?></span></div>
                        </div>

                        <h4 class="title">Directors</h4>

                        <div class="row">
                            <div class="col-md-5"><strong>Directors:</strong></div>
                            <div class="col-md-4"><?php $verified_logo = is_verified( $rs, 'ver_directors' );
								echo $verified_logo; ?></div>
                        </div>

                        <div class="row">
                            <div class="col-md-3"><strong>Title</strong></div>
                            <div class="col-md-3"><strong>Name</strong></div>
                            <div class="col-md-3"><strong>Surname</strong></div>
							<?php
							$rssd = $this->sd_model->find_all( array( 'userid' => val( $rs, 'userid' ) ) );
							foreach ( $rssd as $row ) {
								?>
                                <div class="row">
                                    <div class="col-md-3"><?php echo get_lookup_value( $row->sd_titleid ); ?></div>
                                    <div class="col-md-3"><?php echo get_lookup_value( $row->sd_firstname ); ?></div>
                                    <div class="col-md-3"><?php echo get_lookup_value( $row->sd_lastname ); ?></div>
                                </div>
								<?php
							}
							?>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <img src="http://www.saibppbd.co.za/clientassets/img/ads/advert_profile.png" style="margin-top: 125px;">
                    </div>
                    <!-- Agent box list end -->
                </div>
            </div>

        </div>
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view( 'web/common/inc-partners' ); ?>

<?php $this->load->view( 'web/common/inc-footer' ); ?>

<?php $this->load->view( 'web/common/inc-html-footer' ); ?>
