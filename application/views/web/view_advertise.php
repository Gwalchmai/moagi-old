<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<?php //$this->load->view('web/common/inc-banner'); ?>

<!-- About city estate start -->
<div class="about-city-estate">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="about-text">
                    <div class="main-title-2">
                        <h1>Advertise</h1>
                        <h4>To download advertising spec sheet <a href="http://www.moagi.org/clientassets/img/rate_card.pdf" style="color: #1a4580; !important;" target="_blank">Click Here</a></h4>
                        <h4>For advertising rates contact <a href="mailto:lebo@saibpp.co.za?subject=Advertising rates enquiry" style="color: #1a4580; !important;">Lebo Ngcobo</a> </h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About city estate end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>
