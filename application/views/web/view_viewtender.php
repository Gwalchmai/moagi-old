<?php $this->load->view('web/common/inc-html-header'); ?>

<div class="page_loader"></div>

<?php $this->load->view('web/common/inc-header'); ?>

<!-- Agent section start -->
<div class="content-area my-profile">
   <div class="container">
      <div class="row">
         <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="my-address">
                    <h1 class="title">
                        <?php echo $pagetitle; ?>
                    </h1>
                    <hr>

                    <?php display_messages(); ?>
                    <div class="row">
                      <div class="col-md-12">
                         <div class="alert alert-danger" id="validationErrors" style="display:none;">
                             <button type="button" class="close" data-dismiss="alert">&times;</button>
                             <strong>Validation Errors</strong> - Please ensure that you complete all the required fields correctly.
                         </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12">
                         <form id="form" method="post" enctype="multipart/form-data">
                             <input type="hidden" name="fp" value="1">

                             <div class="form-group">
                                <label>Category <em class="req">*</em> :</label>
                                <?php $fieldname = "tendercategoryid"; ?>
                                <?php echo get_lookup_value(val($rs,$fieldname)); ?>
                             </div>

                             <div class="form-group">
                                 <label>Tender Number <em class="req">*</em> :</label>
                                 <?php $fieldname = "tendernumber"; ?>
                                 <?php echo val($rs,$fieldname,sget($fieldname)); ?>
                             </div>

                             <div class="form-group">
                                 <label>Description: </label>
                                 <?php $fieldname = "description"; ?>
                                 <?php echo nl2br(val($rs,$fieldname,sget($fieldname))); ?>
                             </div>

                             <div class="form-group">
                                 <label>Date Published <em class="req">*</em> : </label>
                                 <?php $fieldname = "tenderdate"; ?>
                                 <?php echo showdate(val($rs,$fieldname,sget($fieldname))); ?>
                             </div>

                             <div class="form-group">
                                 <label>Time Published: </label>
                                 <?php $fieldname = "tenderdatetime"; ?>
                                 <?php echo val($rs,$fieldname,sget($fieldname)); ?>
                             </div>

                             <div class="form-group">
                                 <label>Closing Date: </label>
                                 <?php $fieldname = "tenderclosedate"; ?>
                                 <?php echo substr(val($rs,$fieldname,sget($fieldname)),0,10); ?>
                             </div>

                             <div class="form-group">
                                 <label>Closing Time: </label>
                                 <?php $fieldname = "tenderclosedatetime"; ?>
                                 <?php echo val($rs,$fieldname,sget($fieldname)); ?>
                             </div>

                             <div class="form-group">
                                 <label>Briefing Session Date: </label>
                                 <?php $fieldname = "briefingsessiondate"; ?>
                                 <?php echo substr(val($rs,$fieldname,sget($fieldname)),0,10); ?>
                             </div>

                             <div class="form-group">
                                 <label>Briefing Session Time: </label>
                                 <?php $fieldname = "briefingsessiontime"; ?>
                                 <?php echo val($rs,$fieldname,sget($fieldname)); ?>
                             </div>

                             <div class="form-group">
                                 <label>Tender Document: </label>
                                 <?php $fieldname = "docurl"; ?>
                                 <?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<a href="' . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a>';
                                 $fieldname = "tenderdocument"; ?>
                                 <?php if(val($rs,$fieldname,sget($fieldname)) != '') echo '<a href="' . UPLOADURL . val($rs,$fieldname,sget($fieldname)) . '" target="_blank">View</a>'; ?>
                             </div>

                         </form>
                       </div>
                     </div>
                  </div>
            </div>
        </div>
    </div>
</div>
<!-- Agent section end -->

<?php $this->load->view('web/common/inc-partners'); ?>

<?php $this->load->view('web/common/inc-footer'); ?>

<?php $this->load->view('web/common/inc-html-footer'); ?>

<script>

  function SaveTender()
  {
    var validated = true;
    $(':input[required]:visible').each(function() {
        if ($(this).val() == '') {
           $(this).css("border", "2px solid red");
           validated = false;
       }
    });

    if(validated)
    {
       $('#validationErrors').hide();
       $('#form').submit();
       return true;
    }
    else {
       $('#validationErrors').show();
    }
  }

</script>
