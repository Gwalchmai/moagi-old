<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adminutils extends CI_Controller {

	public function index()
	{
		admin_checklogin();

	}

    public function download()
    {
        $filename = decuri($this->uri->segment(4));
        $title = decuri($this->uri->segment(5));
        $ext = get_file_extension(UPLOADPATH . $filename);
        header("Content-Disposition: attachment; filename=" . $title . '.' . $ext);
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Description: File Transfer");
        header("Content-Length: " . filesize(UPLOADPATH . $filename));
        flush(); // this doesn't really matter.

        $fp = fopen(UPLOADPATH . $filename, "r");
        while (!feof($fp))
        {
            echo fread($fp, 65536);
            flush(); // this is essential for large downloads
        }
        fclose($fp);
    }

    public function imageupload()
    {
        $dir = UPLOADPATH;

        $_FILES['file']['type'] = strtolower($_FILES['file']['type']);

        if ($_FILES['file']['type'] == 'image/png'
        || $_FILES['file']['type'] == 'image/jpg'
        || $_FILES['file']['type'] == 'image/gif'
        || $_FILES['file']['type'] == 'image/jpeg'
        || $_FILES['file']['type'] == 'image/pjpeg')
        {
            // setting file's mysterious name
            $filename = md5(date('YmdHis')).'.jpg';
            $file = $dir.$filename;

            // copying
            copy($_FILES['file']['tmp_name'], $file);

            // displaying file
            $array = array(
                'filelink' => UPLOADURL . $filename
            );

            echo stripslashes(json_encode($array));

        }
    }

    public function getimages()
    {
        $json = '[';
        $jsondata = '';
        $d = dir(UPLOADPATH);
        while (false !== ($entry = $d->read())) {
            $ext = strtoupper(get_file_extension($entry));
            if(($ext == 'PNG') || ($ext == 'JPG') || ($ext == 'GIF'))
            {
                if($jsondata != '')
                    $jsondata .= ',';
                $jsondata .= '{"thumb": "' . UPLOADURL . $entry . '","image": "' . UPLOADURL . $entry . '" }';
            }

        }
        $json .= $jsondata . ']';
        echo $json;
    }
}

/* End of file adminutils.php */
/* Location: ./application/controllers/admin/adminutils.php */
