<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

	public function index()
	{

	}

	public function getdirector()
   {
		header('Access-Control-Allow-Origin: *');
      header('Content-Type: application/json');
      $obj = new stdClass();
      $obj->success = true;

		$rs = $this->db->query('select * from sd where sd_id = ?',array($_GET['sd_id']));
		$obj->sd_id = $rs->row(0)->sd_id;
		$obj->sd_typeid = $rs->row(0)->sd_typeid;
		$obj->sd_titleid = $rs->row(0)->sd_titleid;
		$obj->sd_firstname = $rs->row(0)->sd_firstname;
		$obj->sd_lastname = $rs->row(0)->sd_lastname;
		$obj->sd_countryid = $rs->row(0)->sd_countryid;
		$obj->sd_idnumber = $rs->row(0)->sd_idnumber;
		$obj->sd_passport = $rs->row(0)->sd_passport;
		$obj->sd_civilservantid = $rs->row(0)->sd_civilservantid;
		$obj->sd_disabledid = $rs->row(0)->sd_disabledid;
		$obj->sd_directorsqualification = $rs->row(0)->sd_directorsqualification;

      echo json_encode($obj);
   }

	public function deletedirector()
   {
		header('Access-Control-Allow-Origin: *');
      header('Content-Type: application/json');
      $obj = new stdClass();
      $obj->success = true;

      $this->db->query('delete from sd where sd_id = ?',array($_GET['sd_id']));

      echo json_encode($obj);
   }



}

/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */
