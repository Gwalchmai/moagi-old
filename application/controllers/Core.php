<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core extends MY_Controller {

	public function index()
	{
		checkadminlogin();
		$this->load->view('core/view_dashboard',array(
			'mainmenu' => 'dashboard',
			'submenu' => '',
			'pagetitle' => 'Dashboard'
		));
	}

	public function login()
	{
		if(fp())
		{
			$rs = $this->admin_model->find_all(array('email' => sget('email'),'password' => sget('pwd'),'enabled' => '1'),'*');
			if(count($rs) == 0)
				error_msg('Invalid email address or password. Or your account is locked. Please contact the system administrator.');
			else
			{

				$this->session->set_userdata('admin',$rs[0]);

				header('Location: /admin/users');
				exit;
			}
		}
		$this->load->view('core/view_login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		header('Location: /core');
	}

	public function profile()
	{
		checkadminlogin();
		if(fp())
		{
			$error = '';
			if(sget('pwd') != '')
			{
				if(sget('pwd') != sget('pwd_confirm'))
					$error = 'Your passwords do not match. Please re-enter them carefully.';
			}

			if(!$error)
			{
				$data = array();
				$data['firstname'] = sget('firstname');
				$data['lastname'] = sget('lastname');
				$data['email'] = sget('email');
				$data['mobile'] = sget('mobile');
				$data['landline'] = sget('landline');
				if(sget('pwd') != '')
					$data['password'] = md5(sget('pwd'));

				$this->user_model->update(user()->userid,$data);
				$this->session->set_userdata('user',$this->user_model->get(user()->userid));

				success_msg('User profile updated successfully!');
			}
			else {
				error_msg($error);
			}
		}
		$rs = $this->user_model->get(user()->userid);
		$this->load->view('core/view_myprofile',array(
			'rs' => $rs,
			'mainmenu' => '',
			'submenu' => '',
			'pagetitle' => 'My Profile'
		));
	}

	public function forgotpassword()
	{
		if(fp())
		{
			$rs = $this->user_model->get_by('email',sget('email'));
			if(count($rs) == 0)
				error_msg('Your email address was not found on the system.');
			else {
				$url = base_url('/core/resetpassword/' . encuri($rs->userid));
				$msg = '<p>Dear ' . $rs->firstname . ',</p>
						  <p>Please click on the link below, or copy and paste it into a browser window to reset your password:</p>
						  <p><a href="' . $url . '">'. $url . '</a></p>
						  <p>Regards,<br />Support';
						  echo $msg;
						  exit;
				//sendemail($rs->email,'Password Reset',$msg,null);

			}
		}
		$this->load->view('core/view_forgotpassword');
	}

	public function resetpassword()
	{
		if(fp())
		{
			if(sget('pwd') != sget('pwd_confirm'))
				error_msg('Your passwords do not match!');
			else
			{
				$userid = decuri($this->uri->segment(3));
				$this->user_model->update($userid,array('password' => md5(sget('pwd'))));
				success_msg('Password updated successfully! Please login.');
				header('Location: /core/login');
				exit;
			}
		}
		$this->load->view('core/view_resetpassword');
	}

	public function alerts()
	{
		if($this->uri->segment(3) == 'action')
		{
			$this->alert_model->update(decuri($this->uri->segment(4)),array('readon' => mysqltime()));
			success_msg('Alert marked as actioned!');
		}
		else if($this->uri->segment(3) == 'delete')
		{
			$this->alert_model->update(decuri($this->uri->segment(4)),array('deletedon' => mysqltime()));
			success_msg('Alert deleted!');
		}
		$this->load->view('core/view_alerts',array(
			'mainmenu' => '',
			'submenu' => '',
			'pagetitle' => 'Alerts'
		));
	}
}
