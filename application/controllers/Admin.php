<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	public function index()
	{

	}

	public function users()
	{
		checkadminlogin();

		$editview = 'admin/view_users_edit';
		$listview = 'admin/view_users';
		$mainmenu = 'admin';
		$submenu = 'users';

		$addtitle = 'Add User';
		$addedtitle = 'User Added';
		$edittitle = 'Edit User';
		$editedtitle = 'User Edited';
		$deletedtitle = 'User Deleted';

		$listtitle = 'Companies';

		$listurl = '/admin/users';

		if($this->uri->segment(3) == 'add')
		{
		   if(fp())
		   {
				 $data = array();
				 $data['createdon'] = mysqltime();
				 $data['firstname'] = sget('firstname');
 				 $data['lastname'] = sget('lastname');
 				 $data['email'] = sget('email');
 				 $data['mobile'] = sget('mobile');
 				 $data['landline'] = sget('landline');
				 $data['companyid'] = nullie(sget('companyid'));
				 $data['enabled'] = nullie(sget('enabled'));
				 $data['userroleid'] = sget('userroleid');
 				 if(sget('pwd') != '')
 				    $data['password'] = md5(sget('pwd'));
				 $this->user_model->insert($data);

		       success_msg($addedtitle);
		       header('Location: ' . $listurl);
		       exit;
		   }
		   else
		   {
		       $this->load->view($editview,array(
		           'rs' => null,
					  'pagetitle' => $addtitle,
					  'mainmenu' => $mainmenu,
		  		     'submenu' => $submenu
		       ));
		       return;
		   }
		}
		else if($this->uri->segment(3) == 'edit')
		{
		   $id = decuri($this->uri->segment(4));
		   if(fp())
		   {
				$data = array();
				$data['firstname'] = sget('firstname');
				$data['lastname'] = sget('lastname');
				$data['email'] = sget('email');
				$data['mobile'] = sget('mobile');
				$data['landline'] = sget('landline');
				$data['companyid'] = nullie(sget('companyid'));
				$data['enabled'] = nullie(sget('enabled'));
				$data['userroleid'] = sget('userroleid');
				if(sget('pwd') != '')
					$data['password'] = md5(sget('pwd'));
				$this->user_model->update($id,$data);

				success_msg($editedtitle);
		      header('Location: ' . $listurl);
		      exit;
		   }
		   else
		   {
		       $rs = $this->user_model->get($id);
		       $this->load->view($editview,array(
		           'rs' => $rs,
		           'pagetitle' => $edittitle,
					  'mainmenu' => $mainmenu,
		  		     'submenu' => $submenu
		       ));
		       return;
		   }
		}
		else if($this->uri->segment(3) == 'verify')
		{
		   $id = decuri($this->uri->segment(4));
		   if(fp())
		   {
				$data = array();
				$data['ver_companyreg'] = sget('ver_companyreg');
				$data['ver_tradename'] = nullie(sget('ver_tradename'));
				$data['ver_companyregno'] = nullie(sget('ver_companyregno'));
				$data['ver_vatregno'] = nullie(sget('ver_vatregno'));
				$data['ver_bankingdetails'] = nullie(sget('ver_bankingdetails'));
				$data['ver_directors'] = nullie(sget('ver_directors'));
				$data['ver_financials'] = nullie(sget('ver_financials'));
				$data['ver_telnumbers'] = nullie(sget('ver_telnumbers'));
				$data['ver_address'] = nullie(sget('ver_address'));
				$data['ver_bee'] = nullie(sget('ver_bee'));
				$data['ver_tradereferences'] = nullie(sget('ver_tradereferences'));
				$this->user_model->update($id,$data);

				success_msg($editedtitle);
		      header('Location: ' . $listurl);
		      exit;
		   }
		   else
		   {
		       $rs = $this->user_model->get($id);
		       $this->load->view('admin/view_users_verify',array(
		           'rs' => $rs,
		           'pagetitle' => 'Verify Company Information',
					  	 'mainmenu' => $mainmenu,
		  		     'submenu' => $submenu
		       ));
		       return;
		   }
		}
		else if($this->uri->segment(3) == 'delete')
		{
		   $id = decuri($this->uri->segment(4));
		   $this->user_model->update($id,array('deletedon' => mysqltime()));

		   success_msg($deletedtitle);
		}

		$this->load->view($listview,array(
		   'pagetitle' => $listtitle,
		   'mainmenu' => $mainmenu,
		   'submenu' => $submenu,
			'addtitle' => $addtitle,
			'listurl' => $listurl
		));
	}

	public function tenders()
	{
		checkadminlogin();

		$editview = 'admin/view_tenders_edit';
		$listview = 'admin/view_tenders';
		$mainmenu = 'admin';
		$submenu = 'tenders';

		$addtitle = 'Add Tender';
		$addedtitle = 'Tender Added';
		$edittitle = 'Edit Tender';
		$editedtitle = 'Tender Edited';
		$deletedtitle = 'Tender Deleted';

		$listtitle = 'Tenders/Opportunities';

		$listurl = '/admin/tenders';

		if($this->uri->segment(3) == 'add')
		{
		   if(fp())
		   {
				 $data = array();
				 //$data['userid'] = $this->session->userdata('userid');
		 		 $data['createdon'] = mysqltime();
		 		 $data['tendercategoryid'] = sget('tendercategoryid');
		 		 $data['description'] = sget('description');
		 		 $data['tendernumber'] = sget('tendernumber');
		 		 $data['tenderdate'] = sget('tenderdate');
		 		 $data['tenderclosedate'] = sget('tenderclosedate');
		 		 $data['tenderclosedatetime'] = sget('tenderclosedatetime');
		 		 $data['briefingsessiondate'] = sget('briefingsessiondate');
		 		 $data['briefingsessiontime'] = sget('briefingsessiontime');
		 		 $data['tenderdocument'] = upload_file('tenderdocument');
		 		 $data['tenderdatetime'] = sget('tenderdatetime');
				 $data['docurl'] = sget('docurl');
				 $this->tender_model->insert($data);

		       success_msg($addedtitle);
		       header('Location: ' . $listurl);
		       exit;
		   }
		   else
		   {
		       $this->load->view($editview,array(
		           'rs' => null,
					  'pagetitle' => $addtitle,
					  'mainmenu' => $mainmenu,
		  		     'submenu' => $submenu
		       ));
		       return;
		   }
		}
		else if($this->uri->segment(3) == 'edit')
		{
		   $id = decuri($this->uri->segment(4));
		   if(fp())
		   {
				$data = array();
				$data['tendercategoryid'] = sget('tendercategoryid');
				$data['description'] = sget('description');
				$data['tendernumber'] = sget('tendernumber');
				$data['tenderdate'] = sget('tenderdate');
				$data['tenderclosedate'] = sget('tenderclosedate');
				$data['tenderclosedatetime'] = sget('tenderclosedatetime');
				$data['briefingsessiondate'] = sget('briefingsessiondate');
				$data['briefingsessiontime'] = sget('briefingsessiontime');
				$data['tenderdocument'] = upload_file('tenderdocument');
				$data['tenderdatetime'] = sget('tenderdatetime');
				$data['docurl'] = sget('docurl');
				$this->tender_model->update($id,$data);

				success_msg($editedtitle);
		      header('Location: ' . $listurl);
		      exit;
		   }
		   else
		   {
		       $rs = $this->tender_model->get($id);
		       $this->load->view($editview,array(
		           'rs' => $rs,
		           'pagetitle' => $edittitle,
					  'mainmenu' => $mainmenu,
		  		     'submenu' => $submenu
		       ));
		       return;
		   }
		}
		else if($this->uri->segment(3) == 'delete')
		{
		   $id = decuri($this->uri->segment(4));
		   $this->tender_model->update($id,array('deletedon' => mysqltime()));

		   success_msg($deletedtitle);
		}

		$this->load->view($listview,array(
		   'pagetitle' => $listtitle,
		   'mainmenu' => $mainmenu,
		   'submenu' => $submenu,
			'addtitle' => $addtitle,
			'listurl' => $listurl
		));
	}



}
