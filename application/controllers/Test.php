<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends MY_Controller {

	public function index()
	{

	}

	public function phpinfo()
	{
		phpinfo();
	}

	public function pay()
	{
		CreatePayment();
	}

	public function testinvoice()
	{
		createinvoice();
	}

	public function mail()
	{
		$data = json_decode(json_encode(user()), true)[0];
		$rs = $this->user_model->get($data['userid']);
		welcome_email($rs);
	}


}
