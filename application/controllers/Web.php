<?php
defined( 'BASEPATH' ) OR exit( 'No direct script access allowed' );

class Web extends MY_Controller {

	public function index() {
		$this->load->view( 'web/view_home', array(
			'pagetitle' => 'SAIBPP | Property'
		) );
	}

	public function magazine( $iIssueID ) {
		$this->load->view( 'web/view_magazine', $iIssueID );
	}

	public function terms() {
		$this->load->view( 'web/view_terms', array(
			'pagetitle' => 'Terms and Conditions | SAIBPP'
		) );
	}

	public function continuereg() {
		$this->load->view( 'web/view_continuereg', array(
			'pagetitle' => 'Registration | SAIBPP'
		) );
	}

	public function about() {
		$this->load->view( 'web/view_about', array(
			'pagetitle' => 'About | SAIBPP'
		) );
	}

	public function entdev() {
		$this->load->view( 'web/view_entdev', array(
			'pagetitle' => 'Enterprise Development / Supplier Development in the Property Sector | SAIBPP'
		) );
	}

	public function membership() {
		$this->load->view( 'web/view_membership', array(
			'pagetitle' => 'Membership | SAIBPP'
		) );
	}

	public function confirmation() {
		$this->load->view( 'web/view_confirmation', array(
			'pagetitle' => 'Membership Confirmation | SAIBPP'
		) );
	}

	public function verification() {
		$this->load->view( 'web/view_verification', array(
			'pagetitle' => 'Verification | SAIBPP'
		) );
	}

	public function opportunities() {
		$this->load->view( 'web/view_opportunities', array(
			'pagetitle' => 'Opportunities | SAIBPP'
		) );
	}

	public function advertise() {
		$this->load->view( 'web/view_advertise', array(
			'pagetitle' => 'Advertise | SAIBPP'
		) );
	}

	public function news() {
		$this->load->view( 'web/view_news', array(
			'pagetitle' => 'News | SAIBPP'
		) );
	}

	public function contact() {
		if ( fp() ) {
			$msg = 'From: ' . sget( 'full-name' ) . '<br />' .
			       'Email: ' . sget( 'email' ) . '<br />' .
			       'Subject: ' . sget( 'subject' ) . ' <br />' .
			       'Phone: ' . sget( 'phone' ) . '<br />' .
			       'Message: ' . sget( 'message' );
			sendemail( 'admin@saibppbd.co.za', 'Contact Form', $msg, null );
			success_msg( 'Thank you, your email was received. We\'ll get back to you shortly.' );
		}
		$this->load->view( 'web/view_contact', array(
			'pagetitle' => 'Contact | SAIBPP'
		) );
	}

	public function chat() {
		$this->load->view( 'web/view_chat', array(
			'pagetitle' => 'Chat | SAIBPP'
		) );
	}

	public function logout() {
		$this->session->sess_destroy();
		header( 'Location: /' );
	}

	public function login() {
		if ( fp() ) {
			$rs = $this->user_model->find_all( array( 'email' => sget( 'email' ), 'password' => sget( 'password' ) ) );
			if ( count( $rs ) == 0 ) {
				error_msg( 'Invalid email address or password.' );
			} else {
				$this->session->set_userdata( 'userid', $rs[0]->userid );
				header( 'Location: /myprofile' );
				exit;
			}
		}
		$this->load->view( 'web/view_login', array(
			'pagetitle' => 'Login | SAIBPP'
		) );
	}

	public function registerquick() {
		if ( fp() ) {
			$data                         = array();
			$data['userroleid']           = sget( 'userroleid' );
			$data['name']                 = sget( 'name' );
			$data['surname']              = sget( 'surname' );
			$data['email']                = sget( 'email' );
			$data['tel']                  = sget( 'tel' );
			$data['companyname']          = sget( 'companyname' );
			$data['companyemail']         = sget( 'companyemail' );
			$data['companytel']           = sget( 'companytel' );
			$data['agreetermsconditions'] = sget( 'agreetermsconditions' );
			$data['otherdocuments']       = sget( 'otherdocuments' );

			$data['createdon'] = mysqltime();
			$data['refno']     = 'SB' . date( 'Ymd' ) . rand( 0, 9 ) . rand( 0, 9 ) . rand( 0, 9 );
			$data['password']  = generate_password( 6 );
			$this->user_model->insert( $data );
			$userid = insert_id( 'user' );
			$this->session->set_userdata( 'userid', $userid );
			$user = $this->user_model->get( $userid );
			welcome_email( $user );
			header( 'Location: /continuereg' );
			exit;
		}
		$this->load->view( 'web/view_registerquick', array(
			'pagetitle' => 'Register | SAIBPP'
		) );
	}

	public function changepassword() {
		if ( fp() ) {
			$userid = $this->session->userdata( 'userid' );
			if ( strlen( sget( 'newpassword' ) ) < 6 ) {
				error_msg( 'Please select a password of 6 characters or more.' );
			} else if ( sget( 'confirmnewpassword' ) != sget( 'newpassword' ) ) {
				error_msg( 'Your passwords do not match. Please re-enter them carefully!' );
						} else {
				$data             = array();
				$data['password'] = sget( 'newpassword' );
				$this->user_model->update( $userid, $data );
				success_msg( 'Your password was changed successfully!' );
				header( 'Location: /myprofile' );
				exit;
			}
		}
		$this->load->view( 'web/view_changepassword', array(
			'pagetitle' => 'Change Password | SAIBPP'
		) );
	}

	public function register() {
		$step = 0;
		if ( sget( 'step' ) != '' ) {
			$step = sget( 'step' );
		} else if ( isset( $_GET['step'] ) ) {
			$step = $_GET['step'];
		}


		/*if($step == 0)
			$this->session->unset_userdata('formdata');

		if($this->session->userdata('formdata') == null)
			$data = array();
		else
			$data = $this->session->userdata('formdata');*/

		//$rs = $data;

		if ( isset( $_GET['deleteattachment'] ) ) {
			$id = $this->session->userdata( 'userid' );
			$this->db->query( 'update user set ' . $_GET['deleteattachment'] . ' = null where userid = ?', array( $id ) );
			if ( isset( $_GET['gostep'] ) ) {
				header( 'Location: /register?step=' . $_GET['gostep'] );
				exit;
			}
		}

		if ( isset( $_GET['deletesdattachment'] ) ) {
			$id = $_GET['sd_id'];
			$this->db->query( 'update sd set sd_directorsqualification = null where sd_id = ?', array( $id ) );
			if ( isset( $_GET['gostep'] ) ) {
				header( 'Location: /register?step=' . $_GET['gostep'] );
				exit;
			}
		}

		if ( fp() ) {
			if ( ( $step == 2 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data                           = array();
				$data['userroleid']             = sget( 'userroleid' );
				$data['companyname']            = sget( 'companyname' );
				$data['tradingname']            = sget( 'tradingname' );
				$data['saibppmemberid']         = sget( 'saibppmemberid' );
				$data['companytypeid']          = sget( 'companytypeid' );
				$data['industrysectorid']       = sget( 'industrysectorid' );
				$data['propertysubsectorid']    = sget( 'propertysubsectorid' );
				$data['propertysubsectorother'] = sget( 'propertysubsectorother' );
				$data['propertyassociations']   = post_to_json_str_array( 'propertyassociations' );
				$data['companytel']             = sget( 'companytel' );
				$data['companyemail']           = sget( 'companyemail' );
				$data['companyaddress']         = sget( 'companyaddress' );
				$data['locationid']             = sget( 'locationid' );
				$data['titleid']                = sget( 'titleid' );
				$data['name']                   = sget( 'name' );
				$data['surname']                = sget( 'surname' );
				$data['designation']            = sget( 'designation' );
				$data['yearsoperating']         = sget( 'yearsoperating' );
				$data['tel']                    = sget( 'tel' );
				$data['email']                  = sget( 'email' );

				for ( $i = 1; $i <= 3; $i ++ ) {
					$data[ 'tr_companyname' . $i ]   = sget( 'tr_companyname' . $i );
					$data[ 'tr_description' . $i ]   = sget( 'tr_description' . $i );
					$data[ 'tr_contact' . $i ]       = sget( 'tr_contact' . $i );
					$data[ 'tr_contactnumber' . $i ] = sget( 'tr_contactnumber' . $i );
					$data[ 'tr_contactemail' . $i ]  = sget( 'tr_contactemail' . $i );
					$data[ 'tr_projectcost' . $i ]   = sget( 'tr_projectcost' . $i );
				}

				$this->user_model->update( $this->session->userdata( 'userid' ), $data );
			} else if ( ( $step == 3 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data                               = array();
				$data['blackownershippercent']      = zeroie( sget( 'blackownershippercent' ) );
				$data['blackwomenownershippercent'] = zeroie( sget( 'blackwomenownershippercent' ) );
				$data['youthpercent']               = zeroie( sget( 'youthpercent' ) );
				$data['disabilitypercent']          = zeroie( sget( 'disabilitypercent' ) );
				$data['beestatusid']                = sget( 'beestatusid' );
				$data['enterprisesizeid']           = sget( 'enterprisesizeid' );
				$data['bbbeecerttypeid']            = sget( 'bbbeecerttypeid' );
				$data['verificationauditorid']      = sget( 'verificationauditorid' );
				$data['empoweringsupplierid']       = sget( 'empoweringsupplierid' );
				$data['blackcompanystatusid']       = sget( 'blackcompanystatusid' );
				$data['vatregisteredid']            = sget( 'vatregisteredid' );
				$data['vatnumber']                  = sget( 'vatnumber' );
				$data['companyregno']               = sget( 'companyregno' );
				$data['businessclassificationid']   = sget( 'businessclassificationid' );
				$this->user_model->update( $this->session->userdata( 'userid' ), $data );
			} else if ( ( $step == 4 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data = array();

				$temp = upload_file( 'bbbeecertificate' );
				if ( $temp != '' ) {
					$data['bbbeecertificate'] = $temp;
				}

				$temp = upload_file( 'taxclearancecert' );
				if ( $temp != '' ) {
					$data['taxclearancecert'] = $temp;
				}

				$temp = upload_file( 'workmanscompensation' );
				if ( $temp != '' ) {
					$data['workmanscompensation'] = $temp;
				}

				$temp = upload_file( 'publicliability' );
				if ( $temp != '' ) {
					$data['publicliability'] = $temp;
				}

				$temp = upload_file( 'tradingreferences' );
				if ( $temp != '' ) {
					$data['tradingreferences'] = $temp;
				}

				$temp = upload_file( 'otherdocuments' );
				if ( $temp != '' ) {
					$data['otherdocuments'] = $temp;
				}

				$temp = upload_file( 'ckdocuments' );
				if ( $temp != '' ) {
					$data['ckdocuments'] = $temp;
				}

				$temp = upload_file( 'bankconfirmationletter' );
				if ( $temp != '' ) {
					$data['bankconfirmationletter'] = $temp;
				}

				$temp = upload_file( 'companylogo' );
				if ( $temp != '' ) {
					$data['companylogo'] = $temp;
				}

				$temp = upload_file( 'imageofpremises ' );
				if ( $temp != '' ) {
					$data['imageofpremises '] = $temp;
				}

				$this->user_model->update( $this->session->userdata( 'userid' ), $data );

				if ( sget( 'delete_sd_id' ) != '' ) {
					$this->db->query( 'delete from sd where sd_id = ?', array( sget( 'delete_sd_id' ) ) );
				}
				if ( sget( 'sd_id' ) != '' ) {
					$sddata                       = array();
					$sddata['userid']             = $this->session->userdata( 'userid' );
					$sddata['sd_typeid']          = sget( 'sd_typeid' );
					$sddata['sd_titleid']         = sget( 'sd_titleid' );
					$sddata['sd_firstname']       = sget( 'sd_firstname' );
					$sddata['sd_lastname']        = sget( 'sd_lastname' );
					$sddata['sd_countryid']       = sget( 'sd_countryid' );
					$sddata['sd_idnumber']        = sget( 'sd_idnumber' );
					$sddata['sd_passport']        = sget( 'sd_passport' );
					$sddata['sd_email']           = sget( 'sd_email' );
					$sddata['sd_telno']           = sget( 'sd_telno' );
					$sddata['sd_civilservantid']  = sget( 'sd_civilservantid' );
					$sddata['sd_proassociations'] = post_to_json_str_array( 'sd_proassociations' );
					$sddata['sd_disabledid']      = sget( 'sd_disabledid' );
					$temp                         = upload_file( 'sd_directorsqualification' );
					if ( $temp != '' ) {
						$sddata['sd_directorsqualification'] = $temp;
					}

					if ( sget( 'sd_id' ) == 'N' ) {
						$this->sd_model->insert( $sddata );
					} else {
						$this->sd_model->update( sget( 'sd_id' ), $sddata );
					}
				}
			} else if ( ( $step == 6 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data                            = array();
				$data['supplierpaymentoptionid'] = sget( 'supplierpaymentoptionid' );
				$data['procurerpaymentoptionid'] = sget( 'procurerpaymentoptionid' );

				$option = '';
				if ( ( sget( 'supplierpaymentoptionid' ) == '446' ) || ( sget( 'procurerpaymentoptionid' ) == '448' ) ) {
					$option = 'annual';
				} else {
					$option = '6-monthly';
				}

				$this->user_model->update( $userid, $data );

				$user = $this->user_model->get( $userid );
				membership_email( $user );
				success_msg( 'Your registration was successful. Please check your email for further instructions.' );

				$this->session->set_userdata( 'paymentoption', $option );
				header( 'Location: /payment' );

				return;
			}
		}

		$userid = $this->session->userdata( 'userid' );
		if ( $userid != '' ) {
			$rs = $this->user_model->get( $userid );
		}

		$this->load->view( 'web/view_register', array(
			'pagetitle' => 'Register | SAIBPP',
			'rs'        => $rs,
			'step'      => $step
		) );
	}

	public function profile() {
		$step = 1;
		if ( sget( 'step' ) != '' ) {
			$step = sget( 'step' );
		} else if ( isset( $_GET['step'] ) ) {
			$step = $_GET['step'];
		}

		/*if($this->session->userdata('formdata') == null)
		{
			$data = json_decode(json_encode(user()), true)[0];
		}
		else
			$data = $this->session->userdata('formdata');

		$rs = $data;*/

		if ( isset( $_GET['deleteattachment'] ) ) {
			$userid = $this->session->userdata( 'userid' );
			$this->db->query( 'update user set ' . $_GET['deleteattachment'] . ' = null where userid = ?', array( $userid ) );
			if ( isset( $_GET['gostep'] ) ) {
				header( 'Location: /register?step=' . $_GET['gostep'] );
				exit;
			}
		}

		if ( fp() ) {

			if ( ( $step == 2 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data                           = array();
				$data['userroleid']             = sget( 'userroleid' );
				$data['companyname']            = sget( 'companyname' );
				$data['tradingname']            = sget( 'tradingname' );
				$data['saibppmemberid']         = sget( 'saibppmemberid' );
				$data['companytypeid']          = sget( 'companytypeid' );
				$data['industrysectorid']       = sget( 'industrysectorid' );
				$data['propertysubsectorid']    = sget( 'propertysubsectorid' );
				$data['propertysubsectorother'] = sget( 'propertysubsectorother' );
				$data['propertyassociations']   = post_to_json_str_array( 'propertyassociations' );
				$data['companytel']             = sget( 'companytel' );
				$data['companyemail']           = sget( 'companyemail' );
				$data['companyaddress']         = sget( 'companyaddress' );
				$data['locationid']             = sget( 'locationid' );
				$data['titleid']                = sget( 'titleid' );
				$data['name']                   = sget( 'name' );
				$data['surname']                = sget( 'surname' );
				$data['designation']            = sget( 'designation' );
				$data['vatregisteredid']        = sget( 'vatregisteredid' );
				$data['vatnumber']              = sget( 'vatnumber' );
				$data['companyregno']           = sget( 'companyregno' );
				$data['enterprisesizeid']       = sget( 'enterprisesizeid' );
				$data['yearsoperating']         = sget( 'yearsoperating' );
				$data['tel']                    = sget( 'tel' );
				$data['email']                  = sget( 'email' );

				for ( $i = 1; $i <= 3; $i ++ ) {
					$data[ 'tr_companyname' . $i ]   = sget( 'tr_companyname' . $i );
					$data[ 'tr_description' . $i ]   = sget( 'tr_description' . $i );
					$data[ 'tr_contact' . $i ]       = sget( 'tr_contact' . $i );
					$data[ 'tr_contactnumber' . $i ] = sget( 'tr_contactnumber' . $i );
					$data[ 'tr_contactemail' . $i ]  = sget( 'tr_contactemail' . $i );
					$data[ 'tr_projectcost' . $i ]   = sget( 'tr_projectcost' . $i );
				}
				$this->user_model->update( $this->session->userdata( 'userid' ), $data );
			} else if ( ( $step == 3 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data                               = array();
				$data['blackownershippercent']      = zeroie( sget( 'blackownershippercent' ) );
				$data['blackwomenownershippercent'] = zeroie( sget( 'blackwomenownershippercent' ) );

				$data['beestatusid']           = sget( 'beestatusid' );
				$data['bbbeecerttypeid']       = sget( 'bbbeecerttypeid' );
				$data['verificationauditorid'] = sget( 'verificationauditorid' );
				$data['empoweringsupplierid']  = sget( 'empoweringsupplierid' );
				$data['blackcompanystatusid']  = sget( 'blackcompanystatusid' );

				$this->user_model->update( $this->session->userdata( 'userid' ), $data );
			} else if ( ( $step == 4 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$data = array();

				$temp = upload_file( 'bbbeecertificate' );
				if ( $temp != '' ) {
					$data['bbbeecertificate'] = $temp;
				}

				$temp = upload_file( 'taxclearancecert' );
				if ( $temp != '' ) {
					$data['taxclearancecert'] = $temp;
				}

				$temp = upload_file( 'workmanscompensation' );
				if ( $temp != '' ) {
					$data['workmanscompensation'] = $temp;
				}

				$temp = upload_file( 'publicliability' );
				if ( $temp != '' ) {
					$data['publicliability'] = $temp;
				}

				$temp = upload_file( 'tradingreferences' );
				if ( $temp != '' ) {
					$data['tradingreferences'] = $temp;
				}

				$temp = upload_file( 'otherdocuments' );
				if ( $temp != '' ) {
					$data['otherdocuments'] = $temp;
				}

				$temp = upload_file( 'ckdocuments' );
				if ( $temp != '' ) {
					$data['ckdocuments'] = $temp;
				}

				$temp = upload_file( 'imageofpremises' );
				if ( $temp != '' ) {
					$data['imageofpremises'] = $temp;
				}

				$temp = upload_file( 'bankconfirmationletter' );
				if ( $temp != '' ) {
					$data['bankconfirmationletter'] = $temp;
				}

				$temp = upload_file( 'companylogo' );
				if ( $temp != '' ) {
					$data['companylogo'] = $temp;
				}

				$this->user_model->update( $this->session->userdata( 'userid' ), $data );

				if ( sget( 'delete_sd_id' ) != '' ) {
					$this->db->query( 'delete from sd where sd_id = ?', array( sget( 'delete_sd_id' ) ) );
				}
				if ( sget( 'sd_id' ) != '' ) {
					$sddata                       = array();
					$sddata['userid']             = $this->session->userdata( 'userid' );
					$sddata['sd_typeid']          = sget( 'sd_typeid' );
					$sddata['sd_titleid']         = sget( 'sd_titleid' );
					$sddata['sd_firstname']       = sget( 'sd_firstname' );
					$sddata['sd_lastname']        = sget( 'sd_lastname' );
					$sddata['sd_countryid']       = sget( 'sd_countryid' );
					$sddata['sd_idnumber']        = sget( 'sd_idnumber' );
					$sddata['sd_passport']        = sget( 'sd_passport' );
					$sddata['sd_email']           = sget( 'sd_email' );
					$sddata['sd_telno']           = sget( 'sd_telno' );
					$sddata['sd_civilservantid']  = sget( 'sd_civilservantid' );
					$sddata['sd_proassociations'] = post_to_json_str_array( 'sd_proassociations' );
					$sddata['sd_disabledid']      = sget( 'sd_disabledid' );
					$temp                         = upload_file( 'sd_directorsqualification' );
					if ( $temp != '' ) {
						$sddata['sd_directorsqualification'] = $temp;
					}

					if ( sget( 'sd_id' ) == 'N' ) {
						$this->sd_model->insert( $sddata );
					} else {
						$this->sd_model->update( sget( 'sd_id' ), $sddata );
					}
				}
			} else if ( ( $step == 6 ) && ( sget( 'wizaction' ) != '2' ) ) {
				$userid                          = $this->session->userdata( 'userid' );
				$data                            = array();
				$data['supplierpaymentoptionid'] = sget( 'supplierpaymentoptionid' );
				$data['procurerpaymentoptionid'] = sget( 'procurerpaymentoptionid' );
				$this->user_model->update( $userid, $data );

				success_msg( 'Your profile was updated successfully!' );

				$option = '';
				if ( ( sget( 'supplierpaymentoptionid' ) == '446' ) || ( sget( 'procurerpaymentoptionid' ) == '448' ) ) {
					$option = 'annual';
				} else {
					$option = '6-monthly';
				}

				//membership_email($rs);
				//$this->session->set_userdata('paymentoption',$option);
				header( 'Location: /myprofile' );

				return;
			}
		}

		if ( $this->session->userdata( 'userid' ) != '' ) {
			$rs = $this->user_model->get( $this->session->userdata( 'userid' ) );
		} else {
			$rs = null;
		}

		$this->load->view( 'web/view_register', array(
			'pagetitle' => 'Register | SAIBPP',
			'rs'        => $rs,
			'step'      => $step
		) );
	}

	public function forgotpassword() {
		if ( fp() ) {
			$rs = $this->user_model->find_all( array( 'email' => sget( 'email' ) ) );
			if ( count( $rs ) == 0 ) {
				error_msg( 'Your account was not found on the system.' );
			} else {
				sendemail( $rs[0]->email, 'Your password', 'Your password is: ' . $rs[0]->password, null );
				success_msg( 'Your password was sent to ' . sget( 'email' ) );
			}
		}
		$this->load->view( 'web/view_forgotpassword', array(
			'pagetitle' => 'Forgot Password | SAIBPP'
		) );
	}

	public function search() {
		/*if($this->session->userdata('user') == null)
		{
			error_msg('Please login first to access the search functionality.');
			header('Location: /login');
		}*/
		$this->load->view( 'web/view_search', array(
			'pagetitle' => 'Search Results | SAIBPP'
		) );
	}

	public function companyprofile() {
		$rs = $this->user_model->get( decuri( $this->uri->segment( 2 ) ) );
		$this->load->view( 'web/view_companyprofile', array(
			'pagetitle' => 'View Company Profile | SAIBPP',
			'rs'        => $rs
		) );
	}

	public function myprofile() {
		$userid = $this->session->userdata( 'userid' );
		$rs     = $this->user_model->get( $userid );
		$this->load->view( 'web/view_myprofile', array(
			'pagetitle' => 'View Company Profile | SAIBPP',
			'rs'        => $rs
		) );
	}

	public function mytenders() {
		$userid = $this->session->userdata( 'userid' );
		if ( $this->uri->segment( 2 ) != '0' ) {
			$this->tender_model->delete( decuri( $this->uri->segment( 2 ) ) );
		}
		$rs = $this->tender_model->find_all( array( 'userid' => $userid ) );
		$this->load->view( 'web/view_mytenders', array(
			'pagetitle' => 'My Tenders | SAIBPP',
			'rs'        => $rs
		) );
	}

	public function createtender() {
		$pagetitle = 'Create Tender';
		$rs        = null;
		if ( $this->uri->segment( 2 ) != '0' ) {
			$pagetitle = 'Edit Tender';
			$rs        = $this->tender_model->get( decuri( $this->uri->segment( 2 ) ) );
		}

		if ( fp() ) {
			$data                        = array();
			$data['userid']              = $this->session->userdata( 'userid' );
			$data['createdon']           = mysqltime();
			$data['tendercategoryid']    = sget( 'tendercategoryid' );
			$data['description']         = sget( 'description' );
			$data['tendernumber']        = sget( 'tendernumber' );
			$data['tenderdate']          = sget( 'tenderdate' );
			$data['tenderclosedate']     = sget( 'tenderclosedate' );
			$data['tenderclosedatetime'] = sget( 'tenderclosedatetime' );
			$data['briefingsessiondate'] = sget( 'briefingsessiondate' );
			$data['briefingsessiontime'] = sget( 'briefingsessiontime' );
			$temp                        = upload_file( 'tenderdocument' );
			if ( $temp != '' ) {
				$data['tenderdocument'] = $temp;
			}
			$data['tenderdatetime'] = sget( 'tenderdatetime' );
			if ( $this->uri->segment( 2 ) == '0' ) {
				$this->tender_model->insert( $data );
			} else {
				$this->tender_model->update( decuri( $this->uri->segment( 2 ) ), $data );
			}
			success_msg( 'Tender saved!' );
			header( 'Location: /mytenders/0' );
			exit;
		}

		$this->load->view( 'web/view_createtenders', array(
			'pagetitle' => $pagetitle,
			'rs'        => $rs
		) );
	}

	public function viewtender() {
		$rs = $this->tender_model->get( decuri( $this->uri->segment( 2 ) ) );
		$this->load->view( 'web/view_viewtender', array(
			'pagetitle' => 'View Tender | SAIBPP',
			'rs'        => $rs
		) );
	}

	public function mynotifications() {
		$rs = $this->lookup_model->find_all( array( 'lookuptype' => 'tendercategoryid' ) );
		if ( fp() ) {
			$data                  = array();
			$data['notifications'] = post_to_json_str_array( 'notifications' );
			$this->user_model->update( $this->session->userdata( 'userid' ), $data );
			success_msg( 'Notification settings saved.' );
		}
		$this->load->view( 'web/view_mynotifications', array(
			'pagetitle' => 'My Notifications',
			'rs'        => $rs
		) );
	}

}
